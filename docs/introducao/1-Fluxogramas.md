﻿# Fluxogramas

A seguir serão apresentados os fluxogramas dos principais processos do sistema IOSE e seus respectivos descritivos, de modo a especificar cada etapa e exceções presentes na mesma.

## Cadastro de Usuário 

Fluxograma para o processo de cadastro de todos os usuários pertencentes ao sistema.

### Fluxo para Cadastro de Administrador , Gerente(s) e Usuário(s) de Integração 
<br>

```mermaid
graph TD

	Inicio((Início)):::inicio-->
	Etapa1-Cadastramento(Contrato)-->
	Etapa-Cadastramento-Condicional{Contrato <br>solicita<br> Usuário <br>de Integração?}-->
		|Sim| Cadastra[Cria conta <br>de Usuário <br>de Integração]-->
			  Etapa2-Cadastramento(Cria conta do <br>Administrador e <br>Assinatura do Cliente)

	Etapa-Cadastramento-Condicional{Contrato <br>solicita<br> Usuário <br>de Integração?}-->
		|Não| Etapa2-Cadastramento(Cria conta do <br>Administrador e <br>Assinatura do Cliente)
			  

	Etapa2-Cadastramento(Cria conta do <br>Administrador e <br>Assinatura do Cliente)-->
	Etapa3-Cadastramento(Visita e <br>Levantamento <br>de Dispositivos)

		Etapa3-Cadastramento-->
			Item0(Passa parâmetro de produção)-->
			Item1(Monta, calibra e <br> grava Assinatura <br>do Cliente no módulo em fábrica)-->
			Configuracao_e_Instalacao[Configuração <br>e Instalação]:::titulo-->
			Fim((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
```
<br>

#### Fluxo Principal 
1. Um contrato é feito entre o provedor do serviço e a empresa cliente.
1. Se o contrato solicita um __usuário de integração__ é criada uma conta para este. ([Fluxo E1](#e1-o-contrato-nao-solicita-um-usuario-de-integracao))
1. Uma conta de __administrador__ e uma assinatura única são criadas para a empresa cliente.
1. Uma visita técnica é feita no local de instalação e é realizado o levantamento de quantos módulos serão instalados.
1. Os parâmetros de produção são passados para a fábrica, tais como assinatura do cliente e quantidade de módulos.
1. A montagem dos módulos é realizada, bem como a calibração e a gravação da assinatura do cliente.
1. É iniciado o fluxo de [configuração e instalação](#configuracao-e-instalacao).



#### Fluxo de Exceção 
##### E1. O contrato não solicita um Usuário de Integração
- E1.1 Não é criado um usuário de integração e o fluxograma segue normalmente.


### Fluxo para cadastro de Operador 
<br>

```mermaid
graph TD
	Inicio((Início)):::inicio-->
	Cadastramento-F2-Etapa1(Gerente solicita e-mail ao Operador)-->
	Cadastramento-F2-Etapa2(Gerente envia convite ao Operador)-->
	Cadastramento-F2-Etapa2-Condicional(Operador recebe dados de acesso)-->
	Fim((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
```
<br>

#### Fluxo Principal 

1. Após criada a conta de __gerente__, o mesmo solicita o e-mail do __operador__ à ser cadastrado.
1. __Gerente__ envia convite para e-mail do __operador__.
1. __Operador__ aceita convite enviado e recebe os dados de acesso.

## Configuração e Instalação  

Fluxograma para o processo de configuração e instalação dos módulos.

<br>

```mermaid
graph TD
	
	Inicio((Início)):::inicio --> liga(Liga Módulo <br>em série com circuito)
	liga --> levanta{Módulo levanta <br>Host AP?}
	levanta --> |Sim| substitui{Está substituindo <br>Módulo?}
	
	substitui --> |sim| solicita(Solicita parâmetros <br> de substituição <br> do módulo) -->
		insere(Insere parâmetros <br> de substituição <br> no módulo) --> abre-app(Abre app de <br> configuração wifi)
	substitui --> |não| abre-app
	abre-app --> insere-rede(Insere parâmetro de rede) --> troca-chave{Trocou chave <br> com servidor?} --> |sim| conecta-modulo(Módulo se conecta <br> ao grupo geral de <br> troca de dados) -->
		fecha-app(Fecha app de <br> configuração de módulo) --> abre-app-user(Abre app IOSE <br> como super usuário) -->
		atrela(Super usuário atrela módulos ao quadro) --> testa-com(Testa comunicação <br> de fluxo de <br> dados do módulo) -->
		comunica{Comunicou?} --> |Sim| Fim((Fim)):::fim
	reinicia(Reinicia módulo) --> levanta
	
	troca-chave --> |não| exibe(Exibe mensagem <br> de erro no app) --> reinicia

	comunica --> |Não| troca --> liga
	levanta --> |Não| troca(Troca Módulo)


	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
```

#### Fluxo Principal 

1. É feita a instalação do módulo em série com o circuito do quadro de energia.
1. Ao ligar e detectar que não está conectado ao WiFi, o módulo habilita uma rede HostAP.
1. É aberto o aplicativo de configuração do wifi para se conectar à rede HostAP.
	([Fluxo de Exceção **E1**](#e1-substituicao-de-modulo))
1. São inseridas as credenciais do wifi pelo aplicativo de configuração de módulo.
1. Acontece a troca de chaves com o servidor.([Fluxo de Exceção **E2**](#e2-troca-de-chaves-nao-teve-exito))
1. Módulo se conecta ao grupo geral de troca de dados,onde são geridas todas as informações que os módulos mandam para o servidor.
1. O aplicativo de configuração do wifi é encerrado.
1. É aberto o aplicativo IOSE na interface Super Usuário.
1. O __super usuário__ irá atrelar cada módulo ao seu respectivo quadro.
1. É feito um teste de acionamento de cada módulo para garantir que as configurações foram feitas de forma correta. ([Fluxo de Exceção **E3**](#e3-teste-de-acionamento-do-modulo-nao-teve-exito))
1. É finalizada a configuração e a instalação do módulo.

#### Fluxo de exceção 

##### E1. Substituição de módulo
- E1.1 Solicita parâmetros de substituição do módulo antigo.
- E1.2 Insere parâmetros de substituição no módulo novo.

##### E2. Troca de chaves não teve êxito
- E2.1 Exibe uma mensagem de erro no aplicativo de configuração wifi informando que o módulo não conseguiu se conectar ao servidor.
- E2.2 O módulo é reiniciado e fluxo retorna para levantar HostAP para assim recomeçar o ciclo de conexão.

##### E3. Teste de acionamento do módulo não teve êxito
- E3.1 O erro é analisado, estudado e corrigido pela equipe de instalação, se necessário,realizando a trocar do módulo.
- E3.2 O teste é feito novamente.


## Acionamento e Desacionamento 

Fluxograma para o processo de acionamento e desacionamento dos módulos.

<br>

```mermaid
graph TD
	
	Inicio((Início)):::inicio-->
	Acion_e_Desaci-Etapa1(Servidor envia <br>requisição para módulo)-->
	Acion_e_Desaci-Etapa2(Módulo recebe,<br> executa e <br>notifica servidor)-->
	Acion_e_Desaci-Condicional{Servidor recebeu <br>notificação do módulo?}-->
		|Sim| Confirmação[Apresenta mensagem <br>de confirmação ao usuário]-->
		Fim((Fim)):::fim

	Acion_e_Desaci-Condicional{Servidor recebeu <br>notificação do módulo?}-->
		|Não| Negação[Apresenta mensagem <br>de erro ao usuário]-->
		Acion_e_Desaci-Condicional-Não-Etapa1(Mantém estado do Relé)-->
		Fim((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
```

#### Fluxo Principal 

1. Servidor envia requisição para o módulo, podendo ser para ligar ou desligar o relé.
1. Módulo recebe a requisição, confirma para o servidor que recebeu, executa a ação de ligar ou desligar, e novamente confirma ao servidor se a ação foi bem sucedida. ([Fluxo de Exceção **E1**](#e1-modulo-nao-conseguiu-executar-a-acao-requisitada-pelo-servidor) )
1. Apresenta a mensagem de confirmação ao usuário, informando que processo ocorreu como esperado.
1. Finaliza o acionamento ou desacionamento.

#### Fluxo de Exceção 

##### E1. Servidor não recebeu notificação de confirmação emitida pelo módulo.
- E1.1 Apresenta mensagem de erro para o usuário.
- E1.2 Mantém o estado do relé.
- E1.3 Finaliza o acionamento ou desacionamento.


## Agendamento do Módulo 

Fluxograma para o processo de agendamento de módulos.

<br>

```mermaid
graph TD

	Inicio((Início)):::inicio-->
	Agend-Etapa1(Usuário solicita criação,<br> remoção ou alteração <br>de  um agendamento)-->
	Agend-Etapa2(Servidor envia requisição <br>para o módulo)-->
	Agend-Etapa3(Módulo realiza ação requerida )-->
	Agend-Etapa2-Condicional{Servidor recebe notificação <br> do módulo?}-->
		|Sim| Confirmação[Servidor realiza <br>ação requerida <br>no Banco de Dados]-->
			  Agend-Etapa2-Condicional-Sim-Etapa1(Apresenta mensagem de <br>confirmação ao usuário)-->
			  Fim((Fim)):::fim
		
	Agend-Etapa2-Condicional{Servidor recebe  <br>notificação <br> do módulo?}-->
		|Não| Tentativa[Apresenta mensagem <br>de erro ao usuário]-->
			  Fim((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
	
```

#### Fluxo Principal 

- 1. Usuário solicita criação, remoção ou alteração de um determinado agendamento em sua repectiva interface;

- 2. Servidor envia requisição para todos os módulos pertencentes ao agendamento;

- 3. Módulo(s) recebe requisição e executa ação (deletar, cadastrar ou alterar o agendamento) e depois notifica servidor;

- 4. Servidor receber notificação do(s) módulo(s) ([Fluxo de Exceção **E1**](#e1-servidor-nao-recebe-notificacao-do-modulo));

- 5. Servidor cadastra, deleta ou altera agendamento no Banco de Dados;

- 6. É emitida uma mensagem de confirmação na interface do usuário;

- 7. Agendamento é finalizado;

#### Fluxo de exceção 

##### E1. Servidor não recebe notificação do módulo
- É apresentado ao usuário um alerta que não foi possível executar a ação requerida e solicitado a ele que tente novamente;  
- Agendamento é finalizado;


## Aquisição de Medidas 

Fluxograma para o processo de aquisição de medidas.

<br>

```mermaid
graph TD
	
	Inicio((Início)):::inicio-->
	AquisMedi-Etapa1(Módulo faz as medições do circuito)-->
	AquisMedi-Etapa2(Módulo envia medidas para servidor)-->
	AquisMedi-Etapa2-C{Servidor recebeu <br>as medidas do<br> módulo?}-->
		|Sim| Salva[Salva em Banco de dados]-->
			  Fim((Fim)):::fim

	AquisMedi-Etapa2-C{Servidor recebeu <br>as medidas  do<br> módulo?}-->
		|Não| Guarda[Módulo guarda medidas]-->
			   AquisMedi-Etapa2-Condicional-Não-Condicional{Conexão <br>restabelecida?}-->
			   		|Sim| Envia(Envia medidas guardadas)-->
					Salva(Salva em Banco de dados)
					   	  
				AquisMedi-Etapa2-Condicional-Não-Condicional{Conexão <br>restabelecida?}--> |Não| Guarda[Módulo guarda medidas]



	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
```
#### Fluxo Principal 

1. O módulo faz a leitura de seus sensores.
1.  O módulo realiza o envio de suas leituras para o servidor. ([Fluxo de Exceção **E1**](#e2-servidor-nao-recebe-leituras-do-modulo-devido-a-um-erro-ou-falta-de-conexao)).
1.  As leituras realizadas são salvas no Banco de Dados.
1.  É finalizada a aquisição de medidas.

#### Fluxos de Exceção 

##### E1. Servidor não recebe leituras do módulo devido a um erro ou falta de conexão.
- E1.1 Módulo guarda as leituras em sua memória interna.
- E1.2 Após conexão restabelecida, módulo envia as leituras ao servidor e apaga memória com  medidas anteriormente guardadas. ([Fluxo de Exceção **E2**](#e2-conexao-nao-foi-restabelecida))
- E1.3 As leituras realizadas são salvas no Banco de Dados.

##### E2. Conexão não foi restabelecida
- E2.1 Módulo guarda as leituras em sua memória interna e após um tempo testa novamente conexão, repete esse processo até que a conexão seja restabelecida e possa enviar as medidas.


