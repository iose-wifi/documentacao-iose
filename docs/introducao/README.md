﻿# Introdução

O IOSE fornece módulos para quadros elétricos tornando este um sistema inteligente, com foco em eficiência energética, possibilitando ao usuário controle, corte e religamento da energia por cada circuito de uma unidade consumidora. Aqui serão descritas os principais conceitos e definições que serão necessários para o entendimento do resto da documentação bem como para o entendimento do próprio sistema IOSE em sua versão wifi. Toda a documentação está considerando o modelo de negócios B2C contratual.


## Definições
- __Usuário__ - Qualquer pessoa que tem uma conta de acesso ao sistema IOSE, estes possuem diferentes níveis de acesso às funcionalidades do sistema.
- __Cliente__ - Entidade que representa o conjunto de todas unidades de um determinado contratante.
- __Módulo__ - Termo utilizado para referenciar o hardware do sistema IOSE.
- __Unidade__ - Termo utilizado dentro do sistema IOSE para definir um local físico que está utilizando o sistema. Por exemplo, se uma unidade de um grupo empresarial vai utilizar o IOSE, este local é criado dentro do sistema como uma "unidade virtual".
- __Quadro__ - Termo utilizado dentro do sistema IOSE para definir um grupo que tem relação com um quadro elétrico de um dos locais onde foi instalado o sistema. Desta forma, se em uma unidade existem 3 quadros elétricos serão criados 3 "quadros virtuais" na interface do sistema IOSE. Nos "quadros virtuais" é que serão inseridos os "módulos virtuais", desta forma tem-se uma relação entre a parte física do sistema(quadro elétrico e módulos) e a parte de software.

## Usuários
Aqui serão descritas as informações sobre os tipos de usuários que tem acesso ao sistema.

### Definição dos usuários
Os usuários do sistema foram divididos em quatro tipos

- __Super__ - É um tipo de usuário criado para os funcionários da empresa, estes são os que trabalham com a tarefa de instalação e manutenção dos módulos, bem como nos programas, fornecendo assitência para os clientes.

- __Usuario de integração__ - É um cliente que foi estabelecido em contrato, que possui permissão de visualizar os dados de outros clientes e usar esses dados em seu sistema e pode possuir a permissão de acionar ou desacionar módulos. Por exemplo, uma empresa que presta serviço de análise de dados a um empresa que irá usar o sistema IOSE ou caso a empresa tenha um sistema interno de análise financeira e deseja usar certos dados do IOSE, em ambos, um usuário de integração deverá existir.

- __Administrador__ - Usuário criado para os contratantes do IOSE, este é o primeiro na escala de importância corresponde ao dono da empresa ou sócios que contrata o IOSE, ele está ligado a entidade __Cliente__.

- __Gerente__ - Também associado aos clientes, este usuário ajuda o administrador na tarefa de gerenciar uma unidade, porém com um nível de acesso mais restrito em determinadas funções, pode ser um ou mais de um atrelado a uma conta de administrador.

- __Operador__ - É a classe com menor nível de acesso às funcionalidades do sistema, pode ser um ou mais de um atrelado a uma conta de administrador.

- __Operador com acionamento__ - Tem o mesmo nível de acesso do __operador__ com o diferencial que pode acionar um circuito, pode ser um ou mais de um atrelado a uma conta de administrador.


### Hierarquia de usuários
Os usuários são definidos em uma certa hierarquia onde o __super__ é a maior delas e está associada aos funcionários da empresa. Os demais usuários são todos relacionados ao cliente do produto, dentre estes temos o __usuario de integração__,o __administrador__ que é o maior na hierarquia, o seu subordinado que é o __gerente__ e abaixo dos gerentes temos os __operador com acionamento__ e __operador__ . Visualmente a estrutura é similar a apresentada a seguir. 

- super
- usuário de integração
- administrador
    - gerente
        - operador
        - operador com acionamento


### Permissões de usuários

1. :heavy_minus_sign: - Não possui Atorização.
1. :heavy_check_mark: - Possui Atorização.
<!-- 1. :heavy_exclamation_mark: - Pode ter este nível de Atorização se concedida por um usuário de nível superior. -->

Permissões | super | administrador | gerente | operador | operador com acionamento | usuário de integração
-------- | --------|---------------|---------|----------|--------------------------|------------------------------
Criar __administrador__        | :heavy_check_mark: |:heavy_minus_sign: | :heavy_minus_sign:     | :heavy_minus_sign: |   :heavy_minus_sign:   |:heavy_minus_sign:|
Visualizar __administrador__   | :heavy_check_mark: |:heavy_minus_sign: | :heavy_minus_sign:     | :heavy_minus_sign:   | :heavy_minus_sign:     |:heavy_minus_sign:|
Criar __gerente__              | :heavy_check_mark: |:heavy_check_mark: | :heavy_minus_sign:     | :heavy_minus_sign: |   :heavy_minus_sign:   |:heavy_minus_sign:|
Visualizar __gerente__         | :heavy_check_mark: |:heavy_check_mark: |:heavy_minus_sign:      | :heavy_minus_sign:  |  :heavy_minus_sign:    |:heavy_minus_sign:|
Criar __funcionário__          | :heavy_check_mark: |:heavy_check_mark: | :heavy_check_mark:     | :heavy_minus_sign:  |  :heavy_minus_sign:    |:heavy_minus_sign:|
Visualizar __funcionário__     | :heavy_check_mark: |:heavy_check_mark: |:heavy_check_mark:      | :heavy_minus_sign:  |   :heavy_minus_sign:    |:heavy_minus_sign:|
Criar unidade                  | :heavy_check_mark: |:heavy_check_mark: | :heavy_minus_sign:     | :heavy_minus_sign:  |  :heavy_minus_sign:    |:heavy_minus_sign:|
Visualizar mais de uma unidade | :heavy_check_mark: |:heavy_check_mark: | :heavy_minus_sign:     | :heavy_minus_sign:  |  :heavy_minus_sign:    |:heavy_minus_sign:|
Criar quadro                   | :heavy_check_mark: |:heavy_check_mark: | :heavy_minus_sign:     | :heavy_minus_sign: |    :heavy_minus_sign:   |:heavy_minus_sign:|
Adicionar módulo em quadro     | :heavy_check_mark: |:heavy_check_mark: | :heavy_minus_sign:     | :heavy_minus_sign:  |  :heavy_minus_sign:    |:heavy_minus_sign:|
Acionar módulo                 | :heavy_check_mark: |:heavy_check_mark: | :heavy_check_mark:     | :heavy_minus_sign:  |  :heavy_check_mark:    |:heavy_minus_sign:|
Criar agendamento              | :heavy_check_mark: |:heavy_check_mark: | :heavy_check_mark:     | :heavy_minus_sign:  |  :heavy_minus_sign:    |:heavy_minus_sign:|
Visualizar Dashboard de Quadro | :heavy_check_mark: |:heavy_check_mark: | :heavy_check_mark:     | :heavy_check_mark:  |  :heavy_check_mark:    |:heavy_minus_sign:|
