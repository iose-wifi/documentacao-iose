# Banco de dados 
A seguir será exposto o diagrama dos documentos que serão armazenados na nossa base de dados NoSQL (DynamoDB).

PK : Primary key

SI : Secondary index

OK : Ordenation Key
<!-- ![nova imagem](./imagens/bd.jpg) -->

```mermaid
classDiagram
    class measures{
        active_power: Number
        apparent_power: Number
        current: Number
        export_active_energy: Number
        export_reactive_energy: Number
        import_active_energy: Number
        import_reactive_energy: Number
        index: Number
        line_frequency: Number
        power_factor: Number
        reactive_power: Number
        relay_state: Boolean
        system_status: Number
        thermistor_voltage: Number
        voltage: Number
        uuid_measure : String : (PK)
        status_measure : String : (SI)
        counter: Number : (OK)
        uuid_circuit : String : (SI) 
        created_at: String : (OK)
    }

    class employee{
        created_at : String
        update_at: String
        name: String
        group: String
        estado: String
        cidade: String
        bairro: String
        rua: String
        complemento: String
        numero: Number
        moradores: Number
        cep: String
        cpf: String 
        uuid_unity : String 
        uuid_employee : String : (PK)
        email: String : String : (SI)
        uuid_client : String : (SI)
    }

    class agents_tariffs{
        created_at : String
        update_at: String
        accessor: String
        class: String
        consumption_tariff_te: Number
        consumption_tariff_tusd: Number
        demand_tariff_tusd: Number
        modality: String
        post: String
        subclass: String
        uuid_agent_tariff : String : (PK)
        name_agent : String : (SI)
        subgroup: String : (OK)
        valid_since : String : (OK)
    }

``` 

```mermaid
classDiagram
    class admin{
        created_at : String
        update_at: String
        name: String
        estado: String
        cidade: String
        bairro: String
        rua: String
        numero: Number
        cep: String
        cpf: String 
        uuid_admin : String : (PK)
        uuid_client : String : (SI)
        email: String : String : (SI)
    }

    class circuit{
        created_at: String
        update_at: String
        timestamp: String
        code_module: String
        has_relay: Boolean
        relay_state: Boolean
        name: String
        description: String
        uuid_circuit : String : (PK)
        uuid_unity : String : (SI)
        uuid_client : String : (SI)
        uuid_group : String : (SI) 
    }

    class household_appliances{
        created_at : String
        update_at: String
        name: String
        label: String 
        hours: Number
        potency: Number
        quantity: Number
        uuid_household_appliances : String : (PK)
        uuid_circuit : String : (SI)
        uuid_group : String : (SI)
        uuid_unity : String : (SI)
    }

```

```mermaid
classDiagram
    class unity{
        created_at: String
        update_at: String
        name: String
        description: String
        contracted_demand: Number  
        tariff_data: Object           
        goal: Number
        tariff_period: Number
        uuid_unity : String : (PK)
        uuid_client : String : (SI) 
    }

    class requisitions_scheduling{
        created_at : String
        update_at: String
        action_requisition: String
        index: Number
        state_requisition: String
        type_requisition: String
        uuid_circuit: String
        uuid_scheduling_group: String
        uuid_scheduling_module: String
        uuid_requisition_scheduling : String : (PK) 
    }

    class requisitions_actuation{
        created_at : String
        update_at: String
        state_actuation: Boolean
        type_requisition: String
        uuid_group: String
        uuid_unity: String  
        uuid_requisition_actuation : String : (PK)
        uuid_circuit : String : (SI)
        state_requisition: String :(OK)
    }

```    

```mermaid
classDiagram
    class scheduling_group{
        created_at: String
        update_at: String
        name: String
        action_relay: Boolean
        repeat: Boolean       
        week: Object       
        run_at: String
        uuid_scheduling_group : String : (PK)
        uuid_unity : String : (SI)
    }

    class scheduling_module{
        created_at: String
        update_at: String
        index: Number
        uuid_scheduling_module : String : (PK)
        uuid_unity: String : (SI)
        uuid_scheduling_group: String : (SI)
        uuid_circuit : String : (SI)
        state: String : (OK)
    }

    class flags{
        created_at : String
        update_at: String
        flag: String
        value: Number
        uuid_flag : String : (PK)
        year : Number : (SI)
        month: Number : (OK)
    }

```

```mermaid
classDiagram
    class group{
        created_at: String
        update_at: String
        name: String
        goal: Number
        description: String
        uuid_group : String : (PK)
        uuid_unity : String : (SI)
    }

    class connections{
        update_at: String
        domain_connection: String
        uuid_connection : String : (PK)
        uuid_group : String : (SI)
        uuid_unity : String : (SI)
        created_at : String : (OK)
    }
        
    class clients{
        created_at : String
        update_at: String
        name: String
        description: String 
        uuid_client : String : (PK)
    }

```

```mermaid
classDiagram
    class default_household_appliances{
        name: String
        label: String 
        potency: String
        uuid_client : String : (PK)
    }

    class notification{
        message : String
        uuid_group : String : (PK) 
        sent : Boolean : (SI) 
    }

    class alarms{
        uuid_alarms : String : (PK) 
    }    

```
## Tabela Measures
Measure | (Medida) |
-------- | --------|
active_power: Number |Medida de poder ativo.|
apparent_power: Number |Medida de poder aparente.|
current: Number |Medida atual.|
export_active_energy: Number |Medida da exportação de energia ativa.|
export_reactive_energy: Number |Medida da exportação de energia reativa.|
import_active_energy: Number |Medida da importação de energia ativa.|
import_reactive_energy: Number |Medida da importação de energia reativa.|
index: Number |Índice da medida.|
line_frequency: Number |Medida da frequência da linha.|
power_factor: Number |Medida do fator de poder.|
reactive_power: Number |Medida do poder reativo.|
relay_state: Boolean |Estado do relé.|
system_status: Number |Número do status do sistema.|
thermistor_voltage: Number |Voltagem do termistor.|
voltage: Number |Medida da voltagem.| 
uuid_measure : String :(PK) | __(Chave Primária)__ Identificador único universal da medida.|
status_measure : String :(SI) | __(Índice Secundário)__ Status da medida.|
counter: Number :(OK) | __(Chave de Ordenação)__ Medida do contador.|
uuid_circuit : String :(SI) | __(Índice Secundário)__ Identificador único universal do circuito.| 
created_at: String :(OK) | __(Chave de Ordenação)__ Data de criação.|

## Tabela Employee
Employee | (Empregado) |
-------- | --------|
created_at : String        | Data da criação do empregado. |
update_at: String          | Data da atualização do empregado. |
name: String             | Nome do empregado. |
group: String |Grupo do empregado.|
estado: String |Estado do empregado.|
cidade: String |Cidade do empregado.|
bairro: String |Bairro do empregado.|
rua: String |Rua do empregado.|
complemento: String |Complemento do endereço do empregado.|
numero: Number |Número da casa do empregado.|
moradores: Number |Quantidade de moradores da casa do empregado.|
cep: String |Código postal do empregado.|
cpf: String |Cadastro de pessoa física do empregado.|
uuid_unity : String      |Identificador único universal da unidade.|
uuid_employee : String :(PK)        | __(Chave Primária)__ Identificador único universal do empregado. |
email: String : String : (SI) | __(Índice Secundário)__ Email do empregado.| 
uuid_client : String :(SI)        | __(Índice Secundário)__ Identificador único universal do cliente. |

## Tabela Agents_tariffs
Agents_tariffs | (Tarifas dos agentes) |
-------- | --------|
created_at : String        | Data da criação da tarifa do agente. |
update_at: String          | Data da atualização da tarifa do agente. |
accessor: String |Acessador.|
class: String |Classe da tarifa.|  
consumption_tariff_te: Number |Consumo da tarifa (te).|
consumption_tariff_tusd: Number |Consumo da tarifa (tusd).|
demand_tariff_tusd: Number |Demanda da tarifa (te).|
modality: String |Modalidade da tarifa.|
post: String |Publicação.|
subclass: String |Subclasse da tarifa.|
uuid_agent_tariff : String : (PK) | __(Chave Primária)__ Identificador único universal do empregado. |
name_agent : String : (SI) |__(Índice Secundário)__ Nome do agente de tarifa.|
subgroup: String : (OK) |__(Chave de Ordenção)__ Subgrupo da tarifa.|
valid_since : String : (OK) |__(Chave de Ordenção)__ Data de validação.|

## Tabela Admin
Admin | (Administrador) |
-------- | --------|
created_at : String        | Data da criação do administrador. |
update_at: String          | Data da atualização do administrador. |
name: String             | Nome do administrador. |
estado: String |Estado do administrador.|
cidade: String |Cidade do administrador.|
bairro: String |Bairro do administrador.|
rua: String |Rua do administrador.|
numero: Number |Número da casa do administrador.|
cep: String |Código postal do administrador.|
cpf: String |Cadastro de pessoa física do administrador.|
uuid_admin : String :(PK)        | __(Chave Primária)__ Identificador único universal do administrador. |
uuid_client : String :(SI)        | __(Índice Secundário)__ Identificador único universal do cliente. |
email: String : String : (SI) | __(Índice Secundário)__ Email do administrador.|

## Tabela Circuit
Circuit | (Circuito) |
-------- | --------|
created_at : String        | Data da criação do circuito. |
update_at: String          | Data da atualização do circuito. |
timestamp: String          | Data da marca temporal. |
code_module: String          | Módulo do código do circuito. |  
has_relay: Boolean |Possui relé.|
relay_state: Boolean |Estado do relé.|
name: String            | Nome do circuito. |
description: String            | Descrição do circuito. |
uuid_circuit : String :(PK)        | __(Chave Primária)__ Identificador único universal do circuito. |
uuid_unity : String :(SI)        | __(Índice Secundário)__ Identificador único universal da unidade. |
uuid_group : String :(SI)        | __(Índice Secundário)__ Identificador único universal do grupo. |
uuid_client : String :(SI)        | __(Índice Secundário)__ Identificador único universal do cliente. |

## Tabela Household_appliances   
Household_appliances | (Eletrodomésticos) |
-------- | --------|
created_at : String |Data de criação do eletrodoméstico.|
update_at: String |Data de atualização do eletrodoméstico.| 
name: String |Nome do eletrodoméstico.|
label: String |Marca do eletrodoméstico.| 
hours: Number |Horas.|
potency: Number |Potência do eletrodoméstico.|
quantity: Number |Quantidade do eletrodoméstico.|
uuid_household_appliances : String : (PK) | __(Chave Primária)__ Identificador único universal do eletrodoméstico.|
uuid_circuit : String : (SI) | __(Índice Secundário)__ Identificador único universal do circuito. |
uuid_group : String : (SI) | __(Índice Secundário)__ Identificador único universal do grupo. |
uuid_unity : String : (SI) | __(Índice Secundário)__ Identificador único universal da unidade. |

## Tabela Unity
Unity | (Unidade) |
-------- | --------|
created_at : String        | Data da criação da unidade. |
update_at: String          | Data da atualização da unidade. |
name: String             | Nome da unidade. |
description: String            | Descrição da unidade. |
contracted_demand: Number |Demanda contratada da unidade.|  
tariff_data: Object |Informações da tarifa.|          
goal: Number |Objetivo da unidade|
tariff_period: Number |Período da tarifa.|
uuid_unity : String :(PK)        | __(Chave Primária)__ Identificador único universal da unidade. |
uuid_client : String :(SI)        | __(Índice Secundário)__ Identificador único universal do cliente. |

## Tabela Requisitions_scheduling
Requisitions_scheduling | (Agendamento de requisições) |
-------- | --------|
created_at : String        |Data da criação do agendamento da requisição.|
update_at: String          |Data da atualização do agendamento da requisição.|
action_requisition: String |Ação da requisição.|
index: Number |Índice do agendamento.|
state_requisition: String |Estado da requisição.|
type_requisition: String |Tipo da requisição.|
uuid_circuit: String |Identificador único universal do circuito.|
uuid_scheduling_group: String |Identificador único universal do grupo de agendamento.|
uuid_scheduling_module: String |Identificador único universal do módulo de agendamento.|
uuid_requisition_scheduling : String : (PK) | __(Chave Primária)__ Identificador único universal do agendamento da requisição. |

## Tabela Requisitions_actuation
Requisitions_actuation | (Atuação de requisições) |
-------- | --------|
created_at : String        |Data da criação da atuação da requisição.|
update_at: String          |Data da atualização da atuação da requisição.|
state_actuation: Boolean |Estado da atuação.|
type_requisition: String |Tipo da requisição.|
uuid_group: String |Identificador único universal do grupo.|
uuid_unity: String |Identificador único universal da unidade.|
uuid_requisition_actuation : String : (PK) | __(Chave Primária)__ Identificador único universal da atuação da requisição.|
uuid_circuit: String : (SI) |__(Índice Secundário)__ Identificador único universal do circuito.|
state_requisition: String : (OK) |__(Chave de Ordenação)__ Estado da requisição.|

## Tabela Scheduling_group
Scheduling_group | (Grupo de agendamento) |
-------- | --------|
created_at : String        |Data da criação do grupo de agendamento.|
update_at: String          |Data da atualização do grupo de agendamento.|
name: String |Nome do grupo de agendamento.|
action_relay: Boolean |Ação do relé.|
repeat: Boolean |Repetição.|
week: Object |Semana|
run_at: String |Horário de execução.|
uuid_scheduling_group : String : (PK) | __(Chave Primária)__ Identificador único universal do grupo de agendamento.|
uuid_unity: String : (SI) |__(Índice Secundário)__ Identificador único universal da unidade.|

## Tabela Scheduling_module
Scheduling_module | (Módulo de agendamento) |
-------- | --------|
created_at : String        |Data da criação do módulo de agendamento.|
update_at: String          |Data da atualização do módulo de agendamento.|
index: Number |Índice do agendamento.|
uuid_scheduling_module : String : (PK) | __(Chave Primária)__ Identificador único universal do módulo de agendamento. |
uuid_unity: String : (SI) |__(Índice Secundário)__ Identificador único universal da unidade.|
uuid_scheduling_group: String : (SI) |__(Índice Secundário)__ Identificador único universal do grupo de agendamento.|
uuid_circuit: String : (SI) |__(Índice Secundário)__ Identificador único universal do circuito.|
state: String : (OK) |__(Chave de Ordenação)__ Estado.|

## Tabela Flags
Flags | (Bandeiras) |
-------- | --------|
created_at : String        | Data da criação das bandeira. |
update_at: String          | Data da atualização das bandeira. |
flag: String             | Bandeira. |
value: Number          | Valor da bandeira. |
uuid_flag : String :(PK)        | __(Chave Primária)__ Identificador único universal da bandeira. |
year : Number :(SI)        | __(Índice Secundário)__ Ano. |
month: Number : (OK) |__(Chave de Ordenação)__ Mês.|

## Tabela Group
Group | (Grupo) |
-------- | --------|
created_at : String        | Data da criação do grupo. |
update_at: String          | Data da atualização do grupo. |
name: String             | Nome do grupo. |
goal: Number |Objetivo do grupo|
description: String           | Descrição do grupo. |
uuid_group : String :(PK)        | __(Chave Primária)__ Identificador único universal do grupo. |
uuid_unity : String :(SI)        | __(Índice Secundário)__ Identificador único universal da unidade. |

## Tabela Connections
Connections | (Conexões) |
-------- | --------|
update_at: String          | Data da atualização da conexão. |
domain_connection : String |Domínio da conexão|
uuid_connection : String :(PK) |__(Chave Primária)__ Identificador único universal da conexão|
uuid_group : String :(SI)        | __(Índice Secundário)__ Identificador único universal do grupo. |
uuid_unity : String :(SI)        | __(Índice Secundário)__ Identificador único universal da unidade. |
created_at : String : (OK)        | __(Chave de Ordenação)__ Data da criação . |

## Tabela Clients
Clients | (Clientes) |
-------- | --------|
created_at : String        | Data da criação do cliente. |
update_at: String          | Data da atualização do cliente. |
name: String             | Nome do cliente. |
description: String           | Descrição do cliente. |
uuid_cliente : String :(PK)        | __(Chave Primária)__ Identificador único universal do cliente. |

## Tabela Default_household...
Default_household_appliances | (Eletrodomésticos padrão) |
-------- | --------|
name: String |Nome do eletrodoméstico.|
label: String |Marca do eletrodoméstico.| 
potency: Number |Potência do eletrodoméstico.|
uuid_client : String :(PK)        | __(Chave Primária)__ Identificador único universal do cliente. |

## Tabela Notification
Notification | (Notificação) |
-------- | --------|
message : String                 | Mensagem da notificação. |
uuid_group : String :(PK)        | __(Chave Primária)__ Identificador único universal do grupo. |
sent : Boolean :(SI)             | __(Índice Secundário)__ Envio da notificação. |
## Tabela Alarms 
Alarms | (Alarmes) |
-------- | --------|
uuid_alarms : String :(PK)        | __(Chave Primária)__ Identificador único universal do alarme. |




