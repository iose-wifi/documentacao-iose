# Backup de medidas

A seguir serão expostas as definições de execução de backup de medidas no backend.

## Recepção de Medidas periódica com solicitação de índices perdidos (Fluxo principal)

No fluxo principal de recepção de medidas é verificado se a medida recebida (tópico: XXXXXX ) possui o índice na sequencia em relação a última medida, caso o índice esteja na sequencia nenhuma ação é tomada, caso contrário nos índices faltosos serão colocados sinalizações de medidas perdidas e em seguida serão solicitadas as medidas perdidas a cada device. A seguir exemplo do dado sinalizador de medida perdida.

```json
	{
		"index": 1, 
        "lost_measure": true,
        "absent_measures": false, 
        "contador":0
	}
```

### Fluxograma de recepção de medidas com solicitação de índices perdidos

![nova imagem](./imagens/FluxoPrincipalAquisicaoMedidas.png)

## Requisição de índices perdidos (Agendados)

Atráves do serviço <i>EventBright</i> da AWS será disparado a cada <b>8(oito) horas</b> uma  função <i>lambda</i> que reunirá os índices de medidas perdidas e solicitará para cada dispositivo.

### Fluxograma de requisição de índices perdidos

![nova imagem](./imagens/FluxoAquisicaoDeIndicesPerdidos.png)

## Recepção de índices perdidos

A recepção de índices perdidos será feita em um tópico exclusivo (tópico: XXXXXX ). O serviço <i>IoT Core Aws</i> fará um parser das medidas recebidas.

### Fluxograma de índices perdidos

![nova imagem](./imagens/RecepcaoIndicesPerdidos.png)
    
