# Arquitetura 
A seguir será exposto o diagrama da arquitetura geral do software e a descrição de cada componente  e sua função dentro da arquitetura.

![nova imagem](./imagens/diagrama.png)

## Aplicação (Amazon S3)
A aplicação Web e dados de backup da aplicação IoSE serão armazenados em um repositório Amazon S3 que proporcionará alta disponibilidade a custos baixos.

## Autenticação (AWS Cognitor)
O AWS cognitor é o resposável pelo login e segurança de acesso a nossa API respeitando as regras de usuários da aplicação. 

## API (Amazon Gateway)
A Amazon Gateway será resposável por disponibilizar os endpoints para o usuário e/ou aplicações previamente Atorizados.

## Triggers (AWS Lambda)
O AWS Lambda será responsável por todas as ações que tenham origem nos endpoints da API. A ações serão direcionada principalmente para manipulação de dados em banco de dados e atuações nos devices interagindo com o controlador de devices.

## Armazenamento (DynamoDB)
DynamoDb(NoSQL) será o banco utilizado para o armazenamendo de todos os dados da aplicação que serão divididos principalmente em duas camadas: Dados brutos dos devices e dados adaptados para o frontend IoSE.

## Controle de Devices (AWS IoT Core)
O AWS IoT Core é o resposável direto pela aquisição de dados e atuação no dispositivos através de tópicos. Esse serviço também é responsável por enviar dados para serviços de análise e integrações diretamente com os devices.

## Opcionais
Os serviços a seguir não serão incluídos na primeira fase da aplicação e por isso são considerados serviços opcionais.

### AWS IoT Analytics
AWS IoT Analytics é o serviço responsável por gerações de relatórios para aplicação de acordo com os modelos propostos.  

### HTTPS Downstream
Downstream é o serviço responsável por direcionar as informações vindas dos devices para um endpoint específico. O serviço poderá ser usado em uma possível integração.  