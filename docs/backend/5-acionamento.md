# Acionamento dos dispositivos

A seguir serão expostas as definições do software para acionamento dos dispositivos.

Diagrama Geral de acionamento

![nova imagem](./imagens/sequenciaAcionamento.png)

## Frontend

### Solicitação de acionamento do cliente

A aplicação do lado do cliente solicitará o acionamento via POST na rota { rota/acionamento } da API HTTP do backend com o pacote descrito abaixo:

```json
	{
		"uuid_circuit": "AABBCCDDEEFF",
		"type": "relay_action",
        "expect_state": true
	}
```
True:  Relé fechado
False: Relé aberto

### Recepção de reposta do backend

O software fará uma conexão WebSocket na rota específica da unidade para monitorar o retorno e também receberá o retorno da solicitação POST.



## Backend

### Solicitação de acionamento para o device

Fluxograma de ações do backend:

![nova imagem](./imagens/solicitacaoAcionamentoBackend.png)

#### Solicitação de acionamento ao circuito específico

O backend deverá criar uma requisição no banco de dados com o estado "aberto" e solicitar o acionamento no tópico específico do circuito seguindo o pacote de exemplo a seguir:


Pacote de solicitação de acionamento acionamento
```json
	{
		"uuid_req": "123456789",
		"type": 8, 
		"payload":
		{
			"state_actuation": false
		}
	}
```

O backend deverá aguardar o comando de retorno vindo do tópico específico do circuito. Caso o módulo envie a resposta o backend deverá verificar se o circuito realmente acionado

#### Recepção de resposta do circuito

O pacote de resposta esperado segue o exemplo a seguir:

```json
    {
        "uuid_req": "123456789",
        "uuid_circuit": "AABBCCDDEEFF",
        "succsess": "true",
        "relay_state": "true",
        "timestamp": "2020-09-23T18:54:12.000Z"
    }
```

O backend deverá verificar se o comando foi executado com sucesso e verificar se o relé está com o estado alvo.

#### Retorno de reposta para o cliente.

O backend deverá atualizar a requisição criada no banco de dados como "finalizada", retornar no tópico webSocket específico e retornar o POST requisitado pelo cliente.

