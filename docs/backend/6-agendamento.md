# Agendamento dos dispositivos

A seguir serão expostas as definições do software para acionamento dos dispositivos.

Diagrama Geral de acionamento

![nova imagem](./imagens/sequenciaAgendamento.png)

## Frontend

### Solicitação de criação de agendamento do cliente

O cliente montará a solicitação de agendamento com os circuitos desejados, a hora da ação e se haverá repetição nos dias da semana como ilustrado no exemplo a seguir:

Exemplo do pacote de criação de agendamento:
```json
	{
		"circuits": ["AABBCCDDEEF1", "AABBCCDDEEF2","AABBCCDDEEF3"],
		"name": 'Agendamento Exemplo',
		"uuid_unity": xxxxx-xxxxxxx-xxxxxxxx-xxxx-xxxxxxxxxx-xxxxx,
		"run_at": 1614974920, //"Fri Mar 05 2021 17:08:00 UTC-0300"
        "action_relay": True,
		"repeat": True,
		"week":{
			"sun": True,
			"mon": False,
			"tue": True,
			"wed": True,
			"thu": False,
			"fri": False,
			"sat": False			
		}
	}
```
*Action_relay => True:  Relé fechado

*Action_relay => False: Relé aberto


### Solicitação de update de agendamento do cliente

Na atualização do agendamento o frontend deverá enviar a identificação e os dados do agendamento. Similar ao cadastro de agendamento.

Exemplo do pacote de edição de agendamento:
```json
	{
		"uuid_sched": "xxxxx-xxxxxxx-xxxxxxxx-xxxx-xxxxxxxxxx-xxxxx",
		"circuits": ["AABBCCDDEEF1", "AABBCCDDEEF2","AABBCCDDEEF3"],
		"name": 'Agendamento Exemplo',
		"run_at": 1614974920, //"Fri Mar 05 2021 17:08:00 UTC-0300"
        "action_relay": True,
		"repeat": True,
		"week":{
			"sun": True,
			"mon": False,
			"tue": True,
			"wed": True,
			"thu": False,
			"fri": False,
			"sat": False			
		}
	}
```
### Solicitação de remoção de agendamento

Exemplo do pacote de remoção de agendamento:
```json
	{
		"uuid_scheduling_group": "xxxxx-xxxxxxx-xxxxxxxx-xxxx-xxxxxxxxxx-xxxxx"
	}
```

### Recepção de reposta do frontend

O frontend fará uma conexão WebSocket na rota específica de retorno para receber quais os circuitos foram agendados com sucesso e quais tiveram algum erro no processamento e atualizará a tela. Ao fim do timeout especificado pelo backend todos os agendamento não recebidos serão sinalizados como falha. Os agendamentos sinalizados com falha apresentarão uma opção de reenvio como ilustrado abaixo:

![nova imagem](./imagens/modalAgendamento.png)

*O fluxo de telas da funcionalidade de agendamento estarão na sessão fluxo de telas.

## Backend

### Fluxograma de ações do backend:

![nova imagem](./imagens/FluxogramaAgendamento.png)

### Segmentação, criação da requisição e envio do agendamento para os circuitos

A partir do pacote recebido do frontend o backend deve segmentar cada circuito, criar a requisição de cada circuito e montar o pacote específico para o dispositivo. O pacote a ser enviado está específicado na sessão de firmware.

![nova imagem](./imagens/segmentacaoAgendamento.png)


### Recepção da resposta do device

O backend deverá monitorar o tópíco de retorno e terá 3 tipos de ações possiveis.

<b>1- Caso o dispositivo responda a requisição de agendamento com sucesso</b>

Ações: Finaliza a requisição com sucesso, cria agendamento com sucesso e envia sucesso para o software via web socket. 

<b>2- Caso o dispositivo responda a requisição de agendamento com erro</b>

Ações: Finaliza a requisição com erro e envia erro para o software via web socket. 

Obs: Caso o erro retornado pelo dispositivo seja indisponibilidade de memória o usuário deverá excluir um agendamento já existente e reenviar o agendamento desejado.

<b>3- Caso o dispositivo não responda a requisição no timeout estabelecido</b>

Ações: Finaliza a requisição com erro, cria agendamento com estado pendente e envia erro por desconexão ao software via web socket


### Atualização de agendamento

<b>Fluxo principal</b>

O update de agendamento é dividido em 4 principais condicionais listadas abaixo:

<b> 1- Atualização de agendamento com mudança nos atributos do agendamento </b>

No caso de mudança nos atributos do agendamento (Hora do agendamento, repetição ou dia da semana), deverá ser enviado para todos os circuitos a alteração realizada.

<b> 2- Atualização de agendamento com inclusão de circuito no agendamento </b>

No caso em que a mudança seja apenas a inclusão de novos dispositivos no agendamento existente, deverá ser enviado o agendamento apenas para os dispositivos incluidos. Similar a um cadastro de um novo agendamento.

<b> 3- atualização de agendamento com remoção de circuito do agendamento </b>

No caso em que a mudança seja apenas a remoção de um dispositivo no agendamento existente, deverá ser enviado o comando de remoção de slot apenas para os dispositivos removidos.

Obs: Caso a mudança seja apenas o nome do agendamento a alteração deverá ser feita apenas no banco de dados, ou seja, nada será enviado aos dispositivos.

<b>Fluxo de exceção </b>

Caso algum dispositivo não responda sucesso na operação de atualização será mantido o agendamento para que o usuário possa tentar uma nova atualização em outro momento.

![nova imagem](./imagens/fluxogramaUpdateAgendamento.png)

### Delete de agendamento

<b>Fluxo principal</b>

Caso o usuário remova um agendamento, deverá ser enviado para todos os dispositivos contidos naquele agendamento o comando de remoção de slot. 

<b>Fluxo de exceção </b>

Caso algum dispositivo não responda sucesso na operação de remoção será mantido o agendamento para que o usuário possa tentar uma nova remoção em outro momento.

### Evendo de reenvio de agendamentos pendentes (Caso exista agendamento pendente)

O backend deverá monitorar o tópico de <i>startup</i> dos dispositivos (</i>dev/iose/general/device_start</i>) e a cada mensagem de startup deverar verificar se existe algum agendamento pendente para aquele circuito.

![nova imagem](./imagens/responseAgendamento.png)


