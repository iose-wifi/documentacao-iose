# Notificações

O sistema de notificações é feito utilizando o [Firebase](https://firebase.google.com/), o backend necessita de um json gerado através do firebase com as keys de acesso ao sistema como apresentado na figura abaixo. Caso seja necessário a modificação de dados do firebase é necessária a geração de um novo json e substituir este nesta página. Além disso tem que existir a configuração no aplicativo também para receber as notificações adequadamente.

![backup](./imagens/deploy/5.png)

## O processo de notificações

Com o firebase configurado de forma correta o processo de notificação é então iniciado.

- 1 - O usuário faz login no aplicativo.
- 2 - Após o login é enviada uma requisição para adicionar esse usuário em uma tabela no Dynamo com os dados da unidade que ele pertence.
- 3 - Sempre que é necessário notificar uma unidade, verifica-se na tabela de dispositivos quais pertencem a esta, pega o identificador do celular e então envia a mensagem.
