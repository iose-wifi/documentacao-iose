# Backend

Para o sistema foi proposto uma arquitetura sem servidor (Computação sem servidor) e autoscaling. A arquitetura sem servidor nos traz como principais vantagens pagar somente pelo que usar (baixo custo) com alta disponibilidade e redução no tempo de desenvolvimento e manutenção. O autoscaling nos proporciona a adequação dos recursos da arquitetura de forma rápida e sem intervenção de equipe de manutenção.

![nova imagem](./imagens/diagrama.png)
