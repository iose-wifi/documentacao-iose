# Testes Automatizados- Crição e Manutenção

A criação e manutenção dos testes automatizados é feita a partir de um processo com o Selenium IDE, então aqui será explicado por passos uma forma de fazer tanto a criaçã e a manutenção de arquivos de testes que não funcionam mais e atualiza-los ao repositório com execução de sucesso novamente. 

1) Primeiro se não tiver o programa Selenium IDE tem que baixa-lo, Site Oficial: https://www.selenium.dev/downloads/
2) Na pagina de downloads, procure a parte que contem o Selenium IDE.
![nova imagem](./imagens/seleniumDownload.png)
3) Clicando na opção desejada, você vai poder adicionar o Selenium como extensão do seu navegador.
![nova imagem](./imagens/seleniumExt.png)
4) Pronto, depois disso o programa já está pronto para uso.
5) Acesse o campo de extensões do navegador e clique no Selenium IDE para iniciar o programa.
![nova imagem](./imagens/seleniumBarra.png)
6) Selecione "Create a new project" para realizar um novo script de teste automatizado ou selecione "Open an existing project" para abrir um documento com um script de teste já pronto.
![nova imagem](./imagens/seleniumInit.png)

PARTES ESSENCIAIS DA FERRAMENTA:
![nova imagem](./imagens/seleniumMenu.png)
- 1 - Barra de Ferramentas: Possui as funcionalidades que gerenciam os testes como gravar, executar, pausar, etc.
- 2 - Lista de Casos de Teste: Lista dos casos de teste que compõem a suíte de testes de uso.
- 3 - Editor de Script: Espaço para editar o script do caso de teste selecionado, podendo definir o step onde o teste irá iniciar ou parar.
- 4 - Rodapé: Log da execução do caso de teste selecionado.

OBS: Para os videos de demonstrações a seguir foi criado um canal o youtube com uma conta Nepen para armazenar esses conteudos e manter privados. Vou deixar disponivel o login da conta email: nepenteste@gmail.com senha: iosedev1!

7) Nomeie o arquivo de teste, depois insira a url do site que deseja ser testado, clique no botão de gravar ele abrirá o site que você inseriu, então faça o teste manual no site que foi aberto pelo programa e depois de terminado pare a gravação para que o bot grave todos os passos que você realizou manualmente e depois possa reproduzir.
[Link Video Demonstração Gravar Teste](https://youtu.be/ekJQOF70wuo)
8) Após ter gravado, você reproduz o teste para verificar se o bot está executando o teste tudo certo como deveria. 

OBS: ANTES DE RODAR UM TESTE COLOQUE TIRE A VELOCIDADE DO MAXIMO* 
(alguns testes dão falha, devido a velocidade estar muito rápida,coloque pelo menos na metade da velocidade.)<br />
[Link Video Demonstração Reproduzir Teste](https://youtu.be/BUEk3N8XN_Q)

9) Como a função de gravar o script é automática alguns testes dão falha devido os elementos capturados pelo bot não serem exatos as vezes, o sistema avisa em forma de "warning", ou simplesmente quebra em determinado comando, selecione o passo que está com warning ou erro e faça o ajuste manualmente pelo editor de script no campo de target e selecione outra alternativa que é dada pelo proprio programa do Selenium IDE, ou use a opção "select target in page" e selecione o elemento especifico para o programa registrar e assim adequar o teste e rodar com sucesso.<br />
[Link Video Demonstração Editar Teste](https://youtu.be/Lca7Hq9Acm8)
10) Depois de concluir que o teste está funcionando corretamente pela reprodução do Selenium, você seleciona o arquivo de teste gravado com o botão direito do mouse ou nos três pontos ao lado do nome do teste, irá mostrar algumas opções selecione export e escolha a opção de linguagem "Java JUnit" e por fim clique em export e salve o arquivo no diretório onde estão os outros testes do repositório "iose-test-selenium".
11) Com o novo arquivo já estando no repositório, a única parte que terá utilitade será a parte das linhas de comando do driver no campo @Test, pelo fato da estrutura dos arquivos dos testes que já existem no repositório estarem corretas e não precisam ser alteradas, então tanto para criação quanto manutenção o arquivo seguirá a mesma estrutura padrão dos outros, então o necessario é só copiar as linhas de comando do driver do novo teste gravado e organizar essas linhas de comando.

OBS: a parte @Test é a única que tem que ser alterada, mas mantendo do código antigo as linhas porque são referentes ao login já que toda vez que o teste se inicia o bot precisa logar no sistema, então esse bloco de codigo abaixo deve ser inserido em todos os arquivos de teste:
```
 @Test
  public void editarQuadro() throws InterruptedException {
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    driver.get("https://dev.d2f7jrl4zb2v5t.amplifyapp.com/global/super");
    driver.manage().window().setSize(new Dimension(1360, 728));
    driver.findElement(By.name("email")).sendKeys("iosedesenvolvimento@gmail.com");
    driver.findElement(By.name("email")).sendKeys(Keys.ENTER);
    driver.findElement(By.cssSelector(".Mui-error:nth-child(2)")).click();
    driver.findElement(By.name("password")).sendKeys("iosedev2!");
    driver.findElement(By.name("password")).sendKeys(Keys.ENTER);
    Thread.sleep(7000);
```  

OBS: Estrutura correta a ser seguida:

package br.com.selenium;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.PageLoadStrategy;
import java.util.concurrent.TimeUnit;
import java.net.MalformedURLException;
import io.github.bonigarcia.wdm.WebDriverManager;
public class EditarQuadroTest {
  private static WebDriver driver;
  static JavascriptExecutor js;
  @BeforeAll
  static void setUp() throws MalformedURLException {
    WebDriverManager.chromedriver().setup();

    ChromeOptions options = new ChromeOptions();
    options.addArguments("start-maximized");
    options.addArguments("--disable-infobars");
    options.addArguments("--disable-extensions");
    options.addArguments("chrome.switches", "--disable-extensions");
    options.addArguments("--disable-gpu");
    options.addArguments("--disable-dev-shm-usage");
    options.addArguments("--no-sandbox");
    options.addArguments("--disable-notifications");
    options.addArguments("--disable-popup-blocking");
    options.addArguments("enable-automation");
    options.addArguments("--disable-dev-shm-usage");
    options.addArguments("--disable-browser-side-navigation");
    options.addArguments("--dns-prefetch-disable");
    options.setPageLoadStrategy(PageLoadStrategy.NONE);
    options.setExperimentalOption("useAutomationExtension", false);  
 
    driver = new ChromeDriver(options);
 
    js = (JavascriptExecutor) driver;   
  }

  @Test
  public void editarQuadro() throws InterruptedException {
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    driver.get("https://dev.d2f7jrl4zb2v5t.amplifyapp.com/global/super");
    driver.manage().window().setSize(new Dimension(1360, 728));
    driver.findElement(By.name("email")).sendKeys("iosedesenvolvimento@gmail.com");
    driver.findElement(By.name("email")).sendKeys(Keys.ENTER);
    driver.findElement(By.cssSelector(".Mui-error:nth-child(2)")).click();
    driver.findElement(By.name("password")).sendKeys("iosedev2!");
    driver.findElement(By.name("password")).sendKeys(Keys.ENTER);
    Thread.sleep(7000);
    [INSERIR AS NOVAS LINHAS DE COMANDO DE DRIVER NESSE ESPAÇO, APÓS AS LINHAS DE LOGIN DO BOT NO SISTEMA]
    }
    @AfterAll
    static void tearDown() throws InterruptedException {
    Thread.sleep(5000);
      driver.quit();
    }
}

12) Depois da substituição do código ter sido feita você executa o teste pelo repositório e verifica seu fucionamento, as vezes o teste pode falhar devido à um elemento necessário  não ter carregado à tempo, para isso pode adicionar uma linha de código com o comando "Thread.sleep(5000);"antes da linha de comando que está causando a falha, para realizar uma pausa no bot, (5000 = 5 segundos) podendo ser alterado pro tempo que achar necessário, ou se o teste falhar devido o não reconhecimento de um elemento específico realize a manutenção do comando com falha de acordo com o "passo (9)" e siga o processo até conseguir executar o teste automatizado em java pelo repositório local.
13) Certificando o funcionamento com sucesso do teste pelo repositório local é só fazer um push das alterações para o repósitorio geral.

OBS: FIZ MAIS DOIS VIDEOS PARA ORIENTAÇÃO
Video do processo de gravação do teste, conversão para linha de codigo e adaptação na estrutura certa para rodar os testes: 
<br />
[Link Video Demonstração Reproduzir Teste](https://youtu.be/wNkhXikCZbQ) 

Video do processo de selecionar outras alternativas de um mesmo elemento caso o bot não esteja encontrando um elemento por não estar bem especifico causando erro do teste, então a solução é escolher uma alternativa do mesmo elemento que selecionando o comando onde esta mostra as outras opções, substituindo o bot pode achar a nova alternativa ja que ão achava a antiga, no caso mostrado no video um elemento seria "css=.Mui-focused > .MuiSelect-root" mas esse mesmo elemento tambem pode ser reconhecido assim "xpath=//main[@id='mainContent']/section/div[2]/div[2]/section/form/section/div/div[2]/div/div/div"

<br />
[Link Video Demonstração Elementos Driver](https://youtu.be/1BovhbtAfmE) 


OBS: O DRIVER AS VEZES GRAVA AÇÕES DESNECESSARIAS PARA O TESTE QUE IMPLICA EM ERROS FUTUROS, COMO EXEMPLO (O QUE APARECE COMENTADO NÃO ERA NECESSARIO PARA O TESTE E SÓ PODE TRAZER ERROS, ESSES SÃO EXEMPLOS QUE APARECEM BASTANTE AO DRIVER GRAVAR O SCRIPT E SÃO DESCARTAVEIS, JEITO DE LIDAR COM ISSO É COMENTANDO O QUE ACHA QUE É DESNECESSARIO E EXECUTAR O TESTE VER SE FUNCIONA COM SUCESSO E IR EXCLUINDO ESSES COMANDOS DESNECESSARIOS):
 // {
    //   WebElement element = driver.findElement(By.cssSelector(".MuiTableCell-root:nth-child(2) > .jss480 > .MuiButtonBase-root"));
    //   Actions builder = new Actions(driver);
    //   builder.moveToElement(element).perform();
    // }
    // {
    //   WebElement element = driver.findElement(By.tagName("body"));
    //   Actions builder = new Actions(driver);
    //   builder.moveToElement(element, 0, 0).perform();
    // }
    // {
    //   WebElement element = driver.findElement(By.cssSelector(".jss525"));
    //   Actions builder = new Actions(driver);
    //   builder.moveToElement(element).perform();
    // }
    // {
    //   WebElement element = driver.findElement(By.tagName("body"));
    //   Actions builder = new Actions(driver);
    //   builder.moveToElement(element, 0, 0).perform();
    // }
    driver.findElement(By.xpath("(//button[@type=\'button\'])[6]")).click();
    // driver.findElement(By.cssSelector(".jss526")).click();