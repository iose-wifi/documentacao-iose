# Ações do IOT

treh ksdfj s

## Tópico de cadastro

## Tópico de medidas

Os módulos enviam medidas atravésde um tópico mqtt, para economia de memória no módulo foi necessário que o nome dos parâmetros enviados
fossem o mais curto possível. Foi necessário um mapeamento entre esses nomes enviados pelo tópico qmtt e o nome que receberão
ao serem salvos na base de dados, esses valores podem ser acompanhados na tabela a seguir.

| Chega no tópico  |       Salva no banco 			| Descrição 									| Tipo      |
|------------------|--------------------------------|-----------------------------------------------|-----------|
|  **a**           |       -             			| Id único do módulo. 							| String.	|
|  **b**           |       voltage             		|Tensão RMS da fase medida em Volt. 			| Float.	|
|  **c**           |       line_frequency           | Frequência da rede elétrica medida em Hertz. 	| Float.	|
|  **d**           |       thermistor_voltage       | Temperatura interna do Módulo. 				| Float.	|
|  **e**           |       power_factor             | Fator de potência lido pelo sensor. 			| Float.	|
|  **f**           |       current             		| Corrente RMS medida em Ampere. 				| Float.	|
|  **g**           |       active_power             | Potência ativa lida em Watt. 					| Float.	|
|  **h**           |       reactive_power           | Potência reativa lida em VAr. 				| Float.	|
|  **i**           |       apparent_power           | Potência aparente lida em VA. 				| Float.	|
|  **j**           |       import_active_energy     | Energia ativa consumida lida em Wh. 			| Double.	|
|  **k**           |       export_active_energy     | Energia ativa exportada lida em Wh. 			| Double.	|
|  **l**           |       import_reactive_energy   | Energia reativa consumida lida em Wh. 		| Double.	|
|  **m**           |       export_reactive_energy   | Energia reativa exportada lida em Wh. 		| Double.	|
|  **n**           |       -       					| Valor do System Status Register do MCP. 		| Uint16.	|
|  **o**           |       relay_state              | Estado atual do relé. 						| Boolean.	|
|  **p**           |       creat_at             			| Hora em que o payload foi criado. 			| String.	|

É possível ver na tabela que alguns dados não são salvos, no caso o parâmetro __a__, que representa o id único de cada módulo, é combinado
com o parâmetro __p__ para forma o identificador que será salvo na tabela. Temos então que o parâmetro __uuid_circuit__ no banco será uma concatenação dos valores
dos parâmetros __a__ e __p__ que chegaram no tópico, como mostrado em resumo na tabela a seguir.

|Coluna no banco    | valor      |
|-------------------|------------|
|uuid_circuit       | concat(p,a)|