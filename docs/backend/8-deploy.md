# Deploy

Aqui são descritos os procedimentos necessários para a realização de um deploy em produção. É importante destacar que todos os deploys devem ser feitos em sicronia entre os projetos de front, aplicativo, backend e firmware. Alguns processo do deploy são descritos a seguir.

## Backup de tabelas

Antes de realizar o deploy para a produção é necessário que sejam feitos os backups das tabelas do Dynamo. Isso deve ser feito diretamente pelo console da Amazon. O primeiro passo deve ser ir na aba backup do DynamoDB.

![backup](./imagens/deploy/1.png)

Ir em criar backup.

![backup](./imagens/deploy/2.png)

Após isso é escolher uma tabela e escolher o nome que o backup terá. Esse processo deverá ser realizado para todas as tabelas existentes no dynamo.

![backup](./imagens/deploy/3.png)

## Deploy do backend via Gitlab

Uma das maneiras de fazer o deploy é através do deploy contínuo existente na branch master, isso pode ser feito através do botão apresentado na figura a seguir. O botão fica disponível após uma merge request ou commit ser feito para a branch master e o CI ser realizado com sucesso.

![backup](./imagens/deploy/4.png)

## Deploy do backend via linha de comando

Outra forma de fazer o deploy é através do deploy pela própria linha de comando do serverless framework, uma descrição de como utilizar essa ferramenta para deploy pode ser encontrada [aqui](https://www.serverless.com/framework/docs/providers/aws/cli-reference/deploy/)

## Deploy do Front via Gitlab

Para que o front seja mandado para a produção basta que a modificação seja enviada para a branch master, o Amplify da AWS pega as modificações automaticamente dessa branch e já disoponibiliza para o usuário.
