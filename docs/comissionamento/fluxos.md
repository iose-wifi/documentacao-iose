# Fluxos

A seguir serão apresentados os fluxos para o a nova abordagem de comissionamento dos módulos IoSE. 

## 1 - Fluxo na Visão do Instalador 

Esse fluxo é referente a todo o processo que o instalador realizará para que o comissionamento do(s) módulo(s) ocorra corretamente.<br></br>

<div style='text-align:center;'>

![DiagramaComissionamentoInstalador](./imagens/DiagramaComissionamentoInstalador.png)

</div>

<p style='text-align:center;'>Fluxo visão do <b>Instalador</b></p><br></br>

### 1.1 Descrição do Fluxo

</br>

1.1.1 Fluxo Principal

| Passos | Descrição | 
|--------|-----------|
|   1    |  Instalação dos dispositivos em série com os circuitos pertencentes ao quadro do cliente   |
|   2    |  O instalador realiza o login no app IoSE Connect com a conta de administrador do cliente  |
|   3    |  Instalador entra na [Tela de Escaneamento](prototiposTelas.html#tela-de-escaneamento) e nomeia o módulo a escaneado |
|   4    |  Scaneia o QR Code do módulo  |
|   5    |  Após escanear todos os módulos, o instalador insere os parâmetros de conexão de rede listado na descrição da [Tela de Configuração](prototiposTelas.html#tela-de-configuracao) |
|   6    |  Inicia o processo de configuração onde as informações serão gravadas em cada um dos módulos |
|   7    |  O sistema finaliza processo e retorna estado final de configuração do módulo |

</br>

1.1.2 Fluxo Alternativo: Não foram escaneado todos os módulos

| Passos | Descrição | 
|--------|-----------|
|   4.1  |  O instalador volta para o passo 3 do fluxo principal    |

</br>

1.1.3 Fluxo Alternativo: Algum módulo não configurado com sucesso

| Passos | Descrição | 
|--------|-----------|
|   6.1  |  O instalador poderá reiniciar o processo para que o sistema tente configurar novamente os módulos que sinalizaram falha, voltando par o passo 6 do fluxo principal |

</br>

## 2 - Fluxo na Visão do Sistema 

Esse fluxo é referente a todo o processo realizado pelo sistema do app IoSE Connect para que o comissionamento do(s) módulo(s) ocorra corretamente.<br></br>

<div style='text-align:center;'>

![DiagramaComissionamentoSistema](./imagens/DiagramaComissionamentoSistema.png)

</div>

<p style='text-align:center;'>Fluxo visão do <b>Sistema</b></p><br></br>

### 1.1 Descrição do Fluxo

</br>

1.1.1 Fluxo Principal

| Passos | Descrição | 
|--------|-----------|
|   1    |  Usuário é autenticado pelo cognito do cliente já existente |
|   2    |  O sistema realiza uma requisição para retornar todas a informações do cliente |
|   3    |  Usuário escaneia <i>QR Code</i> do módulo a ser configurado|
|   4    |  Usuário insere um nome para a identificação do módulo escaneado|
|   5    |  Sistema realizar requisição retornando os dados do módulo  |
|   6    |  O nome do módulo é modificado na base de dados |
|   7    |  Após o escaneamento de todos os módulos, o usuário insere os parâmetros de rede a serem configurados |
|   8    |  Usuário inicia o processo de configuração, onde os parâmetros fornecidos serão enviados módulo a módulo |
|   9    |  O sistema é desconectado da internet e conecta-se a um <i>hostAP</i> da lista de dispostivos escaneados  |
|   10   |  O sistema envia os parâmetros de rede para o dispositivo e aguarda sua resposta |
|   11   |  O sistema recebe resposta de sucesso da operação e caso não possua mais dispositivos a serem configurados, notifica usuário e finaliza processo |

</br>

1.1.2 Fluxo Alternativo: Não foram escaneado todos os módulos

| Passos | Descrição | 
|--------|-----------|
|   6.1  |  O usuário volta para o passo 3 do fluxo principal    |

</br>

1.1.3 Fluxo Alternativo: Módulo não configurado com sucesso e número de tentativas menor igual a 3

| Passos | Descrição | 
|--------|-----------|
|   10.1 |  Sistema reenvia os parâmetros de rede para o dispositivo |

</br>

1.1.4 Fluxo Alternativo: Módulo não configurado com sucesso e número de tentativas maior que 3

| Passos | Descrição | 
|--------|-----------|
|   10.2 |  Sistema notifica o falha na configuração do módulo ao usuário |

</br>

1.1.4 Fluxo Alternativo: Existem mais dispositivos a serem configurados

| Passos | Descrição | 
|--------|-----------|
|   11.1 |  Sistema volta para o passo 9 do fluxo principal, desta vez conectando a outro <i>hostAP</i> da lista de dispostivos escaneados |





