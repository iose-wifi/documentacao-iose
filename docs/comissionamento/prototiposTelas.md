# Protótipos de telas

A seguir serão apresentadas as telas prototipadas na ferramenta **Figma** referente ao app **IoSE Connect** necessário para comissionar os módulos na nova abordagem. </br>Em resumo, o app consistirá de três ambientes: [Tela de Lista de Módulos](prototiposTelas.html#tela-de-lista-de-modulos), [Tela de de Configuração](prototiposTelas.html#tela-de-configuracao) e [Tela de execução](prototiposTelas.html#tela-de-execucao), cada uma poderá ser acessada através do **AppBar** na parte inferior do app. </br>
Vale ressaltar que o **botão de escanear módulo** será fixo no AppBar, de modo que poderá ser acessado de qualquer ambiente, medida adota por tratar-se da principal ação do app.</br></br>


## Tela de Login

Tela onde será realizada a autenticação do usuário/instalador por meio de credenciais a ele cedidas. Tais credenciais são referente a conta de administrador do cliente presente no Cognito AWS.<br></br>

<div style='text-align:center;'>

![Login](./imagens/TelaLogin.png)

</div>
<p style='text-align:center;'>Figura 1: Tela de Login</p>



## Tela de Lista de Módulos

Tela inicial do app, que apresentará todos os módulos que foram escaneados. Cada módulo listado poderá ter seu nome editado ou poderá ser removido da lista. Em cada linha é mostrado o nome do módulo, seu MAC e estado de conexão com o wifi que poderá ser:

 - **Não Conectado**: O módulo ainda não passou pelo processso de configuração ou ocorreu uma falha e não foi conectado;
 - **Conectado**: O módulo passou pelo processso de configuração e foi conectado;
 - **Conectando**: O módulo está em processo de configuração;<br></br> 

<div style='text-align:center;'>

![TelaListaModulos](./imagens/TelaListaModulos.png)

</div>
<p style='text-align:center;'>Figura 2:  Tela de Lista de Módulos</p>

## Tela de Escaneamento

Tela que será apresentada quando o usuário clicar no **botão de escanear módulo**, nela a câmera do celular do usuário será acionada prossibilitando o escaneamento do módulo, também terá um campo para que possa ser inserido o seu nome.

<div style='text-align:center;'>

![TelaCaptura.png](./imagens/TelaCaptura.png)

</div>
<p style='text-align:center;'>Figura 3:  Tela de Escanemento</p>

## Tela de Configuração

Tela que irá conter o formulário para que os parâmetro de rede sejam inserido, eles serão gravados no módulo no processo de configuração. Os campos presente no formulário são:

- **SSID (Service Set Identifier)**: Nome da rede do cliente;
- **Password**: Senha da rede do cliente;
- **Switch de IP Estático**: Switch pra indicar se o ip será estático ou dinâmico (DHCP), caso não seja acionado os campos a seguir não serão visualizados;
- **Endereco de IP**: Ip estático do cliente a ser configurado no módulo;
- **Máscara de rede**: Máscara de IP da rede do cliente;
- **Gateway**: Gateway da rede do cliente;
- **DNS primário**: DNS primário da rede do cliente;
- **DNS secundário**: DNS secundário da rede do cliente;</br></br>

<div style='text-align:center;'>

![TelaConfiguracoes](./imagens/TelaConfiguracoes.png)

</div>

## Tela de Execução

Tela que possibilitará o usuário a começar o processo de configuração de todos módulos escaneados de forma automática. O usuário também poderá, caso necessário, parar a operação. <br>
Ao iniciar o processo, será listado todos os módulos com o seu respectivo nome, MAC e estado do qual indicará se a configuraçao foi bem sucedida ou não.


<div style='text-align:center;'>

![TelaConexao](./imagens/TelaConexao.png)

</div>

<p style='text-align:center;'>Figura 4: Tela de Execução</p>

Para mais detalhes das telas do : [Protótipo das telas Figma](https://www.figma.com/file/hTUgYqQIJQbtzQ5wlnc4Rz/App-Comissionamento-IoSE?node-id=2%3A10844)