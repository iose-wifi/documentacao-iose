# Novo Comissionamento

Nessa sessão serão apresentados todos fluxos referentes a nova abordagem de comissionamento dos módulos, em relação a **visão do instalador** e a **visão do sistema**. O novo comissionamento terá um app para que o processo seja devidamente realizado e também, nessa seção, serão apresentados os mockups de telas desse app, intitulado como **IoSE Connect**.