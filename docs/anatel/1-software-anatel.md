﻿# Software de certificação
Para realizar a certificação do ESP32 pela Anatel serão utilizados software e firmware fornecidos pela própria fabricante do microcontrolador,que já possui uma interface completa para configurar o microcontrolador com os parâmetros de cada teste a ser feito em laboratório.
O documento completo contendo a documentação desse software está disponível nesse [link.](./imagens/ESP32ESP8266_RF_Performance_Test_Demonstration__EN.pdf)

## Primeiros passos
Para conectar a placa a ser testada em um computador,basta plugar um cabo micro USB na placa e ligar à uma porta USB no computador. Se necessário,plugar os equipamentos de medição no conector SMA da placa.
## Interface do programa de teste

### Configurar a porta serial
![imagem 1](./imagens/imagem1.jpg)
- Escolha ESP32 para ChipType
- Escolha corretamente a porta **COM** em que o ESP32 está conectado(no caso o ESP32 está conectado na **COM27**)
- Configure o BaudRate para **115200**
- Mude o estado da porta serial para aberta clicando no sinal de play

### Gravar firmware
![imagem 11](./imagens/imagem12.jpg)<br>
Após ter configurado corretamente a porta serial:
- Aperte nos 3 pontinhos para escolher o firmware a ser gravado, e escolha o firmware ESP32_RF_TEST_V1_40M.
- Ao lado,selecione para gravação em memória flash.
- Por último,aperte no botão **load bin** para fazer a gravaçao

## Teste de Rádio Frequência
### WiFi
![imagem 2](./imagens/imagem3.jpg)
<br>Para iniciar o teste de WiFi basta selecionar a aba **wifi Test**. Dentro dessa aba terão os parâmetros:
- Test Mode: Colocar na opção TX continues para entrar no modo de transmissão contínua.
- WiFi Rate: Selecionar qual padrão de WiFi será utilizado, B,G ou N,com suas respectivas taxas de transmissão.
- BandWith: Selecionar se a largura de banda será de 20 ou 40MHz,utilizar para o teste no padrão N20 e N40.
- Channel: Selecionar qual canal será feito o teste de WiFi,sendo o canal inicial 1 de 2412MHz,canal central 6 de 2437MHz e canal final 11 de 2462MHz.

Para começar o teste basta configurar os parâmetros de acordo com as necessidades do teste e apertar no botão de **start**,para parar basta apertar o botão à direta de **stop**.

#### Teste do padrão 802.11B
![imagem 3](./imagens/imagem4.jpg)<br>
Para começar o teste do padrão 802.11B preencha os parâmetros:
- Test Mode em **TX continue**
- WiFi Rate em **11b 11M**
- BandWidth em **20M**
- Channel em **1/2412** para **Canal Inicial** , **6/2437** para **Canal Central** e **11/2462** para **Canal Final**.

Após os parâmetros serem configurados,aperte o botão **start** para começar o teste, e **stop** para finalizar.

#### Teste do padrão 802.11G
![imagem 4](./imagens/imagem5.jpg)<br>
Para começar o teste do padrão 802.11G preencha os parâmetros:
- Test Mode em **TX continue**
- WiFi Rate em **11g 54M**
- BandWidth em **20M**
- Channel em **1/2412** para **Canal Inicial** , **6/2437** para **Canal Central** e **11/2462** para **Canal Final**.

Após os parâmetros serem configurados,aperte o botão **start** para começar o teste, e **stop** para finalizar.

#### Teste do padrão 802.11N20/N40
![imagem 5](./imagens/imagem6.jpg)<br>
##### Para começar o teste do padrão 802.11N20 preencha os parâmetros:
- Test Mode em **TX continue**
- WiFi Rate em **11n MCS7**
- BandWidth em **20M**
- Channel em **1/2412** para **Canal Inicial** , **6/2437** para **Canal Central** e **11/2462** para **Canal Final**.

Após os parâmetros serem configurados,aperte o botão **start** para começar o teste, e **stop** para finalizar.

![imagem 6](./imagens/imagem7.jpg)<br>
##### Para começar o teste do padrão 802.11N40 preencha os parâmetros:
- Test Mode em **TX continue**
- WiFi Rate em **11n MCS7**
- BandWidth em **40M**
- Channel em **3/2422** para **Canal Inicial** , **6/2437** para **Canal Central** e **11/2462** para **Canal Final**.

Após os parâmetros serem configurados,aperte o botão **start** para começar o teste, e **stop** para finalizar.

### Bluetooth
![imagem 7](./imagens/imagem8.jpg)<br>
Para iniciar o teste de Bluetooth basta selecionar a aba **BT Test**. Dentro dessa aba terão os seguintes parâmetros:
- Test mode: Seleciona o protocolo do Bluetooth,sendo **BT TX** o Bluetooth EDR e **BLE TX** o Bluetooth BLE.
- Data Rate: Seleciona o padrão de modulação,sendo a opção **1M** para padrão GFSK,**3M** para padrão 8DPSK.
- Power Level: Seleciona a potência de transmissão, sendo a opção **8** a máxima potência do Bluetooth.
- Channel: Seleciona qual canal será feito o teste de Bluetooth,sendo o canal inicial 0 de 2402MHz,canal central 39 de 2441MHz e canal final 78 de 2480MHz.
- Hoppe: Seleciona se o salto entre frequências será habilitado ou não,sendo **Yes** para habilitado e **No** para desabilitado.

**OBS:** O Date Rate possui três informações:
- Padrão de modulação: Podendo ser **1M** para **GFSK** e **3M** para **8DPSK**.
- Tipo de frame: Podendo ser **DH1**,**DH3** ou **DH5**.
- Payload a ser enviado: Podendo ser payload de bits **1010** , **00001111** ou  **prbs9**.

#### Teste do padrão EDR GFSK
![imagem 8](./imagens/imagem9.jpg)<br>
Para começar o teste do padrão EDR GFSK preencha os parâmetors:
- Test Mode em **BT TX**.
- Power Level em **8**.
- Channel em **0/2402** para **Canal Inicial** , **39/2441** para **Canal Central** e **78/2480** para **Canal Final**.
- Hoppe em **Yes** para habilitar saltos de frequência ou **No** para desabilitar saltos de frequência.
- Date Rate em **1M** , podendo selecionar o tipo de frame e payload enviado.

#### Teste do padrão EDR 8DPSK
![imagem 9](./imagens/imagem10.jpg)<br>
Para começar o teste do padrão EDR 8DPSK preencha os parâmetors:
- Test Mode em **BT TX**.
- Power Level em **8**.
- Channel em **0/2402** para **Canal Inicial** , **39/2441** para **Canal Central** e **78/2480** para **Canal Final**.
- Hoppe em **Yes** para habilitar saltos de frequência ou **No** para desabilitar saltos de frequência.
- Date Rate em **3M** , podendo selecionar o tipo de frame e payload enviado.

#### Teste do padrão BLE GFSK
![imagem 10](./imagens/imagem11.jpg)<br>
Para começar o teste do padrão BLE GFSK preencha os parâmetors:
- Test Mode em **BLE TX**.
- Power Level em **8**.
- Channel em **0/2402** para **Canal Inicial** , **39/2441** para **Canal Central** e **78/2480** para **Canal Final**.
- Date Rate em **LE** , podendo selecionar o payload enviado.

## Teste EMC/EMI
### WiFi
![imagem 12](./imagens/imagem13.jpg)<br>
Para o teste WiFi de EMC/EMI,basta configurar os parâmetros da seguinte maneira:
- Test Mode em **TX continue**
- WiFi Rate em **11n MCS7**
- BandWidth em **40M**
- Channel em **11/2462**.

Após os parâmetros serem configurados,aperte o botão **start** para começar o teste, e **stop** para finalizar.

### Bluetooth

#### Bluetooth EDR
![imagem 13](./imagens/imagem14.jpg)<br>
Para o teste Bluetooth EDR de EMC/EMI basta configurar os parâmetros da seguinte maneira:
- Test Mode em **BT TX**.
- Power Level em **8**.
- Channel em **78/2480**.
- Date Rate em 3M_DH5_1010.
- Hoppe em **No**.

Após os parâmetros serem configurados,aperte o botão **start** para começar o teste, e **stop** para finalizar.



#### Bluetooth BLE
![imagem 14](./imagens/imagem15.jpg)<br>
Para o teste Bluetooth BLE de EMC/EMI basta configurar os parâmetros da seguinte maneira:
- Test Mode em **BLE TX**.
- Power Level em **8**.
- Channel em **39/2480**.
- Date Rate em LE_1010.
- Syncw em **0x0**.

Após os parâmetros serem configurados,aperte o botão **start** para começar o teste, e **stop** para finalizar.


