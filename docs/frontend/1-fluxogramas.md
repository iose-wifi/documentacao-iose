# Fluxogramas para ações na interface
Aqui são descritos os fluxogramas que descrevem o comportamento necessário para um Usuário realizar ações através da interface.

A interface considera quatro telas principais, são estas:

- **Login** - é a tela inicial onde o Usuário insere os dados para ter acesso à aplicação e por ela também a tela de esquecia senha pode ser acessada.
- **Painel de controle do super usuário** - esse painel só é acessível se o utilizador for do tipo __super usuário__, nele é possível ver todos os clientes e administradores cadastrados.
- **Painel das unidades** - neste painel, quando acessado por um __administrador__ é possível ver todas as unidades que pertencem a ele, já quando é acessada por um __gerente__, __operador__ e __operador com acionamento__ é possíver apenas uma única unidade.
- **Painel de controle do quadro (Dashboard)** - neste painel é possível ver informações técnicas e o gráfico de consumo do quadro em questão.

Do ponto de vista dos Usuários normais, isto é, __adminstradores__, __gerentes__ e __operadores__, todas as funções do sistema ficam acessíveis em duas telas (painel de unidades e dashboard). As figuras abaixo apresentam algumas dessas telas.

**Observação:**  <p style='width:70px;text-align: center;background-color:#F3C32A;border:2px solid black;color:#000;'>Bloco<p> 
Todos os blocos com este estilo nos diagramas, referem-se a etapas feitas apenas por um **super-usuário**.


<p style='text-align:center;'>Figura do painel de controle do quadro</p>

![TelaUnidades](./imagens/TelaPainelControleQuadro.jpg)

<p style='text-align:center;'>Figura do painel de controle do super usuário</p>

![TelaSuperUsuario](./imagens/TelaSuperUsuario.jpg)

<p style='text-align:center;'>Figura do painel das unidades</p>

![TelaUnidades](./imagens/TelaUnidades.jpg)


A seguir serão apresentados os fluxogramas para realizar as principais operações no IOSE.

## Ações relacionadas ao gerenciamento de Usuários
Aqui ficam os fluxos relacionados com o gerenciamento de usuários.
### Cadastrar e Deletar Super usuário
Todoo processo de cadastrar, editar e deletar um __super usuário__ é feito diretamente no banco de dados e apenas um __super usuário__ é capaz de realizar essas ações

<!-- #### Fluxo principal

1. Usuário faz login no sistema através da tela de login.
1. No painel de controle do super usuário já é possível cadastrar e deletar outro __super usuário__. A edição de dados do perfil de um __super usuário__ só pode ser realizada pelo mesmo, para isto é utilizado o menu de dropdown localizado no cabeçalho da página.

<p style='text-align:center;'>Figura do ciclo do processo de super usuário</p>
 
![CRUD_superusuário](./imagens/SUCadastrarEditarDeletarAdmin.gif) -->



### Cadastrar, Editar e Deletar Cliente

Fluxograma para o processo de cadastrar, editar e deletar um __Cliente__.
```mermaid
graph TD

subgraph Deletar 
	Inicio((Início)):::inicio --> login(usuário <br> faz login)
	login --> admin(usuário clica no icone de deletar <br> no card do Cliente) --> B(usuário  confirma deleção <br> Cliente na painel lateral)
	B -->Fim((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
end

subgraph Editar 
	Inicio2((Início)):::inicio --> login2(usuário <br> faz login)
	login2 --> admin2(usuário clica no icone de editar <br> no card do Cliente) --> C(usuário  edita <br> Cliente na painel lateral)
	C -->Fim2((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
end

subgraph Criar
	Inicio3((Início)):::inicio --> login3(usuário <br> faz login)
	login3 --> admin3(usuário clica no botão <br>adicionar<br> Cliente no painel<br> de controle do super usuário) --> C3(usuário cria<br> Cliente<br> na painel<br> lateral)
	C3 -->Fim3((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
end
```
#### Fluxo principal Criar
1. Usuário faz login no sistema através da tela de login.
1. No painel de controle do __super usuário__ já é possível cadastrar um __cliente__ através do acesso à barra lateral.

#### Fluxo principal  Editar
1. Usuário faz login no sistema através da tela de login.
1. No painel de controle do __super usuário__ já é possível editar um __cliente__ através do botão de deletar presente no card do cliente.

#### Fluxo principal Deletar 
1. Usuário faz login no sistema através da tela de login.
1. No painel de controle do __super usuário__ já é possível deletar um __cliente__ através do botão de deletar presente no card do cliente.

<!-- <p style='text-align:center;'>Criar, Editar e Deletar Administrador</p>

![AdminCadastrarEditarDeletarGerente](./imagens/AdminCadastrarEditarDeletarGerente.gif) -->

### Cadastrar, Editar e Deletar Administrador

Fluxograma para o processo de cadastrar, editar e deletar um __Administrador__.
```mermaid
graph TD

subgraph Deletar 
		Inicio_deletar((Início)):::inicio --> A_deletar(usuário <br> faz login)
	A_deletar --> B_deletar(usuário clica no ícone do<br> administrador<br> presente no card do<br> Cliente) 
	B_deletar --> C_deletar(usuário clica no<br> botão de deletar<br> presente no card<br> do administrador)
	C_deletar --> D_deletar(usuário deleta <br>Administrador) 
	D_deletar --> E_deletar((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
end

subgraph Editar 
	Inicio_Editar((Início)):::inicio --> A_editar(usuário <br> faz login)
	A_editar --> B_editar(usuário clica no ícone do<br> administrador<br> presente no card do<br> Cliente) 
	B_editar --> C_editar(usuário clica no<br> botão de editar<br> presente no card<br> do administrador)
	C_editar --> D_editar(usuário edita <br>Administrador) 
	D_editar --> E_editar((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
end

subgraph Criar
	Inicio3((Início)):::inicio --> A_Criar(usuário <br> faz login)
	A_Criar --> B_Criar(usuário clica no ícone do<br> administrador<br> presente no card do<br> Cliente) 
	B_Criar --> C_Criar(usuário clica no botão de adicionar<br> presente na barra lateral<br> que se abriu )
	C_Criar --> D_Criar(usuário cria <br>Administrador) 
	D_Criar --> E_Criar((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
end
```
#### Fluxo principal Criar
1. Usuário faz login no sistema através da tela de login.
1. Usuário clica no ícone do administrador que estar no card do Cliente.
1. Usuário clica no botão de adicionar presente na barra lateral que se abriu 
1. Usuário cria administrador através do formulário

#### Fluxo principal  Editar
1. Usuário faz login no sistema através da tela de login.
1. Usuário clica no ícone do administrador que estar no card do Cliente.
1. Usuário clica no botão de editar presente no card do administrador que está na lsita de cadastrador
1. Usuário edita administrador através do formulário

#### Fluxo principal Deletar 
1. Usuário faz login no sistema através da tela de login.
1. Usuário clica no ícone do administrador que estar no card do Cliente.
1. Usuário clica no botão de deletar presente no card do administrador que está na lsita de cadastrador
1. Após confirmação usuário é deletado

<!-- <p style='text-align:center;'>Criar, Editar e Deletar Administrador</p>

![AdminCadastrarEditarDeletarGerente](./imagens/AdminCadastrarEditarDeletarGerente.gif) -->


### Cadastrar, Editar e Deletar Gerente e Operador
Fluxograma para o processo de cadastrar, editar e deletar um __gerente__ ou um __operador__.

```mermaid
graph TD
	Inicio((Início)):::inicio --> login(usuário faz login)
	login --> admin(usuário clica<br> no card um Cliente<br>que deseja ver):::superusuario
    admin --> unidade(usuário entra na<br> tela de unidades do<br> cliente escolhido)
	unidade --> liga(usuário clica em uma<br> unidade e abre barra lateral)
	--> C(usuário cria, edita e deleta<br> gerente ou operador <br>na barra lateral)
	C--> Fim((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef superusuario fill:#F3C32A ,stroke:#333,stroke-width:2px,color:#000;
```
#### fluxo principal
1. Usuário faz login no sistema através da tela de login.
1. Usuário escolhe qual __cliente__ deseja ver.
1. No painel das unidades já é possível cadastrar, editar e deletar um __gerente__ ou __operador__ através do uso da barra lateral.


<p style='text-align:center;'>Figura do ciclo do processo de gerente e operador</p>

![GerenteCadastrarEditarDeletarOperador](./imagens/GerenteCadastrarEditarDeletarOperador.gif)

### Editar o próprio perfil

<br>

```mermaid
graph TD
	Inicio((Início)):::inicio --> login(Usuário faz login)
	login --> admin(Usuário vai na barra<br> de cabeçalho, clica <br>no perfil e edita)
	admin --> Fim((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
```

#### fluxo principal
1. Usuário faz login no sistema através da tela de login.
1. Usuário já pode editar o próprio perfil logo após fazer o login no sistema, basicamente esta função é disponível em todos os locais da aplicação.



<p style='text-align:center;'>Figura do ciclo do processo para editar perfil</p>

![QualquerUsuarioEditarPerfil](./imagens/QualquerUsuarioEditarPerfil.gif)


## Ações relacionadas aos Circuitos

### Acionamento do circuito

<br>

```mermaid
graph TD

subgraph Opção 2
	Inicio((Início)):::inicio --> login(Usuário faz login)
	login --> admin(Usuário clica em<br> um Cliente<br>que deseja ver):::superusuario
    admin --> unidade(Usuário entra na<br> tela de unidades do<br> Cliente escolhido)
	unidade --> barra(Usuário clica em<br> uma unidade e<br> abre barra lateral)
	--> C(Usuário liga ou<br> desliga circuito<br> no quadro e unidade escolhida)
	C--> Fim((Fim)):::fim
end
subgraph Opção 1
	Inicio2((Início)):::inicio --> login2(Usuário faz login)
	login2 --> admin2(Usuário clica em<br> um administrador<br>que deseja ver):::superusuario
    admin2 --> unidade2(Usuário entra na<br> tela de unidades do<br> administrador escolhido)
	unidade2 --> barra2(Usuário clica em<br> uma unidade e<br> abre barra lateral)
	barra2 --> quadro2(Usuário clica <br>em um quadro)
	quadro2 --> liga2(Usuário entra na<br> dashboard do quadro)
	--> C2(Usuário liga ou<br> desliga circuito<br> do quadro escolhido)
	C2 -->Fim2((Fim)):::fim
end


	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef superusuario fill:#F3C32A ,stroke:#333,stroke-width:2px,color:#000;
```

#### Fluxo Opção 1
1. Usuário faz login no sistema através da tela de login.
1. Usuário escolhe qual __cliente__ deseja ver.
1. Usuário escolhe uma unidade, clica em um quadro e na dashboard consegue acionar o circuito.

 

#### Fluxo Opção 2
1. Usuário faz login no sistema através da tela de login.
1. Usuário escolhe qual __cliente__ deseja ver.
1. Usuário escolhe uma unidade dentre as disponíveis no painel e após isso clica em acionar o circuito diretamente da barra lateral.


<p style='text-align:center;'>Figura do ciclo do processo de ligar/desligar circuito<p>

![LigarDesligarCircuitos](./imagens/LigarDesligarCircuitos.gif)

### Associar e Desassociar novos circuitos


```mermaid
graph TD

subgraph Desassociar
	Inicio_Desassociar((Início)):::inicio --> A_Desassociar(usuário <br> faz login)
	A_Desassociar-->B_Desassociar(Clicar em uma <br> Unidade)
	B_Desassociar--> C_Desassociar(Clicar em um <br> Quadro)
	C_Desassociar --> D_Desassociar(Clicar no botão <br> de Desassociar)
	D_Desassociar --> E_Desassociar(Desassociar Circuito)
	E_Desassociar --> Fim_Desassociar((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
end

subgraph Associar
	Inicio_Associar((Início)):::inicio --> A_Associar(usuário <br> faz login)
	A_Associar-->B_Associar(Usuário associa novos circuitos <br>através do aviso apresentado)
	B_Associar-->Fim_Associar((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
end
```
#### Fluxo Associar
1. Usuário faz login no sistema através da tela de login.
1. Usuário escolhe qual __cliente__ deseja ver.
1. Expande aviso de circuitos desassociados e através das instruções presentes nele associa circuito

#### Fluxo Desassocia
1. Usuário faz login no sistema através da tela de login.
1. Usuário escolhe qual __cliente__ deseja ver.
1. Usuário escolhe o quadro que possui o circuito que deseja desassociar.
1. Clica no botão de desassociação de circuito.
1. Seleciona circuito e o desassocia.


<!-- <p style='text-align:center;'>Figura do ciclo do processo de editar e excluir circuito<p>

![EditarExcluirCircuito](./imagens/EditarExcluirCircuito.gif) -->

 <!-- ### Atrelar um circuito em um quadro


#### Fluxo atrelar circuito ao quadro
1. Usuário faz login no sistema através da tela de login.
1. Usuário escolhe qual __administrador__ deseja ver.
1. Usuário escolhe uma unidade dentre as disponíveis no painel.
1. Na aba lateral usuário recebe uma notificação sobre circuitos ainda não atrelados a nenhum quadro.
1. Usuário clica no botão com ícone de raio em um dos quadros disponíveis.
1. Usuário escolhe circuitos para atrelar no quadro escolhido.


<p style='text-align:center;'>Figura do ciclo do processo de atrelar circuito em quadro<p>

![AtrelarCircuitosemQuadro.gif](./imagens/AtrelarCircuitosemQuadro.gif)
 --> 

### Editar e  Deletar um circuito

```mermaid
graph TD

subgraph Deletar 
	Inicio_Deletar((Início)):::inicio --> A_Deletar(usuário <br> faz login)
	A_Deletar-->B_Deletar(Clicar em uma <br> Unidade)
	B_Deletar--> C_Deletar(Entra na Dashborad co Quadro)
	C_Deletar --> D_Deletar(Clicar no botão de deletar <br> circuito<br> presente na<br> Tabela logo abaixo<br> do gráfico)
	D_Deletar --> E_Deletar(Confirma deleção e circuito <br>é deletado)
	E_Deletar --> Fim_Deletar((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
end

subgraph Editar 
	Inicio_Editar((Início)):::inicio --> A_Editar(usuário <br> faz login)
	A_Editar-->B_Editar(Clicar em uma <br> Unidade)
	B_Editar--> C_Editar(Expande card do Quadro)
	C_Editar --> D_Editar(Clicar circuito que deseja presente na lista)
	D_Editar --> E_Editar(Clicar no botão de editar circuito presente no card do circuito)
	E_Editar --> F_Editar(Editar circuito)
	F_Editar --> Fim_Editar((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
end
```
#### Fluxo editar
1. Usuário faz login no sistema através da tela de login.
1. Usuário escolhe qual __cliente__ deseja ver.
1. Usuário escolhe o quadro que possui o circuito.
1. Clica no circuito que deseja editar presente na lista.
1. Clica no botão de editar presente no card do circuito


#### Fluxo deletar
1. Usuário faz login no sistema através da tela de login.
1. Usuário escolhe qual __cliente__ deseja ver.
1. Usuário escolhe o quadro que possui o circuito e entra na na sua Dashboard
1. Na tabela logo abaixo do gráfico usuário clica no botão de deletar do circuito deseja
1. Usuario confirma deleção e deleta circuito
## Ações relacionadas aos Quadros

### Criar, Editar e Deletar Quadros ###

<br>

```mermaid
graph TD

subgraph Editar e Deletar
	Inicio2((Início)):::inicio --> login2(Usuário faz login)
	login2 --> admin2(Usuário clica<br> em um cliente<br>que deseja ver):::superusuario
    admin2 --> unidade2(Usuário entra na<br> tela de unidades do<br> cliente escolhido)
	unidade2 --> barra2(Usuário clica em uma<br> unidade e abre barra lateral)
	barra2 --> quadro2(Usuário clica <br>em um quadro)
	quadro2 --> liga2(Usuário entra na dashboard de quadro)
	--> C(Usuário edita e deleta o quadro selecionado)
	C -->Fim2((Fim)):::fim
end

subgraph Criar
	Inicio3((Início)):::inicio --> login3(Usuário faz login)
	login3 --> admin3(Usuário clica<br> em um cliente<br>que deseja ver):::superusuario
    admin3 --> unidade3(Usuário entra na<br> tela de unidades do<br> cliente escolhido)
	unidade3 --> barra3(Usuário clica em uma<br> unidade e abre barra lateral)
	barra3 --> quadro(Usuário  cria um quadro)
	quadro -->Fim3((Fim)):::fim
end




	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef superusuario fill:#F3C32A ,stroke:#333,stroke-width:2px,color:#000;


```

#### Fluxo Principal Criar

1. Usuário faz o login no sistema;
1. Usuário escolhe qual **cliente** deseja ver;
1. Usuário escolhe a unidade;
1. Usuário já é possível criar um Quadro na barra lateral;

#### Fluxo Principal Editar e Deletar

1. Usuário faz o login no sistema;
1. Usuário escolhe qual **cliente** deseja ver;
1. Usuário escolhe a unidade;
1. Usuário entra no quadro desejado e já é possível editar e deletar Quadro;



<p style='text-align:center;'>Figura do ciclo do processo de criar quadro</p>

![CriarQuadro](./imagens/CriarQuadro.gif)

<p style='text-align:center;'>Figura do ciclo do processo de editar e deletar quadros</p>

![EditarDeletarQuadros](./imagens/EditarDeletarQuadros.gif)

## Ações relacionadas com Medidas

```mermaid
graph TD
	Inicio2((Início)):::inicio --> login2(Usuário faz login)
	login2 --> admin2(Usuário escolhe<br> qual cliente<br> deseja ver):::superusuario
    admin2 --> unidade2(Usuário entra na<br> tela de unidades)
	unidade2 --> quadro(Usuário clica <br>em uma unidade<br> e seleciona quadro)
	quadro --> liga2(Usuário na dashboard de<br> usuário clica no botão de histórico)
	liga2 --> hist(Usuário seta período que deseja)
	hist --> A(Usuário visualiza e baixa histórico de  medidas)
	A-->Fim2((Fim)):::fim

	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef superusuario fill:#F3C32A ,stroke:#333,stroke-width:2px,color:#000;
```
#### fluxo principal 
1. Usuário faz login no sistema através da tela de login.
1. Usuário escolhe qual __cliente__ deseja ver.
1. Usuário escolhe uma unidade e acessa dashboard do quadro desejado.
1. Logo abaixo do gráfico clica no botão de histórico.
1. Após setar período usuário pode ver o histórico de medidas dos circuitos selecionados.


<!-- <p style='text-align:center;'>Figura do ciclo do processo de vizualizar histórico de medidas</p>

![BaixarVisualizarHistoricoMedidas](./imagens/BaixarVisualizarHistoricoMedidas.gif) -->

## Ações relacionadas com Agendamentos

### Criar, Editar e Deletar Agendamento
```mermaid
graph TD
	Inicio((Início)):::inicio --> login(Usuário faz login)
	login --> admin(Usuário Usuário escolhe<br> qual cliente deseja ver):::superusuario
    admin --> unidade(Usuário entra na tela de unidades)
	unidade --> agenda(Usuário clica em botão agenda <br> na unidade e realiza <br> todas as operações de agendamento)
	agenda--> Fim((Fim)):::fim

	
	classDef titulo fill:#ebc532 ,stroke:#333,stroke-width:4px;
	classDef inicio fill:#4ABF8A ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef fim fill:#EF6222 ,stroke:#333,stroke-width:2px,color:#ffff;
	classDef superusuario fill:#F3C32A ,stroke:#333,stroke-width:2px,color:#000;
```

#### fluxo principal 
1. Usuário faz login no sistema através da tela de login.
1. Usuário escolhe qual __cliente__ deseja ver.
1. Usuário escolhe um unidade dentre as disponíveis no painel e clica no ícone de calendário desta.
1. Na barra lateral usuário cria, edita e deleta um agendamento.

<p style='text-align:center;'>Figura do ciclo do processo de Agendamento</p>

![CriarEditarDeletarAgendamento](./imagens/CriarEditarDeletarAgendamento.gif)





