# Fuota Iose

Este documento tem como finalidade definir os requisitos para a ferramenta utilizada para realizar a operação de atualização de firmware nos dispositivos

# 1 - Criação de usuário 

Todos os usuários que manipularão a aplicação de manutenção terão seus usuários gerados manualmente com revogação de credenciais mensalmente.

# 2 - Descrição dos casos de uso

### 2.1 Autenticação no sistema

Descrição: UC-1 - Autenticação no sistema de manutenção  
Ator: Mantenedor

Fluxo Principal - Autenticação

| Passos | Descrição | 
|--------|-----------|
|   1    |  O Ator fornece os dados de login e senha   |
|   2    |  O sistema autentica as credenciais do Ator
|   3    |  O Ator é redirecionado a tela de histórico dos Fuota já executados   |

Fluxo Alternativo - Erro na autenticação

| Passos | Descrição | 
|----------|-----------|
|   2.1    |  O sistema invalida os dados de autenticação do Ator | 
|   2.2    |  O sistema informa ao Ator o erro no processo de autenticação   |

### 2.2 Cria um processo Fuota

Descrição: UC-2 - Criação do processo de Fuota
Pré-requisitos: UC-1  
Ator: Mantenedor

<b>Fluxo Principal - Criação do processo de Fuota</b>

| Passos | Descrição | 
|--------|-----------|
|   1    |  O Ator solicita a criação de um novo processo de Fuota   |
|   2    |  O sistema gera uma identificação automática do Fuota e solicita como campo obrigatório uma descrição do processo de Fuota |
|   3    |  O sistema fornece uma lista de firmware disponíveis para a seleção |
|   4    |  O Ator seleciona obrigatoriamente o firmware desejado  |
|   5    |  Após a seleção o sistema fornece a versão e as principais features do firmware selecionado   |
|   6    |  O sistema solicita um arquivo no formato "CSV" contendo os MACS dos dispositivos que passarão pelo processo de Fuota   |
|   7    |  O sistema valida cada MAC inserido no arquivo CSV e permite apenas macs validados atualizarem   |
|   8    |  O sistema cria um processo de Fuota e encaminha o Ator para a página que lista os processos de Fuota não executados   |

<b>Fluxo Alternativo - Lista com todos os MACS inválidos</b>

| Passos | Descrição | 
|----------|-----------|
|   7.1    |  O sistema invalida todos os MACS da lista fornecida   |
|   7.2    |  O sistema bloqueia o prosseguimento para a próxima etapa | 
|   7.3    |  O sistema informa ao usuário que todos os MACS fornecidos estão inválidos |

### 2.3 Execução de um processo Fuota

Descrição: UC-3 - Execução de um processo Fuota
Pré-requisitos: UC-1, UC-2
Ator: Mantenedor

<b>Fluxo Principal - Execução de um processo Fuota  </b>

| Passos | Descrição | 
|--------|-----------|
|   1    |  O Ator acessa a sessão de Fuotas não executados   |
|   2    |  O sistema retorna todos os processo de Fuotas criados e não executados   |
|   3    |  O Ator executa o processo Fuota selecionado   |
|   4    |  O sistema bloqueia os outros processos não executados |
|   5    |  O sistema retorna no resultado do processo em um intervalo máximo de 3 minutos |
|   6    |  O sistema libera os processos Fuota não executados caso existam  |

### 2.4 Visualização de históricos dos artefatos gerados.

1- O Ator poderá visualizar a lista de Fuotas executados e todo o <i>snapshot</i> das ações ocorrida naquele update.</br>
2- O Ator poderá visualizar a lista de Fuotas ainda não executados.</br>
3- O Ator poderá visualizar a linha do tempo de atualização de firmware de cada dispositivo incluídos no artefato Fuota criado.</br>

### 2.5 Protótipos de telas

A seguir serão apresentadas as telas prototipadas na ferramenta figma referente ao FUOTA:</br></br>



<p style='text-align:center;'>Tela de Login</p>

![Login](./imagens/Login.png)

</br>

<p style='text-align:center;'>Tela de Histórico</p>

![Histórico](./imagens/Historico.png)

</br>

<p style='text-align:center;'>Tela de informações sobre o artefato FUOTA</p>

![InformacoesFUOTA](./imagens/InformacoesFUOTA.png)

</br>

<p style='text-align:center;'>Tela de informações sobre o circuito/módulo contigo no artefato FUOTA</p>

![InformacoesCircuito](./imagens/InformacoesCircuito.png)

</br>

<p style='text-align:center;'>Tela de Execução onde encontram-se todos artefatos FUOTA a serem executados</p>

![Executar](./imagens/Executar.png)

</br>

<p style='text-align:center;'>Tela apresentada ao iniciar a execução do FUOTA</p>

![GravacaoModulos](./imagens/GravacaoModulos.png)

</br>

<p style='text-align:center;'>Tela de Execução enquanto está executando um FUOTA</p>

![ExecutarComFUOTARodando](./imagens/ExecutarComFUOTARodando.png)

</br>

<p style='text-align:center;'>Modal de criação do artefato FUOTA, informações FUOTA</p>

![novoFOUTAEtapa1](./imagens/novoFOUTAEtapa1.png)

</br>

<p style='text-align:center;'>Modal de criação do artefato FUOTA, Selecionar firmware </p>

![novoFOUTAEtapa2](./imagens/novoFOUTAEtapa2.png)
      
</br>

<p style='text-align:center;'>Modal de criação do artefato FUOTA, Adiconar módulos</p>

![novoFOUTAEtapa3](./imagens/novoFOUTAEtapa3.png)

</br>

<p style='text-align:center;'>Modal de criação do artefato FUOTA, Validação módulos</p>

![novoFOUTAEtapa4](./imagens/novoFOUTAEtapa4.png)




Para mais detalhes das telas da ferramenta de Fuota: [Protótipo das telas](https://www.figma.com/file/auo8ekoXzRqPp8fqw4G9mA/FUOTA-IoSE?node-id=2%3A1084)


