﻿# Acionamento Remoto
# Introdução
Este documento especifica a funcionalidade de _Acionamento Remoto_. Nesta especificação serão definidos:

* tópicos de requisição e resposta para a acionamento do relé;
* formatação de dados trocados entre software e firmware: requisição e resposta;
* comportamento esperado do dispositivo para realização da operação.

Esta especificação deve, ao seu fim, ser de tal forma que o dispositivo esteja apto a receber a requisição, realizar o acionamento e enviar o estado do relé e do comando para os tópicos específicos.

# Comunicação MQTT
Nesta secção são determinados os tópicos de onde são feitos as requisições e para onde serão enviadas as medidas solicitadas e seus respectivos pacotes JSON.

## Requisição
### Tipo
| Operação | Tipo |
|----------|------|
| Acionamento | 8 |

### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |
| Produção | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_req | string | Identificador único da requisição (máx. 63 caracteres) |
| type | integer | Indica o tipo de requisição |
| payload | struct | Contém o estado de comutação |
| state_actuation | boolean | Ação do relé: Fechar = True, Abrir = False |

#### Exemplo
```json
{
	"uuid_req": "123456789",
	"type": 8, 
	"payload":
	{
		"state_actuation": false
	}
}
```

## Resposta - Retorno de execução da requisição
### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/return |
| Produção | prod/iose/general/return  |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único da módulo |
| uuid_req | string | Identificador único da requisição recebida (máx. 63 caracteres) |
| req_type | integer | Tipo de requisição recebida |
| success | boolean | Estado de execução |
| relay_state | boolean | Estado atual do relé |
| timestamp | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo
```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_req": "123456789",
    "req_type": 2,
    "success": true,
    "relay_state": true,
    "timestamp": "2020-09-23T18:54:12.000Z"
}
```

# Comportamento Esperado
## Falha
Abaixo segue os casos de falha e o comportamento.
| Falha | Comportamento |
|-------|---------------|
| Pacote JSON inválido | Enviará pacote de retorno com req_type = 0 e success = false |
| Falha na execução | Enviará pacote de retorno com req_type = 8 e success = false |

## Sucesso
O módulo irá comutar o relé e retornar um pacote com o estado de sucesso na execução da requisição e outro pacote contendo a [origem do acionamento e o estado do relé]() para os seus respectivos tópicos.