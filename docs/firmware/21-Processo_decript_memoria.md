﻿# Processo de Decriptação de Memória Lida 
# Introdução
Este documento especifica a funcionalidade de _Processo de Decriptação de Memória Lida_. Nesta especificação serão definidos:

* leitura da região de memória;
* decriptação da memória encriptada lida;
* decriptação de partição NVS.

Esta especificação deve, ao seu fim, ser de tal forma que o desenvolvedor esteja apto a recuperar informações do módulo em auxílio ao diagnóstico de dispostivos encriptados.

# Memória encriptada
O IDF da espressif para ESP32 possui a capacidade de encriptar a memória flash em algumas partições, exceto a de subtipo NVS, neste caso a nvs é encriptada por em um processo separado. Para a devida leitura dessas regiões é necessário usar os processos descritos neste documento.

## Leitura da região de memória
No ESP32 as regiões de memória são divididas em partições, da forma mais próxima ao usado no linux. Logo abaixo estão descritos as ferramentas necessárias e o processo de leitura dessas partições.

### esptool.py

Sintaxe: 
>esptool.py -p {PORT} read_flash {ADDRESS} {SIZE} {OUTPUT_FILE}

| Parâmetro | Descrição |
|----------|--------|
| PORT | Nome da porta serial |
| ADDRESS | Endereço do início da partição |
| SIZE | Tamanho da partição |
| OUTPUT_FILE | Caminho para o arquivo de saída|

### Procedimento
Preencha os campos do comando descrito acima com os parâmetros desejados e execute.

Exemplo:
>esptool.py read_flash 0x1b0000 0x100000 "dump_ota0.bin"

Saida:
```
esptool.py v3.0
Found 1 serial ports
Serial port /dev/ttyUSB0
Connecting....
Detecting chip type... ESP32
Chip is ESP32-D0WD (revision 1)
Features: WiFi, BT, Dual Core, 240MHz, VRef calibration in efuse, Coding Scheme None
Crystal is 40MHz
MAC: b8:f0:09:9a:cb:90
Uploading stub...
Running stub...
Stub running...
1048576 (100 %)
1048576 (100 %)
Read 1048576 bytes at 0x1b0000 in 95.7 seconds (87.7 kbit/s)...
Hard resetting via RTS pin...
```

## Decriptação da informação lida
É possível encriptar quase todos os subtipos de partições, exceto a NVS que possui um processo próprio. Quando a memória é gravada encriptada, é possível decriptá-la após ler a partição desejada. Logo abaixo estão descritos as ferramentas necessárias e o processo de decriptação dessas partições.

### Demais subtipos de partição
### espsecure.py

Sintaxe:
>espsecure.py decrypt_flash_data {ENCRYPTED_DATA} -k {KEY_FILE} -o {OUTPUT_FILE} -a {ADDRESS}

| Parâmetro | Descrição |
|----------|--------|
| ENCRYPTED_DATA | Caminho para o arquivo encriptado |
| KEY_FILE | Caminho para o arquivo da chave de encriptação |
| OUTPUT_FILE | Caminho para o arquivo de saída |
| ADDRESS | Endereço do início da partição |

### Procedimento
Preencha os campos do comando descrito acima com os parâmetros desejados e execute.

Exemplo:
>espsecure.py decrypt_flash_data dump_ota0.bin -k main/helpers/files/keys/flash_encrypt_key.bin -o ./dump_ota0_decr.bin -a 0x1b0000

Saida:
```
espsecure.py v3.0
Using 256-bit key
```

### Subtipo NVS
Para decriptar esse subtipo será necessário ter a chave NVS de encriptação, que pode ser lida e decriptada usnado o processos acima.

### nvs_partition_gen.py

Sintaxe:
>python {IDF_PATH}/components/nvs_flash/nvs_partition_generator/nvs_partition_gen.py decrypt {ENCRYPTED_DATA} {NVS_KEY_FILE} {OUTPUT_FILE}

| Parâmetro | Descrição |
|----------|--------|
| IDF_PATH | Caminho para o IDF |
| ENCRYPTED_DATA | Caminho para o arquivo encriptado |
| NVS_KEY_FILE | Caminho para o arquivo da chave NVS de encriptação |
| OUTPUT_FILE | Caminho para o arquivo de saída |

### Procedimento
Preencha os campos do comando descrito acima com os parâmetros desejados e execute.

Exemplo:
>python $IDF_PATH/components/nvs_flash/nvs_partition_generator/nvs_partition_gen.py decrypt dump_nvs.bin dump_nvs_keys_decr.bin dump_nvs_decr.bin

Saida:
```
Created NVS decrypted binary: ===> /home/renanpascoal/Documentos/NEPEN-REPO/iose-release/iose-fw/fw/src/iose_wifi/src/dump_nvs_decr.bin
```

## Fluxo de leitura das partições encriptadas
### Demais subtipos de partição
![Fluxo para demais subtipos](./imagens/decrypt_encrypted_mem-diag-1-v0r0.png)

### Subtipo NVS
![Fluxo para subtipo NVS](./imagens/decrypt_encrypted_nvs-diag-1-v0r0.png)