﻿# Calibração remota do Módulo
# Introdução

Este documento especifica a funcionalidade de _Calibração do Módulo_. Nesta especificação são definidos:

* Especificação do modo de calibração;
* formatação de dados trocados entre software e firmware: mensagem com informações do circuito;
* fluxo de atividade do firmware para realização da calibração do dispositivo;
* comportamento esperado do dispositivo para realização a calibração.

Esta especificação deve, ao seu fim, ser de tal forma que o dispositivo esteja apto a calibrar suas medições elétricas remotamente por requisição do software.

As possibilidades de calibração esperadas para esta funcionalidade são:

1) Restaurar registradores de calibração e offset para valores de fábrica;
2) Executar calibração para sem carga (ajustes de offset);
3) Executar calibração para 5A@220Vac e fator de potência 1;
4) Executar calibração para 5A@220Vac e fator de potência 0.5;
<!--5) Alterar parâmetros de calibração;-->

# Comunicação remota via MQTT

Nesta secção são determinados os dados trocados entre o módulo (firmware) e AWS IOT core (software) a fim de:

* Executar todas as etapas de calibração;
* Enviar resposta com o estado das requisições.

## Executar etapa de calibração

A calibração possui 4 etapas sequenciais, restauração de padrão do fabricante, calibração sem carga, fator de potÊncia 1 e 0,5. Abaixo segue a indexação dessas etapas para a requisição.

| Etapas | Índices |
|:-:|:-:|
| Restauração de fábrica | 0 |
| Sem carga | 1 |
| FP 1 | 2 |
| FP 0.5 | 3 |

### Condições de uso
* A etapa só terá sucesso quando ela e as etapas anteriores obtiverem êxito em seus processos;
* Uma vez com sucesso essa etapa retornará falso enquanto as etapas anteriores não forem executadas novamente e obtiverem êxito;
* Caso a etapa falhe, é possível tentar novamente, contudo, calibração do medidor só estará completa quando todas as etapas forem concluídas com sucesso.

## Requisição
### Tipo
| Operação | Tipo |
|----------|------|
| Executar etapa de calibração | 17 |

### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |
| Produção | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_req | string | Identificador único da requisição (máx. 63 caracteres) |
| type | integer | Indica o tipo de requisição |
| step | integer | Índice da etapa de calibração |

### Restauração de calibração do fabricante

Para a primeira calibração é mandatório restaurar os valores de ganhos e offsets, para o padrão estabelecido pelo fabricante.<br>
Este processo também libera as "flags" das calibrações realizadas.

#### Exemplo
##### Requisição para executar etapa 0

```json
{
  "uuid_req": "123456789",
  "type": 17,
  "step": 0
}
```

### Calibração sem carga

Esta etapa tem como objetivo compensar os valores residuais enquanto o módulo estiver sem carga.

#### Exemplo
##### Requisição para executar etapa 1

```json
{
  "uuid_req": "123456789",
  "type": 17,
  "step": 1
}
```

### Calibração em ator de potência 1

Esta etapa tem como objetivo calibrar as medidas de tensão, frequência, corrente e potência ativa, enquanto o fator de potência da carga é 1.

#### Exemplo
##### Requisição para executar etapa 2

```json
{
  "uuid_req": "123456789",
  "type": 17,
  "step": 2
}
```
### Calibração em ator de potência 0.5

Esta etapa tem como objetivo calibrar as medidas de potência reativa e compensar ângulo de defasagem, enquanto o fator de potência da carga é 0.5.

#### Exemplo
##### Requisição para executar etapa 3

```json
{
  "uuid_req": "123456789",
  "type": 17,
  "step": 3
}
```

## Resposta
### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/factory |
| Produção | prod/iose/general/factory |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único da módulo |
| uuid_req | string | Identificador único da requisição recebida (máx. 63 caracteres) |
| req_type | integer | Tipo de requisição recebida |
| success | boolean | Estado de execução |
| volt | float | Tensão RMS (V) |
| freq | float | Frequência da rede (Hz) |
| pwr_f | float | Fator de potência (entre -1.00 e 1.00) |
| curr | float | Corrente RMS (A) |
| ac_pwr | float | Potência Ativa (W) |
| rc_pwr | float | Potência Reativa (VAr) |
| app_pwr | float | Potência Aparente (VA) |
| timestamp | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo
##### Sucesso na calibração de fator de potencia 1 (também aplicável para todas as outras etapas)

```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_req": "123456789",
    "step": 1,
    "success": true,
    "sample": [
      {
        "volt": 220,
        "freq": 60,
        "pwr_f": 1,
        "curr": 5,
        "ac_pwr": 1100,
        "rc_pwr": 0,
        "app_pwr": 1100,
      }
    ],
    "timestamp": "2020-09-23T18:54:12.000Z",
}
```

##### Falha na calibração de fator de potencia 1 (também aplicável para todas as outras etapas)

```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_req": "123456789",
    "step": 1,
    "success": false,
    "timestamp": "2020-09-23T18:54:12.000Z",
}
```

## Casos de Uso

|  Condição / Fluxo | Descrição / Dado trocado |
|-|-|
| Ator Principal | Módulo e MQTT (Cloud) |
| Ator Secundário | Rotina de calibração e medidor |
| Pré-condição | Módulo com medidor descalibrado |
| Pós-condição | Módulo com medidor calibrado |
| Fluxo Principal | <br>1) Módulo recebe uma requisição para **executar etapa de calibração**: <br><br>&emsp;Executa rotina da etapa de calibração, medidor responde sucesso, medidas passam na validação; <br><br>2) Módulo envia uma resposta de sucesso na execução:<br><br>&emsp;Etapa 0: json-msg(AABBCCDDEEFF, 123456789, 18, 220, 60, 1, 5, 4, 3, 0, true, 2021-01-21T23:01:22.000Z).<br><br>&emsp;Se etapa 0 obter sucesso,<br><br>&emsp;Etapa 1: json-msg(AABBCCDDEEFF, 123456789, 18, 220, 60, 1, 0, 0, 0, 0, true, 2021-01-21T23:01:22.000Z).<br><br>&emsp;Se etapa 0 e 1 obterem sucesso,<br><br>&emsp;Etapa 2: json-msg(AABBCCDDEEFF, 123456789, 18, 220, 60, 1, 5, 1100, 0, 1100, true, 2021-01-21T23:01:22.000Z).<br><br>&emsp;Se etapa 0, 1 e 2 obterem sucesso,<br><br>&emsp;Etapa 3: json-msg(AABBCCDDEEFF, 123456789, 18, 220, 60, 0.5, 5, 550, 952.63, 1100, true, 2021-01-21T23:01:22.000Z).<br><br>
| Fluxo Alternativo | <br>1) Módulo recebe uma requisição para **executar etapa de calibração**: <br><br>&emsp;Executa rotina da etapa de calibração, medidor responde falha, ou medidas reprovam na validação; <br><br>2) Módulo envia uma resposta de falha na execução:<br><br>&emsp;Etapa 0: json-msg(AABBCCDDEEFF, 123456789, 18, 220, 60, 1, 5, 4, 3, 2, false, 2021-01-21T23:01:22.000Z).<br><br>&emsp;Se etapa 0 também obter falha,<br><br>&emsp;Etapa 1: json-msg(AABBCCDDEEFF, 123456789, 18, 220, 60, 1, 2, 2, 0, 2, false, 2021-01-21T23:01:22.000Z).<br><br>&emsp;Se etapa 0 e/ou 1 também obterem falha,<br><br>&emsp;Etapa 2: json-msg(AABBCCDDEEFF, 123456789, 18, 210, 60, 1, 4.7, 987, 0, 987, false, 2021-01-21T23:01:22.000Z).<br><br>&emsp;Se etapa 0, e/ou 1, e/ou 2 também obterem falha,<br><br>&emsp;Etapa 3: json-msg(AABBCCDDEEFF, 123456789, 18, 210, 60, 0.5, 4.7, 493.5, 854.77, 987, false, 2021-01-21T23:01:22.000Z).<br><br> |
<!--
## Alterar parâmetros de calibração

A calibração exige parâmetros de referência, por padrão adotado em fábrica os valores são os seguintes

| Parâmetros | Valores |
|:-:|:-:|
| Tensão | 220 Vac |
| Corrente | 5 A |

Esses parâmetros podem ser alterados através da requisição remota que será explicada em seguida.

## Requisição
### Tipo
| Operação | Tipo |
|----------|------|
| Alterar parâmetros de calibração | 19 |

### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |
| Produção | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_req | string | Identificador único da requisição (máx. 63 caracteres) |
| type | integer | Indica o tipo de requisição |
| volt_ref | integer | Tensão de referência (V) |
| curr_ref | integer | Corrente de referência (mA) |

#### Exemplo
##### Alteração de parâmetros para 220 V e 5000 mA
```json
{
  "uuid_req": "123456789",
  "type": 19,
  "volt_ref": 220,
  "curr_ref": 5000
}
```

## Resposta
### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/factory |
| Produção | prod/iose/general/factory |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único da módulo |
| uuid_req | string | Identificador único da requisição recebida (máx. 63 caracteres) |
| req_type | integer | Tipo de requisição recebida |
| success | boolean | Estado de execução |
| timestamp | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo
##### Sucesso
```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_req": "123456789",
    "req_type": 19,
    "success": true,
    "timestamp": "2020-09-23T18:54:12.000Z"
}
```
##### Falha
```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_req": "123456789",
    "req_type": 19,
    "success": false,
    "timestamp": "2020-09-23T18:54:12.000Z"
}
```

## Casos de Uso

|  Condição / Fluxo | Descrição / Dado trocado |
|-|-|
| Ator Principal | Módulo e MQTT (Cloud) |
| Ator Secundário | Memória não volátil (NVM) |
| Pré-condição | Módulo com parâmetros de calibração padrão |
| Pós-condição | Módulo com parâmetros de calibração alterados |
| Fluxo Principal | <br>1) Módulo recebe uma requisição para **alterar parâmetros de calibração**: <br><br>&emsp;Altera os valores dos parâmetros armazenados na NVM; <br><br>2) Módulo envia uma resposta de sucesso na execução:<br><br>&emsp;json-msg(AABBCCDDEEFF, 123456789, 19, true, 2021-01-21T23:01:22.000Z).<br><br>
| Fluxo Alternativo | <br>1) Módulo recebe uma requisição para **alterar parâmetros de calibração**: <br><br>&emsp;Falha ao alterar os valores dos parâmetros armazenados na NVM; <br><br>2) Módulo envia uma resposta de falha na execução:<br><br>&emsp;json-msg(AABBCCDDEEFF, 123456789, 19, false, 2021-01-21T23:01:22.000Z).<br><br> |

## Restaurar valores de calibração padrão

Os valores de ganho e offsets podem ser restaurados para o padrão de fábrica do medidor, isso é útil para caso a calibração falha de forma persistente.

## Requisição
### Tipo
| Operação | Tipo |
|----------|------|
| Restaurar calibração de fábrica | 20 |

### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |
| Produção | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_req | string | Identificador único da requisição (máx. 63 caracteres) |
| type | integer | Indica o tipo de requisição |

#### Exemplo
##### Executa restauração para calibração de fábrica
```json
{
  "uuid_req": "123456789",
  "type": 20
}
```

## Resposta
### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/factory |
| Produção | prod/iose/general/factory |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único da módulo |
| uuid_req | string | Identificador único da requisição recebida (máx. 63 caracteres) |
| req_type | integer | Tipo de requisição recebida |
| success | boolean | Estado de execução |
| timestamp | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo
##### Sucesso
```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_req": "123456789",
    "req_type": 20,
    "success": true,
    "timestamp": "2020-09-23T18:54:12.000Z"
}
```
##### Falha
```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_req": "123456789",
    "req_type": 20,
    "success": false,
    "timestamp": "2020-09-23T18:54:12.000Z"
}
```

## Casos de Uso

|  Condição / Fluxo | Descrição / Dado trocado |
|-|-|
| Ator Principal | Módulo e MQTT (Cloud) |
| Ator Secundário | Medidor |
| Pré-condição | Módulo com valores de calibração alterados |
| Pós-condição | Módulo com valores de calibração de fábrica |
| Fluxo Principal | <br>1) Módulo recebe uma requisição para **restaurar valores de calibração padrão**: <br><br>&emsp;Firmware envia pacote requisitando a restauração de calibração ao medidor, este, por sua vez, retorna sucesso na execução; <br><br>2) Módulo envia uma resposta de sucesso na execução:<br><br>&emsp;json-msg(AABBCCDDEEFF, 123456789, 20, true, 2021-01-21T23:01:22.000Z).<br><br>
| Fluxo Alternativo | <br>1) Módulo recebe uma requisição para **restaurar valores de calibração padrão**: <br><br>&emsp;Firmware envia pacote requisitando a restauração de calibração ao medidor, este, por sua vez, retorna falha na execução; <br><br>2) Módulo envia uma resposta de falha na execução:<br><br>&emsp;json-msg(AABBCCDDEEFF, 123456789, 20, false, 2021-01-21T23:01:22.000Z).<br><br> |
-->