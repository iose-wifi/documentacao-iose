﻿# Processo do modo Fábrica.
Nesse documento será feita a análise do procedimento do modo fábrica,assim como seu diagrama e descritivo,explicando cada etapa e como os teste serão realizados. Para facilitar o processo de testagem dos módulos,será usada uma plataforma de testes,para testar múltiplos módulos com maior agilidade.

![imagem 2](./imagens/imagem2.png)

### Fluxo Principal
1. É feita a verificação se as portas USBs do HUB estão calibradas(**E1**).
1. É feito o preenchimento das credenciais do teste,compostas por SSID e senha do WiFi,número de série do dono dos Módulos a serem testados e porta de teste WiFi.
1. É feita a verificação se as USBs foram configuradas na ordem correta(**E2**).
1. É feita a leitura dos QR Codes presentes em cada um dos módulos,com o auxílio de um leitor de QR Codes/Código de barras.
1. É feita a gravação do firmware de fábrica no módulo, que terá como propósito realizar todos os testes principais de hardware.
1. É feito o teste de validação de comunicação serial com o módulo,que permite a identificação do mesmo com seu endereço MAC(**E3**).
1. É feito o teste de validação do relé,que consiste em ativar o relé e confirmar a ativação com a detecção de fase da rede(**E4**).
1. Com a SSID e senha que forem fornecidos ao firmware por meio do software de Fábrica,o módulo se conecta ao WiFi.
1. Módulo envia comando de validação da comunicação WiFi,para verificar se a comunicação está ocorrendo normalmente(**E5**).
1. Módulo envia comando de validação para o sensor MCP,para verificar se a comunicação está ocorrendo normalmente(**E6**).
1. É feito o teste de validação dos LEDs,que consiste em verificar se todos os LEDs do módulo estão funcionando adequadamente(**E7**).
1. É feito o teste de validação dos botões,que consiste em apertar cada um dos botões e apagar um LED correspondente(**E8**).
1. É feita a calibração final do Módulo,que consistem em realizar a calibração sem carga,calibração de fator de potência 1 e 0,5 e reset das energias acumuladas (**E9**).
1. É feita a geração dos certificado e chave AWS(**E10**).
1. É feita a criação de uma "Coisa" dentro da plataforma da AWS com os certificados. O nome da Coisa será o endereço MAC do Módulo fornecido previamente durante o processo de teste(**E11**).
1. É feita a inserção do certificado e chave no projeto do firmware final.
1. É feita a configuração do WiFi padrão de testes e Mqtt ID no firmware final.
1. É feita a gravação do firmware final(**E12**).
1. É feito o registro do Módulo no Banco de Dados por meio de uma mensagem MQTT(**E13**).
1. É feita a geração do Log do processo do modo fábrica.

### Fluxo de exceção

#### E1. As portas USBs do HUB não foram calibradas.
- E1.1 É realizado o processo de calibração das portas USBs,que grava quais portas estão sendo usadas pelos módulos em ordem aleatória em um arquivo.
- E1.2 Procede para a próxima etapa.

#### E2. As portas USBs não estão na ordem correta.
- E2.1 É realizado o processo de ordenação das portas USBs calibradas.
- E2.2 Procede para a próxima etapa.

#### E3. O teste de validação de comunicação serial não é bem sucedido
- E3.1 É gerado um log de erro referente à comunicação serial.
- E3.2 O teste sofre falha.

#### E4. O teste de validação do relé não é bem sucedido.
- E4.1 É gerado um log de erro referente ao relé.
- E4.2 O teste sofre falha.

#### E5. O teste de validação da comunicação WiFi não é bem sucedido
- E5.1 É gerado um log de erro referente à comunicação WiFi.
- E5.2 O teste sofre falha.

#### E6. O teste de validação do sensor MCP não é bem sucedido
- E6.1 É gerado um log de erro referente à comunicação com o MCP.
- E6.2 O teste sofre falha.

#### E7. O teste de validação de LED não é bem sucedido
- E7.1 É gerado um log de erro referente ao LED.
- E7.2 O teste sofre falha.

#### E8. O teste de validação dos botões não é bem sucedido
- E8.1 É gerado um log de erro referente aos botões.
- E8.2 O teste sofre falha.

#### E9. A calibração final do Módulo não é bem sucedida.
- E9.1 É gerado um log de erro referente à calibração do módulo.
- E9.2 O teste sofre falha.

#### E10. A geração de chaves e certificados AWS não é bem sucedida.
- E10.1 É gerado um log de erro referente à geração de chaves e certificado AWS.
- E10.2 O teste sofre falha.

#### E11. A criação de "Coisa" AWS não é bem sucedida.
- E11.1 É gerado um log de erro referente à criação de Coisa AWS.
- E11.2 O teste sofre falha.

#### E12. A gravação do firmware final não é bem sucedida.
- E12.1 É gerado um log de erro referente à a gravação do firmware final.
- E12.2 O teste sofre falha.

#### E13. O cadastro do módulo no Banco de Dados via MQTT não é bem sucedido.
- E13.1 É gerado um log de erro referente à comunicação Mqtt.
- E13.2 O teste é abortado.



