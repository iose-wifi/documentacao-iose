# Protocolo e formato do payload JSON
Nesse documento será definido e descrito o formato das mensagens trocadas entre o firmware e o backend no formato JSON.
## Mensagens Servidor -> Dispositivo
### Requisições
O módulo é capaz de executar ações e retornar informações à partir de requisições vindas do tópico específico do broker MQTT.
#### Pacotes JSON das requisições
| Operação | Tipo | Pacotes JSON |
|--------|--------|--------------|
| Recuperação de medidas | 2 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/10-Recuperacao_de_medidas.html#requisicao) |
| Recuperação de eventos | 3 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/11-Eventos_Alarmes_Log.html#_4-3-formatacao-de-dados) |
| Acionamento remoto | 8 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/12-Acionamento_remoto.html#requisicao) |
| FUOTA | 9 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/13-FUOTA.html#requisicao) |
| Zerar energias | 10 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/14-Zerar_energias.html#requisicao) |
| Mudar período de medidas | 12 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/16-Medidas_sincronas_assincronas.html#requisicao) |
| Agendamento | 13 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/8-Agendamento.html#formatacao-dos-dado-pela-camada-de-comunicacao) |
| Verificar estado do relé | 14 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/17-Atualizacao_estado_rele.html#requisicao) |
| Executar etapa de calibração | 17 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/23-Calibracao_remota.html#requisicao) |
| Obter medidas assíncronas | 18 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/16-Medidas_sincronas_assincronas.html#requisicao-2) |
| Alterar ou capturar limites de alarmes do medidor | 19 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/24-Alarmes_do_medidor.html#requisicao) |

## Mensagens Dispositivo -> Servidor
### Retorno de execução
Uma mensagem de retorno será enviado do firmware para o backend, informando se a ação requerida foi executada corretamente pelo módulo.

#### Pacotes JSON dos retornos de execução
| Operação | Tipo | Pacote JSON |
|--------|--------|-------------|
| Recuperação de medidas | 2 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/10-Recuperacao_de_medidas.html#resposta-retorno-de-execucao-da-requisicao) |
| Acionamento remoto | 8 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/12-Acionamento_remoto.html#resposta-retorno-de-execucao-da-requisicao) |
| FUOTA | 9 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/13-FUOTA.html#resposta-retorno-de-execucao-da-requisicao) |
| Zerar energias | 10 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/14-Zerar_energias.html#resposta-retorno-de-execucao-da-requisicao) |
| Mudar período de medidas | 12 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/16-Medidas_sincronas_assincronas.html#resposta) |
| Executar etapa de calibração | 17 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/23-Calibracao_remota.html#resposta) |
| Obter medidas assíncronas | 18 | [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/16-Medidas_sincronas_assincronas.html#resposta-medidas-assincronas) |

### Medidas síncronas
Medidas são enviadas periodicamente para o backend.
O pacote com as medidas periódicas terá mais de um tipo de payload: monofásico ou trifásico.

#### Monofásico
Para mais informações siga para o [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/16-Medidas_sincronas_assincronas.html#medidas-sincronas).

#### Trifásico
(TBD).

### Recuperação de medidas
O pacote com as medidas recuperadas terá mais de um tipo de payload: monofásico ou trifásico. Podendo haver entre 1 à 4 medidas por pacote.

#### Monofásico
Para mais informações siga para o [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/10-Recuperacao_de_medidas.html#monofasico).

#### Trifásico
(TBD).

### Mensagem de inicialização do dispositivo
A mensagem de inicialização será enviada pelo módulo ao backend toda vez que o mesmo iniciar.<br>

Para mais informações siga para o [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/18-Mensagem_inicializacao_dispositivo.html#mensagem-de-inicializacao).

### Comissionamento
As informações do circuito podem ser enviadas através do módulo, para facilitar em sua dentificação na interface.<br>

Para mais informações siga para o [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/9-Comissionamento.html#topico).

### Atualização de estado do relé
Este pacote é enviado a cada 30 segundos, ou por requisição, ou toda vez que o relé do módulo for acionado, ele também informa a origem do acionamento.<br>

Para mais informações siga para o [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/17-Atualizacao_estado_rele.html#resposta).

## Mensagens Software de fábrica -> Servidor
### Cadastramento do módulo
O pacote de cadastramento do módulo será enviado para cadastrar no banco de dados um módulo recém-criado.<br>

Para mais informações siga para o [**link**](https://iose-wifi.gitlab.io/documentacao-iose/firmware/19-Cadastramento_do_modulo.html#cadastramento)
