﻿# Protocolo para geração de versão de firmware

Modificações no firmware final salvo em fábrica devem seguir os passos:

1) todas as modificações necessárias devem estar contidas no branch do git o qual o software de fábrica utilizará como base para geração do binário a ser gravado;
2) essa modificação deve ser sinalizada por email por parte do líder de firmware ao líder de sorftware e encaminhado ao gerente do projeto e demais interessados, contendo:

 * nome do branch do git a ser utilizado;
 * hash do commit do branch a ser utilizado;
 * hash do partition_table que se espera ser gerado no build;
 * justificativa(s) da modificação.

3) esse email deve ser respondido pelo líder de software ao líder de firmware contendo:

 * nome do branch do git utilizado;
 * hash do commit do branch utilizado;
 * hash do partition_table gerado no build.

4) o conteúdo deste último email deve ser verificado pelo líder de firmware e repondido ao líder de software se o protocolo pode ser dado como finalizado ou não. 