﻿# Zerar Energias
# Introdução
Este documento especifica a funcionalidade de _Zerar Energias_. Nesta especificação serão definidos:

* tópicos de requisição e resposta para a zerar o acumulado de energias;
* formatação de dados trocados entre software e firmware: requisição e resposta;
* comportamento esperado do dispositivo para realização da operação.

Esta especificação deve, ao seu fim, ser de tal forma que o dispositivo esteja apto a receber a requisição, reiniciar a contagem do acúmulo de energias e enviar a mensagem de retorno para o tópico específico.

# Comunicação MQTT
Nesta secção são determinados os tópicos de onde são feitos as requisições e para onde serão enviadas as medidas solicitadas e seus respectivos pacotes JSON.

## Requisição
### Tipo
| Operação | Tipo |
|----------|------|
| Zerar energiar | 10 |

### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |
| Produção | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_req | string | Identificador único da requisição (máx. 63 caracteres) |
| type | integer | Indica o tipo de requisição |

#### Exemplo
```json
{
  "uuid_req": "123456789",
  "type": 10
}
```

## Resposta - Retorno de execução da requisição
### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/return |
| Produção | prod/iose/general/return  |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único da módulo |
| uuid_req | string | Identificador único da requisição recebida (máx. 63 caracteres) |
| req_type | integer | Tipo de requisição recebida |
| success | boolean | Estado de execução |
| timestamp | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo
```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_req": "123456789",
    "req_type": 2,
    "success": true,
    "timestamp": "2020-09-23T18:54:12.000Z"
}
```

# Comportamento Esperado
## Falha
Abaixo segue os casos de falha e o comportamento.
| Falha | Comportamento |
|-------|---------------|
| Pacote JSON inválido | Enviará pacote de retorno com req_type = 0 e success = false |
| Falha na execução | Enviará pacote de retorno com req_type = 10 e success = false |

## Sucesso
O módulo irá reiniciar a contagem no acúmulo de eergias, enviar a mensagem de retorno para o seu respectivo tópico.