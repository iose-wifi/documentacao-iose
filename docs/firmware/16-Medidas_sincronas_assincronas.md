﻿# Medidas Síncronas e Assíncronas
# Introdução
Este documento especifica a funcionalidade de _Medidas Síncronas e Assíncronas_. Nesta especificação serão definidos:

* tópico de envio das medidas síncronas;
* tópico de requisição e resposta da alteração de período de medidas;
* tópico de requisição e resposta das medidas assíncronas;
* formatação de dados trocados entre software e firmware;
* comportamento esperado do dispositivo para realização da operação.

Esta especificação deve, ao seu fim, ser de tal forma que o dispositivo esteja apto a enviar periodicamente medidas elétricas, alterar seu período e obter medidas assíncronas.

# Comunicação MQTT
Nesta secção são determinados os tópicos de onde são feitos as requisições e para onde serão enviadas as medidas solicitadas e seus respectivos pacotes JSON.

## Medidas Síncronas

As informações de medidas, estado do medidor e do relé, são enviados periodicamente para o broker MQTT na nuvem.
### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/measures |
| Produção | prod/iose/general/measures |

### Monofásico
### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| a | string | Identificador único do módulo |
| b | float | Tensão RMS (V) |
| c | float | Frequência da rede (Hz) |
| d | float | Temperatura interna (leitura ADC) |
| e | float | Fator de potência (entre -1.00 e 1.00) |
| f | float | Corrente RMS (A) |
| g | float | Potência Ativa (W) |
| h | float | Potência Reativa (VAr) |
| i | float | Potência Aparente (VA) |
| j | float | Energia Ativa consumida (Wh) |
| k | float | Energia Ativa gerada (Wh) |
| l | float | Energia Reativa consumida (VArh) |
| m | float | Energia Reativa gerada (VArh) |
| n | integer | [Registrador System Status](https://gitlab.com/iose-wifi/documentacao-iose/-/raw/master/docs/firmware/anexos/meter/mcp39f5x1/MCP39F511A%20-%20Datasheet.pdf#page=33?inline=true) (uint16) |
| o | boolean | Estado do relé |
| p | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |
| q | integer | Índice da medida |

#### Exemplo
```json
{
    "a": "AABBCCDDEEFF",		
    "b": 227,
    "c": 60,
    "d": 100,
    "e": 5,
    "f": 10,
    "g": 25,
    "h": 25,
    "i": 20,
    "j": 30,
    "k": 30,
    "l": 30,
    "m": 30,
    "n": 1010,
    "o": true,
    "p": "2020-09-23T18:54:12.000Z",
    "q": 1
}
```

### Comportamento Esperado
#### Falha
Abaixo segue os casos de falha e o comportamento.
| Falha | Comportamento |
|-------|---------------|
| Medida inválida | Nenhum pacote de medidas será enviado |

#### Sucesso
O módulo irá receber as informações de medidas armazenar e enviá-la para o seu respectivo tópico a cada período determinado.
## Casos de Uso

|  Condição / Fluxo | Descrição / Dado trocado |
|-|-|
| Ator Principal | Módulo e MQTT (Cloud) |
| Ator Secundário | Rotina de aferição e medidor |
| Pré-condição | Módulo não envia medidas entre um determinado período |
| Pós-condição | Módulo envia medidas ao atingir o período de tempo |
| Fluxo Principal | <br>1) Módulo envia **medidas síncronas**: <br><br>&emsp;Executa rotina de aferição, medidor responde sucesso, medidas são montadas para envio e armazenamento; <br><br>2) Módulo envia as medidas síncronas:<br><br>&emsp;json-msg(AABBCCDDEEFF, 220, 60, 100, 5, 10, 25, 25, 20, 30, 30, 30, 30, 2562, true, 123, 2021-01-21T23:01:22.000Z).<br><br>
| Fluxo Alternativo | <br>1) Módulo não envia **medidas síncronas**: <br><br>&emsp;Falha ao executar rotina de aferição, ou ao montar as medidas; <br><br>2) Módulo não envia as medidas síncronas.<br><br>
## Alterar Período De Medidas Síncronas
A alteração no período de medidas pode ser feito remotamente via requisição MQTT, enviando o valor desejado levando em conta o valor padrão e o limite mínimo listado abaixo.

| Padrão | Mínimo |
|----------|--------|
| 15min (900s) | 30s |

### Requisição
#### Tipo
| Operação | Tipo |
|----------|------|
| Mudar período de medidas | 12 |

#### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |
| Produção | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |

#### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_req | string | Identificador único da requisição (máx. 63 caracteres) |
| type | integer | Indica o tipo de requisição |
| payload | struct | Contém o novo período |
| period | integer | Novo valor de período em segundos (**Mín: 30s**) |

#### Exemplo
```json
{
    "uuid_req": "123456789",
    "type": 12,
    "payload":
    {   
        "period": 30
    }
}
```
### Resposta
#### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/return |
| Produção | prod/iose/general/return  |

#### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único da módulo |
| uuid_req | string | Identificador único da requisição recebida (máx. 63 caracteres) |
| req_type | integer | Tipo de requisição recebida |
| success | boolean | Estado de execução |
| timestamp | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo
```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_req": "123456789",
    "req_type": 12,
    "success": true,
    "timestamp": "2020-09-23T18:54:12.000Z"
}
```

# Comportamento Esperado
## Falha
Abaixo segue os casos de falha e o comportamento.
| Falha | Comportamento |
|-------|---------------|
| Pacote JSON inválido | Enviará pacote de retorno com req_type = 0 e success = false |
| Falha na execução | Enviará pacote de retorno com req_type = 10 e success = false |

## Casos de Uso

|  Condição / Fluxo | Descrição / Dado trocado |
|-|-|
| Ator Principal | Módulo e MQTT (Cloud) |
| Ator Secundário | Rotina de modificação e armazenamento do período |
| Pré-condição | Usuário necessita alterar período de medidas síncronas |
| Pós-condição | Módulo envia medidas no período novo |
| Fluxo Principal | <br>1) Módulo recebe uma requisição para **alterar período de medidas**: <br><br>&emsp;Executa rotina de modificação do período usado na aplicação e no valor armazenado; <br><br>2) Módulo envia mensagem de sucesso na operação:<br><br>&emsp;json-msg(AABBCCDDEEFF, 123456789, 12, true, 2021-01-21T23:01:22.000Z).<br><br>
| Fluxo Alternativo | <br>1) Módulo recebe uma requisição para **alterar período de medidas**: <br><br>&emsp;Falha ao executar rotina de modificação do período usado na aplicação ou no armazenamento do novo valor; <br><br>2) Módulo envia mensagem de falha na operação:<br><br>&emsp;json-msg(AABBCCDDEEFF, 123456789, 12, false, 2021-01-21T23:01:22.000Z).<br><br> |

## Medidas Assíncronas
Caso seja necessária uma aferição de medidas instantâneas no módulo IOSE é possível obtê-las remotamente.

### Requisição
#### Tipo
| Operação | Tipo |
|----------|------|
| Obter medidas assíncronas | 18 |

#### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |
| Produção | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |

#### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_req | string | Identificador único da requisição (máx. 63 caracteres) |
| type | integer | Indica o tipo de requisição |

#### Exemplo
```json
{
    "uuid_req": "123456789",
    "type": 18
}
```
### Resposta - Medidas assíncronas
#### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/measures |
| Produção | prod/iose/general/measures |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_req | string | Identificador único da requisição recebida (máx. 63 caracteres) |
| a | string | Identificador único do módulo |
| b | float | Tensão RMS (V) |
| c | float | Frequência da rede (Hz) |
| d | float | Temperatura interna (leitura ADC) |
| e | float | Fator de potência (entre -1.00 e 1.00) |
| f | float | Corrente RMS (A) |
| g | float | Potência Ativa (W) |
| h | float | Potência Reativa (VAr) |
| i | float | Potência Aparente (VA) |
| j | float | Energia Ativa consumida (Wh) |
| k | float | Energia Ativa gerada (Wh) |
| l | float | Energia Reativa consumida (VArh) |
| m | float | Energia Reativa gerada (VArh) |
| n | integer | [Registrador System Status](https://gitlab.com/iose-wifi/documentacao-iose/-/raw/master/docs/firmware/anexos/meter/mcp39f5x1/MCP39F511A%20-%20Datasheet.pdf#page=33?inline=true) (uint16) |
| o | boolean | Estado do relé |
| p | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo
```json
{
    "uuid_req": "123456789",
    "a": "AABBCCDDEEFF",		
    "b": 227,
    "c": 60,
    "d": 100,
    "e": 5,
    "f": 10,
    "g": 25,
    "h": 25,
    "i": 20,
    "j": 30,
    "k": 30,
    "l": 30,
    "m": 30,
    "n": 1010,
    "o": true,
    "p": "2020-09-23T18:54:12.000Z"
}
```
## Resposta - Caso falha da requisição
### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/return |
| Produção | prod/iose/general/return  |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único da módulo |
| uuid_req | string | Identificador único da requisição recebida (máx. 63 caracteres) |
| req_type | integer | Tipo de requisição recebida |
| success | boolean | Estado de execução |
| timestamp | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo
```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_req": "123456789",
    "req_type": 18,
    "success": false,
    "timestamp": "2020-09-23T18:54:12.000Z"
}
```

## Casos de Uso

|  Condição / Fluxo | Descrição / Dado trocado |
|-|-|
| Ator Principal | Módulo e MQTT (Cloud) |
| Ator Secundário | Rotina de aferição e medidor |
| Pré-condição | Usuário necessita de medidas instantâneas |
| Pós-condição | Módulo envia medidas instantâneas |
| Fluxo Principal | <br>1) Módulo recebe uma requisição para **medidas assíncronas**: <br><br>&emsp;Executa rotina de aferição e montagem de medidas assíncronas, medidor responde sucesso, medidas são capturadas; <br><br>2) Módulo envia as medidas assíncronas:<br><br>&emsp;json-msg(123456789, AABBCCDDEEFF, 220, 60, 100, 5, 10, 25, 25, 20, 30, 30, 30, 30, 2562, true, 2021-01-21T23:01:22.000Z).<br><br>
| Fluxo Alternativo | <br>1) Módulo recebe uma requisição para **medidas assíncronas**: <br><br>&emsp;Falha ao executa rotina de aferição ou montagem de medidas assíncronas, medidor responde falha <br><br>2) Módulo envia msg de falha na requisição:<br><br>&emsp;json-msg(AABBCCDDEEFF, 123456789, 18, false, 2021-01-21T23:01:22.000Z).<br><br> |