﻿# Requisitos do Firmware modo Fábrica.

O firmware do modo fábrica consiste nos comandos para execução de rotinas no hardware para cada um dos testes que serão realizados no módulo, de forma a garantir que o software de IHM receba um código de sucesso ou falha em cada teste realizado.
Para isso, os seguintes requisitos foram levantados e analisados, dos quais precisam ser alcançados para o módulo responder de forma adequada ao software:

### Teste dos LEDs.

Ao iniciar,o módulo irá piscar duas vezes seus 3 LEDs e após isso permanecerão acesos, dessa forma o usuário poderá inspecionar os LEDs e verificar se todos estão ligados corretamente. Os LEDs também poderão ser validados por meio de um comando enviado pelo Software de Fábrica.

### Teste dos botões.

Os 3 botões presentes no módulo irão se comportar da seguinte maneira no Firmware de Fábrica:

* Se o botão "Function" for pressionado o LED "Online" irá mudar de estado,podendo acender ou apagar;
* Se o botão "Relay" for pressionado o LED "Load" irá mudar de estado,podendo acender ou apagar;
* Se o botão "Reset" for pressionado todos os LEDs irão piscar duas vezes.


### Teste da conexão WiFi

A ESP irá iniciar a interface e conexão WiFi em modo station, usando o SSID e senha fornecidos pelo software. Caso haja uma confirmação de IP válido será enviada ao software uma mensagem de sucesso.

### Teste da comunicação WiFi.

A ESP irá tentar uma comunicação via socket TCP/IP com o software de fábrica, enviando ma mensagem. O software deverá aguardar e interpretar uma resposta válida, caso tenha sucesso, o teste passou, caso contrário o módulo falhou no teste.

### Teste da comunicação com o MCP.

A ESP irá inicializar a interface UART com o MCP e enviará um comando para ler o registrador "System Version", será aguardado e interpretado uma resposta do MCP. Caso seja uma resposta válida será enviada ao software uma mensagem de sucesso.

### Teste do relé.

A ESP irá mandar acionar o relé,o qual estará conectado na energia,e irá detectar fase no sensor MCP caso o relé esteja funcionando adequadamente.

### Validação da calibração sem carga.
O módulo irá tentar realizar uma calibração sem carga no sensor MCP. Caso a calibração seja bem sucedida,será enviada uma mensagem de sucesso.

### Validação da calibração de fator de potência 0,5.
O módulo irá tentar realizar uma calibração de fator de potência 0,5 no sensor MCP. Caso a calibração seja bem sucedida,será enviada uma mensagem de sucesso.

### Validação da calibração de fator de potência 1.
O módulo irá tentar realizar uma calibração de fator de potência 1 no sensor MCP. Caso a calibração seja bem sucedida,será enviada uma mensagem de sucesso.

### Validação do reset de energias acumuladas.
O módulo irá tentar realizar um reset de energias acumuladas no sensor MCP. Caso o reset seja bem sucedido,será enviada uma mensagem de sucesso.



