﻿# Recuperação de Medidas
# Introdução
Este documento especifica a funcionalidade de _Recuperação de Medidas_. Nesta especificação são definidos:

* tópicos de requisição e resposta para a recuperação de medidas;
* formatação de dados trocados entre software e firmware: requisição e resposta;
* comportamento esperado do dispositivo para realização da operação.

Esta especificação deve, ao seu fim, ser de tal forma que o dispositivo esteja apto a receber a requisição, buscar a informação e enviar as medidas no intervalo de índices solicitado para o tópico específico.

# Comunicação MQTT
Nesta secção são determinados os tópicos de onde são feitos as requisições e para onde serão enviadas as medidas solicitadas e seus respectivos pacotes JSON.

## Requisição
### Tipo
| Operação | Tipo |
|----------|------|
| Recuperação de Medidas | 2 |

### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |
| Produção | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_req | string | Identificador único da requisição (máx. 63 caracteres) |
| type | integer | Indica o tipo de requisição |
| payload | struct | Contém o intervalo de índices |
| s_index | integer | Índice inicial |
| e_index | integer | Índice final |

#### Exemplo
```json
{
    "uuid_req": "123456789",
    "type": 2,
    "payload":
    {   
        "s_index": 0,
        "e_index": 100
    }
}
```
## Resposta - Retorno de execução da requisição
### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/return |
| Produção | prod/iose/general/return  |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único da módulo |
| uuid_req | string | Identificador único da requisição recebida (máx. 63 caracteres) |
| req_type | integer | Tipo de requisição recebida |
| success | boolean | Estado de execução |
| timestamp | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo
```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_req": "123456789",
    "req_type": 2,
    "success": true,
    "timestamp": "2020-09-23T18:54:12.000Z"
}
```
## Resposta - Medidas recuperadas
### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/recover_measures |
| Produção | prod/iose/general/recover_measures  |

### Monofásico
### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_req | string | Identificador único da requisição (máx. 63 caracteres) |
| a | string | Identificador único do módulo |
| measures | array | Contém as medidas recuperadas |
| b | float | Tensão RMS (V) |
| c | float | Frequência da rede (Hz) |
| d | float | Temperatura interna (leitura ADC) |
| e | float | Fator de potência (entre -1.00 e 1.00) |
| f | float | Corrente RMS (A) |
| g | float | Potência Ativa (W) |
| h | float | Potência Reativa (VAr) |
| i | float | Potência Aparente (VA) |
| j | float | Energia Ativa consumida (Wh) |
| k | float | Energia Ativa gerada (Wh) |
| l | float | Energia Reativa consumida (VArh) |
| m | float | Energia Reativa gerada (VArh) |
| n | integer | [Registrador System Status](https://gitlab.com/iose-wifi/documentacao-iose/-/raw/master/docs/firmware/anexos/meter/mcp39f5x1/MCP39F511A%20-%20Datasheet.pdf#page=33?inline=true) (uint16) |
| o | boolean | Estado do relé |
| p | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |
| q | integer | Índice da medida |

#### Exemplo
```json
{
  "uuid_req": "123456789",
  "a": "AABBCCDDEEFF",
  "measures": [
    {
      "b": 216.7,
      "c": 60.09,
      "d": 274,
      "e": 0.63,
      "f": 0.04,
      "g": 6.3,
      "h": 4.8,
      "i": 9.9,
      "j": 131868.16,
      "k": 0.01,
      "l": 6321.7,
      "m": 9669.21,
      "n": 24624,
      "o": false,
      "p": "2021-02-17T22:36:48.000Z",
      "q": 0
    },
    {
      "b": 216.9,
      "c": 60.09,
      "d": 273,
      "e": 0.67,
      "f": 0.04,
      "g": 5.9,
      "h": 4.3,
      "i": 8.8,
      "j": 131868.25,
      "k": 0.01,
      "l": 6321.78,
      "m": 9669.21,
      "n": 24624,
      "o": false,
      "p": "2021-02-17T22:37:48.000Z",
      "q": 1
    },
    {
      "b": 213.1,
      "c": 60.19,
      "d": 279,
      "e": 0.65,
      "f": 0.04,
      "g": 6.1,
      "h": 4.5,
      "i": 9.3,
      "j": 131868.35,
      "k": 0.01,
      "l": 6321.85,
      "m": 9669.21,
      "n": 24624,
      "o": false,
      "p": "2021-02-17T22:38:48.000Z",
      "q": 2
    },
    {
      "b": 213.5,
      "c": 60.14,
      "d": 277,
      "e": 0.64,
      "f": 0.04,
      "g": 6,
      "h": 4.4,
      "i": 9.3,
      "j": 131868.45,
      "k": 0.01,
      "l": 6321.93,
      "m": 9669.21,
      "n": 24624,
      "o": false,
      "p": "2021-02-17T22:39:48.000Z",
      "q": 3
    }
  ]
}
```
# Comportamento Esperado
## Falha
Abaixo segue os casos de falha e o comportamento.
| Falha | Comportamento |
|-------|---------------|
| Pacote JSON inválido | Enviará pacote de retorno com req_type = 0 e success = false |
| Nenhuma medida encontrada no intervalo | Enviará pacote de retorno com req_type = 2 e success = false |
## Sucesso
O módulo irá buscar no armazenamento de medidas e retornar um pacote com o estado de sucesso da requisição e outro pacote contendo atá 4 medidas dentro do intervalo solicitado para os seus respectivos tópicos.