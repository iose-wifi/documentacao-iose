﻿# Mensagem de Inicialização do Dispositivo
# Introdução
Este documento especifica a funcionalidade de _Mensagem de Inicialização do Dispositivo_. Nesta especificação serão definidos:

* tópico de envio da mensagem de inicialização do dispositivo;
* formatação de dados trocados entre software e firmware;
* comportamento esperado do dispositivo para realização da operação.

Esta especificação deve, ao seu fim, ser de tal forma que o dispositivo esteja apto a enviar na sua inicialização uma mensagem com a versão do firmware atual para o tópico específico.

# Comunicação MQTT
Nesta secção são determinados os tópicos de onde são feitos as requisições e para onde serão enviadas as medidas solicitadas e seus respectivos pacotes JSON.

## Mensagem de inicialização
### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/device_start |
| Produção | prod/iose/general/device_start |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único do módulo |
| fw_version | string | Versão do firmware no formato X.Y.Z, onde X, Y, e Z são inteiros não negativos e não devem conter zeros à esquerda. X é a versão maior, Y é a versão menor e Z é a versão de correção |
| hw_version | string | Versão do hardware no formato Xph_vYrZ, onde X, Y, e Z são inteiros não negativos e não devem conter zeros à esquerda. X é o número de fases do módulo, Y é a versão e Z é a revisão |
| boot_times | integer | Número de vezes em que o dispositivo foi inicializado |
| runtime | integer | Tempo de última execução do dispositivo (segundos)  |
| timestamp | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo
```json
{
    "uuid_circuit":	"AABBCCDDEEFF",
    "fw-version": "0.0.0",
    "hw-version": "1ph_v1r1",
    "boot_times": 1,
    "last_runtime": 123,
    "timestamp": "2020-09-28T13:36:26.000Z"
}
```

# Comportamento Esperado
O módulo irá enviar o pacote após inicializar e sincronizar o horário com o servidor NTP para o seu respectivo tópico.