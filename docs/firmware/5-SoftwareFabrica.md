﻿# Software de teste do Modo Fábrica
Nesse documento será feita a análise do software de teste do modo fábrica,assim como será feito seu fluxograma e descrição de cada estapa. O software de teste tem como função testar todos os requisitos do processo
de fábrica do módulo,asism como suas funcionalidades.

## Protocolo do modo Fábrica:
Foi desenvolvido um pequeno protocolo para realizar todos os testes do modo fábrica. Todas as mensagens trocadas entre o módulo e a aplicação 
são codificadas em ASCII,posuem caractere fim de linha "\n" ao final e são enviadas por comunicação serial por padrão, também possuem um código de identificação do conteúdo da mensagem:
1. **100 "Confirmação Inicial"**: Comando enviado do software para o módulo requisitando seu Id,o módulo irá responder com o mesmo comando contendo seu Id. **Exemplo: "100\n" ou "100,123456789\n"**
1. **101 "Envio de Parâmetros"**: Comando enviado do software para o módulo avisando que ele foi escutado,contém os parâmetros na ordem: IP do software,SSID de WiFi,senha de WiFi e porta WiFi. **Exemplo: "101,192.168.0.101,Rede_WiFi,12345678,9999\n"**
1. **200 "Teste de WiFi"**:	Comando enviado do software para o módulo solicitando teste de WiFi. **Exemplo: "200\n"**
1. **201 "Confirmação de WiFi"**: Comando enviado do módulo para o software via **WiFi** para validar comunicação WiFi. **Exemplo: "201\n"**
1. **300 "Teste de MCP"**: Comando enviado do software para o módulo solicitando teste de sensor MCP. **Exemplo: "300\n"**
1. **301 "Confirmação de MCP"**: Comando enviado do módulo para o software informando sucesso ou erro do teste do sensor MCP. **Exemplo: "301\n"**
1. **302 "Calibração de FP 1"**: Comando enviado do software para o módulo solicitando calibração de fator de potência 1. **Exemplo: "302\n"**
1. **303 "Confirmação de calibração de FP 1"**: Comando enviado do módulo para o software informando sucesso ou erro na calibração de fator de potência 1. **Exemplo: "303\n"**
1. **304 "Calibração de FP 0,5"**: Comando enviado do software para o módulo solicitando calibração de fator de potência 0,5. **Exemplo: "304\n"**
1. **305 "Confirmação de calibração de FP 0,5"**: Comando enviado do módulo para o software informando sucesso ou erro na calibração de fator de potência 0,5. **Exemplo: "305\n"**
1. **306 "Calibração sem carga"**: Comando enviado do software para o módulo solicitando calibração sem carga. **Exemplo: "306\n"**
1. **307 "Confirmação de calibração sem carga"**: Comando enviado do módulo para o software informando sucesso ou erro na calibração sem carga. **Exemplo: "307\n"**
1. **308 "Reset de energias"**: Comando enviado do software para o módulo solicitando o reset de energias acumuladas no sensor MCP. **Exemplo: "308\n"**
1. **309 "Confirmação de Reset de energias"**: Comando enviado do módulo para o software informando sucesso ou erro no reset de energias acumuladas. **Exemplo: "309\n"**
1. **310 "Requisição de medidas de MCP"**: Comando solicitando as medidas lidas pelo sensor MCP,com o intuito de verificar se o módulo está devidamente calibrado. É retornado com o mesmo comando,seguido dos seguintes valores: tensão,corrente,potência ativa,potência reativa,fator de potência e frequência. **Exemplo de envio: "310\n"**. **Exemplo de retorno: "310,220,5,1000,1000,1,60\n"**
1. **400 "Teste de Relé"**: Comando enviado do software para o módulo solicitando teste de relé. **Exemplo: "400\n"**
1. **401 "Confirmação de Relé"**: Comando enviado do módulo para o software informando sucesso ou erro do teste de relé. **Exemplo: "401\n"**
1. **500 "Piscar LED"**: Comando enviado do software para o módulo solicitando piscagem de LED.


## Fluxo do Software de Fábrica
![imagem 1](./imagens/fluxograma_sw_fabrica_v2.png)
1. É verificado se o HUB USB já foi previamente calibrado correspondendo com a ordem da placa de testes.(**E1**)
1. São inseridos na Tela de Configuração os parâmetros essenciais para os testes: Nome da rede WiFi,senha da rede WiFi,porta de comunicação WiFi e Código Hash do Dono.
1. É verificado se as portas USBs do HUB foram ordenadas corretamente.(**E2**)
1. É realizada a bipagem de todos os módulos com um leitor de QR Code.
1. É iniciado o fluxograma de **Testes de Pré-Calibração**.
1. É verificado se os testes de Pré-Calibração foram todos concluídos com sucesso.(**E3**)
1. É iniciado o fluxograma de **Calibração sem Carga**.
1. É iniciado o fluxograma de **Calibração de Fator de Potência 1**.
1. É iniciado o fluxograma de **Calibração de Fator de Potência 0,5**.
1. É verificado se os testes anteriores foram todos concluídos com sucesso.(**E4**)
1. É iniciado o fluxograma de **Testes de Pós-Calibração**.
1. É feita a visualização dos resultados gerais de todos os testes. Se todos os testes tiverem sido validados,o módulo é aprovado.
1. É iniciada uma nova bateria de testes, fluxograma é direcionado à etapa de **Testes de Pré-Calibração**.(**E5**)

### Fluxo de exceção do Software de Fábrica
#### E1. HUB USB não foi previamente calibrado
- E1.1 O operador deverá conectar o HUB USB ao computador.
- E1.2 O operador irá iniciar o processo de calibração no Software de Fábrica.
- E1.3 Após o processo de calibração ser concluído, o fluxo retorna de onde parou e continua.

#### E2. Portas USBs do HUB não foram ordenadas corretamente
- E2.1 É realizado o fluxograma da **Ordenação de Portas USBs**.
- E2.2 Após o processo de ordenação ser concluído, o fluxo retorna de onde parou e continua.

#### E3. Os testes de Pré-Calibração não foram bem sucedidos
- E3.1 O fluxograma é direcionado para Testes de Pré-Calibração.
- E3.2 Os testes de pré-calibração são feitos novamente.

#### E4. Os testes anteriores à Pós-Calibração não foram bem sucedidos
- E4.1 É verificado qual teste obteve falha.
- E4.2 O fluxograma é direcionado para o teste em que houve falha.

#### E5. Não é iniciada uma nova bateria de testes.
- E5.1 O Software de Fábrica é encerrado.


## Fluxo da Ordenação de Portas USBs
![imagem 2](./imagens/fluxograma_ordenacao_usb.png)
1. É feita a escolha de uma das posições calibradas no Software de Fábrica.
1. É feito o acionamento de LED do módulo apertando o botão de piscar LED na posição selecionada.
1. É feita a verificação se o Módulo que piscou está na posição correspondete do Software de Fábrica.(**E1**)
1. É feita a verificação se todas as posições estão devidamente ordenadas.(**E2**)
1. É concluída a Ordenação de Portas USBs

### Fluxo de exceção da Ordenação de Portas USBs
#### E1. Módulo que piscou não corresponde à posição selecionada
- E1.1 Operador abre a caixa de seleção ComboBox da posição selecionada.
- E1.2 Operador seleciona em qual índice a posição selecionada deveria estar.
- E1.3 Fluxograma retorna de onde parou e continua.

#### E2. Posições ainda não estão corretamente ordenadas
- E2.1 Fluxograma é redirecionado para a primeira etapa do fluxo principal.


## Fluxo da Pré-Calibração
![imagem 3](./imagens/fluxograma_pre_calibracao.png)
1. É realizado o processo de gravação de firmware de fábrica pelo software.(**E1**)
1. É realizado o envio de um comando **100** pelo software que irá receber uma resposta **100** do módulo contendo seu identificador único,validando a comunicação Serial.(**E2**)
1. É realizado o  envio de um comando **101** para o módulo contendo os parâmetros do teste.
1. É recebida uma respota do módulo contendo o comando de confirmação **102**,indicando que a mensagem anterior foi recebida com sucesso.(**E2**)
1. É realizado o envio de um comando **400** para o módulo solicitando teste de Relé.(**E3**)
1. É recebida uma resposta do módulo contendo o comando de confirmação **401**, validando o teste de relé. Se o comando de confirmação não for recebido, teste de relé é invalidado.
1. É realizado o envio de um comando **200** para o módulo solicitando teste de WiFi.
1. É recebida uma resposta do módulo contendo o comando de confirmação **201**, validando a conexão e comunicação WiFi. Se o comando de confirmação não for recebido, teste de WiFi é invalidado.
1. É realizado o envio de um comando **300** para o módulo solicitando teste do sensor MCP.
1. É recebida uma resposta do módulo contendo o comando de confirmação **301**, validando a comunicação com o sensor MCP. Se o comando de confirmação não for recebido, teste de MCP é invalidado.
1. É realizado uma verificação visual dos LEDs pelo operador, que irá informar ao software se os LEDs foram validados ou não.
1. É realizado uma verificação manual e visual dos Botões pelo operado, que irá informar ao software se os Botões foram validados ou não.
1. É realizado a geração de logs de cada uma das etapas do teste e gravado em um arquivo txt.

### Fluxo de exceção

#### E1. Firmware não foi gravado com sucesso
- E1.1 Software irá dar como falho a gravação de firmware de fábrica.
- E1.2 Software irá desviar fluxo para a etapa de geração de Log.
- E1.3 Etapa de pré-calibração é encerrada.

#### E2. Software não recebeu mensagem de confirmação do módulo.
- E2.1 Software irá dar como falho a confirmação de comunicação serial.
- E2.2 Software irá desviar fluxo para a etapa de geração de Log.
- E2.3 Etapa de pré-calibração é encerrada.

#### E3. Módulo não possui Relé
- E3.1 Software não realiza teste de relé.
- E3.2 Software  irá desviar fluxo para etapa de validação WiFi.

## Fluxo da Calibração sem Carga
![imagem 4](./imagens/fluxograma_calibracao_sem_carga.png)
1. É realizado o envio de um comando **100** pelo software que irá receber uma resposta **100** do módulo contendo seu identificador único,validando a comunicação Serial.(**E1**)
1. É selecionado o modo de aquisição de medidas sem carga no software.(**E2**)
1. São requisitadas as medidas ao módulo,cujo o qual responde com um comando **310**,contendo todas as medidas.
1. É verificado se as medidas recebidas estão dentro da tolerância.(**E3**)
1. É validada a Calibração sem Carga.
1. É realizado a geração de logs de cada uma das etapas do teste e gravado em um arquivo txt.

### Fluxo de exceção
#### E1. Software não recebeu mensagem de confirmação do módulo.
- E1.1 Software irá dar como falho a confirmação de comunicação serial.
- E1.2 Software irá desviar fluxo para a etapa de geração de Log.
- E1.3 Etapa de calibração sem carga é encerrada.

#### E2. Aquisição de medidas não é selecionada.
- E2.1 É feita a seleção da calibração sem carga ao invés.
- E2.2 É realizada a calibração sem carga no módulo.
- E2.3 O fluxograma é redirecionado para **Recebe medidas do Módulo**.

#### E3. Medidas recebidas não estão dentro da tolerância.
- E3.1 A calibração sem carga é mal sucedida.
- E3.2 É realizado a geração de logs de cada uma das etapas do teste e gravado em um arquivo txt.

## Fluxo da Calibração de Fator de Potência 1
![imagem 5](./imagens/fluxograma_calibracao_FP_1.png)
1. É realizado o envio de um comando **100** pelo software que irá receber uma resposta **100** do módulo contendo seu identificador único,validando a comunicação Serial.(**E1**)
1. É selecionado o modo de aquisição de medidas com carga e fator de potência 1 no software.(**E2**)
1. São requisitadas as medidas ao módulo,cujo o qual responde com um comando **310**,contendo todas as medidas.
1. É verificado se as medidas recebidas estão dentro da tolerância.(**E3**)
1. É validada a Calibração de Fator de Potência 1.
1. É realizado a geração de logs de cada uma das etapas do teste e gravado em um arquivo txt.

### Fluxo de exceção
#### E1. Software não recebeu mensagem de confirmação do módulo.
- E1.1 Software irá dar como falho a confirmação de comunicação serial.
- E1.2 Software irá desviar fluxo para a etapa de geração de Log.
- E1.3 Etapa de calibração de Fator de Potência 1 é encerrada.

#### E2. Aquisição de medidas não é selecionada.
- E2.1 É feita a seleção da calibração de Fator de Potência 1 ao invés.
- E2.2 É realizada a calibração de Fator de Potência 1 no módulo.
- E2.3 O fluxograma é redirecionado para **Recebe medidas do Módulo**.

#### E3. Medidas recebidas não estão dentro da tolerância.
- E3.1 A calibração de Fator de Potência 1 é mal sucedida.
- E3.2 É realizado a geração de logs de cada uma das etapas do teste e gravado em um arquivo txt.

## Fluxo da Calibração de Fator de Potência 0,5
![imagem 6](./imagens/fluxograma_calibracao_FP_05.png)
1. É realizado o envio de um comando **100** pelo software que irá receber uma resposta **100** do módulo contendo seu identificador único,validando a comunicação Serial.(**E1**)
1. É selecionado o modo de aquisição de medidas com carga e fator de potência 0,5 no software.(**E2**)
1. São requisitadas as medidas ao módulo,cujo o qual responde com um comando **310**,contendo todas as medidas.
1. É verificado se as medidas recebidas estão dentro da tolerância.(**E3**)
1. É validada a Calibração de Fator de Potência 0,5.
1. É realizado a geração de logs de cada uma das etapas do teste e gravado em um arquivo txt.

### Fluxo de exceção
#### E1. Software não recebeu mensagem de confirmação do módulo.
- E1.1 Software irá dar como falho a confirmação de comunicação serial.
- E1.2 Software irá desviar fluxo para a etapa de geração de Log.
- E1.3 Etapa de calibração de Fator de Potência 0,5 é encerrada.

#### E2. Aquisição de medidas não é selecionada.
- E2.1 É feita a seleção da calibração de Fator de Potência 0,5 ao invés.
- E2.2 É realizada a calibração de Fator de Potência 0,5 no módulo.
- E2.3 O fluxograma é redirecionado para **Recebe medidas do Módulo**.

#### E3. Medidas recebidas não estão dentro da tolerância.
- E3.1 A calibração de Fator de Potência 0,5 é mal sucedida.
- E3.2 É realizado a geração de logs de cada uma das etapas do teste e gravado em um arquivo txt.

## Fluxo da Pós-Calibração
![imagem 7](./imagens/fluxograma_pos_calibracao.png)

1. É realizado o envio de um comando **100** pelo software que irá receber uma resposta **100** do módulo contendo seu identificador único,validando a comunicação Serial.(**E1**)
1. É realizado a criação de uma "Coisa" AWS para o módulo,sendo o nome da Coisa o próprio endereço MAC do módulo fornecido durante a confirmação serial.(**E2**)
1. É realizado a importação dos certificados da Coisa criada para dentro do firmware final,para que o módulo possa se autenticar com os serviços da AWS.
1. É realizado o registro do módulo no banco de dados a partir de uma mensagem json enviado pelo protocolo MQTT.(**E3**)
1. É realizado o envio de um comando **308** para o módulo solicitando o reset das energias acumuladas.
1. É recebida uma resposta do módulo contendo o comando de confirmação **309**, validando o reset das energias acumuladas.(**E4**)
1. É realizado o build do firmware final,importando configurações como credenciais da rede WiFi,nome da Coisa,endpoint do servidor MQTT dentro do sdkconfig do firmware.
1. É realizado o flash do firmware final.(**E5**)
1. É realizado a geração de logs de cada uma das etapas do teste e gravado em um arquivo txt.


### Fluxo de exceção
#### E1. Software não recebeu mensagem de confirmação do módulo.
- E1.1 Software irá dar como falho a confirmação de comunicação serial.
- E1.2 Software irá desviar fluxo para a etapa de geração de Log.
- E1.3 Etapa de pós-calibração é encerrada.

#### E2. Criação da Coisa AWS falhou.
- E2.1 Software irá abortar processo de Pós-calibração.
- E2.2 Software irá desviar fluxo para a etapa de geração de Log.
- E2.3 Etapa de pós-calibração é encerrada.

#### E3. Registro do Módulo no Banco de Dados falhou.
- E3.1 Software irá abortar processo de Pós-calibração.
- E3.2 Software irá desviar fluxo para a etapa de geração de Log.
- E3.3 Etapa de pós-calibração é encerrada.

#### E4. Reset de energias acumuladas falhou.
- E4.1 Software irá abortar processo de Pós-calibração.
- E4.2 Software irá desviar fluxo para a etapa de geração de Log.
- E4.3 Etapa de pós-calibração é encerrada.

#### E5. Flash do firmware final falhou.
- E5.1 Software irá abortar processo de Pós-calibração.
- E5.2 Software irá desviar fluxo para a etapa de geração de Log.
- E5.3 Etapa de pós-calibração é encerrada.



