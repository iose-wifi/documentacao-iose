# Log de Eventos e Alarmes
# Índice
[TOC]

# 1 Eventos

Eventos são acontecimentos quaisquer que ocorram durante o funcionamento do dispositivo. Eventos tem níveis de criticidade distintos e de acordo com estes níveis diferentes ações podem ser tomados pelo dispositivo devido ao acontecimento de um dado evento.

A granularidade de registro e reporte de eventos é de segundos, uma vez que a informação de tempo utilizada é [timestamp unix](https://www.unixtimestamp.com/).

## 1.1 *Log* de eventos

Log de eventos é um catálogo de documentação de eventos decorrentes do funcionamento do dispositivo. Independente da criticidade certos eventos devem ter seu acontecimento documentado para posterior recuperação e análise.

**Essa informação deve conter data e hora de acontecimento do evento**. Deve-se notar que esse requisito é difícil de ser mantido em dispositivos que não mantenham a hora correta enquanto desligados (não possuam um [RTC](https://pt.wikipedia.org/wiki/RTC) conectado a bateria).

Uma vez armazenada, **essa informação deve poder ser posteriormente recuperada** por tipos específicos ou entre intervalos de tempos específicos.

## 1.2 Condições e comportamento

Como referido acima, os eventos devem ser *logados* pelo dispositivo e acessível para posterior recuperação, conforme será descrito mais a frente neste documento.

Em sua versão atual, o *log* de um evento é atrelado a o dispositivo ter a sua hora atualizada com relação à da rede de comunicação remota a qual ele deve se conectar:

> **Dispositivos que não estejam conectados à rede de comunicação remota e tem sua data e hora atualizadas não tem capacidade de *logar* eventos**, uma vez que a informação de instante de ocorrencia do evento não estará correta.

# 2 Alarmes

Alarme é um eventos que o sistema deve ter ciencia da ocorrencia o quanto antes for registardo, dessa forma, eventos podem ser promovidos a alarmes.

Quaisquer dos eventos *logados* pelo dispositivo podem ser reportados como alarme mediante configuração para tal. O comportamento de *report* de alarmes é global: **todos os alarmes seguem uma regra comum de comportamento de *report***.

Assim como os eventos, os alarmes são atrelados ao fato de o dispositivo ter sua hora atualizada e estar conectado à rede de comunicação remota.

## 2.1 Comportamento

Determinados eventos são enviados pelo dispositivo (uma vez configurado para tal) por uma quantidade máxima de vezes ou até que o sistema comande o dispositivo que pare de alarmar aquele evento específico.

O comando de parada de envio de alarme pelo sistema é conhecido como *acknowledge* de alarme.

A configuração de comportamento do envio de alarmes é configurável globalmente. O comportamento de envio de alarmes é regido pelos parâmetros [dados na tabela](https://gitlab.com/iose-wifi/iose-fw/-/blob/develop/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#25-configura%C3%A7%C3%A3o-de-comportamento-de-report).

Para que o dispositivo pare de enviar alarme o sistema deve enviar uma mensagem codificada como dado abaixo:

```json
{
  "uuid_req": "string up to 64 characters",
  "operation": 0,
  "time": "YYYY-mm-ddTHH:MM:SS.000Z",
  "type": integer
}
```

<p>
<details>
<summary>onde:</summary>

| Campo | Tipo | Valor |
|:-:|:-:|:-:|
| `uuid_req` | `string` | string de até 64 caracteres |
| `operation` | `integer` | `0`: como dado na tabela abaixo |
| `time` | `string` | data-hora de ocorrencia do evento, com encoding dado acima |
| `type` | `integer`| [valor do tipo do alarme](https://gitlab.com/iose-wifi/iose-fw/-/blob/c29a64d278e25ee8a6fa6149f00c180cb99179a0/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#23-decodifica%C3%A7%C3%A3o-dos-eventos) em andamento a ser parado |

</details>
</p>

| Operation | Valor |
|:-:|:-:|
| `acknowledge` | 0 |
| `read` | 1 |
| `write` | 2 |

# 3 *Report* de Alarmes

Os dados de Eventos (para o caso de recuperação) e Alarmes (para o caso de *report*) são formatados igualmente. Abaixo é mostrada a formatação `JSON` destes para protocolo de comunicação MQTT.

```json
{
  "time": "YYYY-mm-ddTHH:MM:SS.000Z",
  "type": integer,
  "relay-connected": boolean,
  "network-connected": boolean,
  "power-connected": boolean
}
```

<p>
<details>
<summary>No `JSON` mostrado acima:</summary>

| Campo | Tipo | Encoding |
|-:|:-:|:-|
| `time` | `string` | [especificado conforme a tabela](https://www.techonthenet.com/c_language/standard_library_functions/time_h/strftime.php). |
| `type` | `integer` | [coluna `encoding`, especificado conforme a tabela](https://gitlab.com/iose-wifi/iose-fw/-/blob/c29a64d278e25ee8a6fa6149f00c180cb99179a0/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#23-decodifica%C3%A7%C3%A3o-dos-eventos) |
| `relay-connected` | `boolean` | estado no instante de ocorrencia do evento |
| `network-connected` | `boolean` | estado no instante de ocorrencia do evento |
| `power-connected` | `boolean` | estado no instante de ocorrencia do evento |

</details>
</p>

Essa mensagem é enviada no tópico `iose/events` em caso de Evento e em `iose/alarms` em caso de Alarme.

# 4 Recuperação de eventos

A recuperação do *log* de eventos pode ser feita de duas maneiras:

* por tipos de eventos;
* entre dois instantes de tempo;

## 4.1 Busca por tipos de eventos

A busca por tipos de eventos, na versão atual, somente pode ser realizada para tipos que não apresentem *gap* entre si. Exemplos:

* 1 a 3: [1, 3];
* 5 e 6: [5, 6].

A API em sua forma nativa não suporta busca em que os tipos tenham intervalo entre si, exemplo **1 e 3** não são aceitos.

Um vantagem deste tipo de busca é que o dump de todos os eventos atualmente registrados pode ser feito buscando o intervalo `0x00000000` a `0xFFFFFFFF`.

## 4.2 Busca por intervalo de tempo

A busca por eventos pode ser feita entre dois instantes de tempo, dessa forma o dispositivo deve retornar todas as entradas cujo instante do registro foi tomado dentro desse intervalo de tempo.

A vantagem deste tipo de busca é que uma vez conhecido o possível instante de um determinado evento, a busca pelo evento pode ser reduzida buscando os eventos ocorridos no intervalo e selecionando o evento específico, caso registrado.

## 4.3 Formatação de dados

| Requisição (`JSON`) | Resposta (`JSON`) |
|-|-|
|<pre style="background-color: white" lang="json">{<br>  "uuid_req": "string up to 64 characters",<br>  "type": 3,<br>  "operation": 1,<br>  "target": string,<br>  "start": integer,<br>  "end": integer<br>}</pre>|<pre style="background-color: white" lang="json">{<br>  "uuid_req": "string up to 64 characters",<br>  [<br>    [Array de eventos com esta formatação](https://iose-wifi.gitlab.io/documentacao-iose/firmware/11-Eventos_Alarmes_Log.html#_6-1-recuperacao-de-log-de-eventos)<br>  ]<br>}</pre>|

<p>
<details>
<summary>onde:</summary>

| Campo | Tipo | Valor |
|:-:|:-:|:-:|
| `target` | `string` | [`type`](https://iose-wifi.gitlab.io/documentacao-iose/firmware/11-Eventos_Alarmes_Log.html#_6-1-1-busca-por-tipos) ou [`time`](https://iose-wifi.gitlab.io/documentacao-iose/firmware/11-Eventos_Alarmes_Log.html#_6-1-2-busca-por-instantes) |
| `operation` | `integer` | `1`: [conforme a tabela dada em](https://gitlab.com/iose-wifi/iose-fw/-/blob/c29a64d278e25ee8a6fa6149f00c180cb99179a0/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#23-decodifica%C3%A7%C3%A3o-dos-eventos)|
| `start` | `integer` | [timestamp unix](https://www.unixtimestamp.com/) ou [tipo conforme a tabela](https://gitlab.com/iose-wifi/iose-fw/-/blob/c29a64d278e25ee8a6fa6149f00c180cb99179a0/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#23-decodifica%C3%A7%C3%A3o-dos-eventos) |
| `end` | `integer` | [timestamp unix](https://www.unixtimestamp.com/) ou [tipo conforme a tabela](https://gitlab.com/iose-wifi/iose-fw/-/blob/c29a64d278e25ee8a6fa6149f00c180cb99179a0/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#23-decodifica%C3%A7%C3%A3o-dos-eventos) |

</details>
</p>

* os campos `start` e `end`, na busca, são **inclusivos**;
* se `start` igual a `end`, a busca é realizada nesse único valor, possibilitando a **busca por um único tipo ou instante de ocorrencia de evento entre todos os registros**.

<p>
<details>
<summary>Exemplos:</summary>

| Busca | `target`: `start`,`end` |
|:-:|:-:|
| Busca de quaisquer eventos da classe `meter` | `type`: [1024, 2047](https://gitlab.com/iose-wifi/iose-fw/-/blob/c29a64d278e25ee8a6fa6149f00c180cb99179a0/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#23-decodifica%C3%A7%C3%A3o-dos-eventos) |
| Busca de quaisquer eventos da classe `meter` ou `relay` | `type`: [1024, 4097](https://gitlab.com/iose-wifi/iose-fw/-/blob/c29a64d278e25ee8a6fa6149f00c180cb99179a0/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#23-decodifica%C3%A7%C3%A3o-dos-eventos) |
| Busca de quaisquer eventos de qualquer tipo | `type`: 0, 0xFFFFFFFF |
| Busca (somente) por evento de sincronização de tempo com rede de comunicação remota | `type`: [515, 515](https://gitlab.com/iose-wifi/iose-fw/-/blob/c29a64d278e25ee8a6fa6149f00c180cb99179a0/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#23-decodifica%C3%A7%C3%A3o-dos-eventos) |
| Busca de todos os eventos registrados | `time`: 0, 0xFFFFFFFF |
| Busca de todos os eventos registrados entre as 12 e 13h do dia 16/07/2021 | `time`: 1626447600, 1626451200 |

</details>
</p>

# 5 Configuração de alarmes

A configuração para alarme de um dado evento pode ser escrita ou lida pelo envio das mensagens dadas abaixo.

| Operação | Capacidade |
|-:|:-|
| Escrita | configurar `n` (até `256`) eventos de uma classe a partir de um valor específico de evento |
| Leitura | ler todos os tipos de eventos de uma classe (`256`) a partir do índice da classe |

As respostas de leitura ou escrita de configuração de um determinado evento tem a mesma resposta.

## 5.1 Leitura

| Requisição (`JSON`) | Resposta (`JSON`) |
|-|-|
|<pre style="background-color: white" lang="json">{<br>  "uuid_req": "string up to 64 characters",<br>  "operation": 1,<br>  "event-class": integer<br>}</pre>|<pre style="background-color: white" lang="json">{<br>  "uuid_req": "string up to 64 characters",<br>  "status": array(boolean),<br>  "repetition-number": integer,<br>  "repetition-delay": integer,<br>  "max-random-start-delay": integer<br>}
</pre>|

No `JSON` de requisição:

* o campo `operation` é `1`, [conforme dado na tabela](https://iose-wifi.gitlab.io/documentacao-iose/firmware/11-Eventos_Alarmes_Log.html#_2-1-comportamento);
* o campo `event-class` é conforme [documentado](https://gitlab.com/iose-wifi/iose-fw/-/edit/develop/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#23-decodifica%C3%A7%C3%A3o-dos-eventos) na coluna `class-index`.

No `JSON` de resposta:

* o campo `status` é um array de `256` entradas tipo `boolean` indicando o estado da habilitação de cada evento da classe requisitada.

Os campos `repetition-number`, `repetition-delay` e `max-random-start-delay` são documentados [conforme a tabela](https://gitlab.com/iose-wifi/iose-fw/-/blob/develop/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#25-configura%C3%A7%C3%A3o-de-comportamento-de-report).

## 5.2 Escrita

| Requisição (`JSON`) | Resposta (`JSON`) |
|-|-|
|<pre style="background-color: white" lang="json">{<br>  "uuid_req": "string up to 64 characters",<br>  "operation": 2,<br>  "event-start": integer,<br>  "status-mask": array(boolean),<br>  "repetition-number": integer,<br>  "repetition-delay": integer,<br>  "max-random-start-delay": integer<br>}</pre>|<pre style="background-color: white" lang="json">{<br>  "uuid_req": "string up to 64 characters",<br>  "mask-match": boolean,<br>  "repetition-number": integer,<br>  "repetition-delay": integer,<br>  "max-random-start-delay": integer<br>}</pre>|

No `JSON` de requisição:

* o campo `operation` é `2`, [de acordo com a tabela em](https://iose-wifi.gitlab.io/documentacao-iose/firmware/11-Eventos_Alarmes_Log.html#_2-1-comportamento);
* o campo `event-start` é [conforme documentado](https://gitlab.com/iose-wifi/iose-fw/-/edit/develop/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#23-decodifica%C3%A7%C3%A3o-dos-eventos) na coluna `class encode`. A configuração é aplicada a partir desse evento, e;
* o campo `status-mask` consiste de um `array` de `boolean` especificando o estado de cada evento específico que se deseja configurar. Esse array **deve conter no máximo** `256` entradas (número máximo de tipos de eventos para uma dada classe).

No `JSON` de resposta:

* o campo `is_mask_enabled` é do tipo `boolean` e indica se o salvamento da configuração teve sucesso assim como se a máscara de configuração requisitada é igual à máscara salva -- leitura após o salvamento para confirmar igualdade entre o dado salvo e o dado requisitado;

Os campos `repetition-number`, `repetition-delay` e `max-random-start-delay` são documentados [conforme a tabela](https://gitlab.com/iose-wifi/iose-fw/-/blob/develop/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#25-configura%C3%A7%C3%A3o-de-comportamento-de-report).

Caso o sistema não deseje modificar os parâmetros de comportamento** (`repetition-number`, `repetition-delay` e `max-random-start-delay`) **estes devem ser preenchidos com o valor** `0`.

# 6 Casos de uso

As especificações de encodings (valores) de cada evento pode ser acessada [em](https://gitlab.com/iose-wifi/iose-fw/-/blob/c29a64d278e25ee8a6fa6149f00c180cb99179a0/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#23-decodifica%C3%A7%C3%A3o-dos-eventos).

## 6.1 Recuperação de *log* de eventos

Abaixo são dados exemplos de `JSON` contendo múltiplas informações de eventos, como array:

<p>
<details>
<summary>tipo único:</summary>

```json
{
  "uuid_req": "string up to 64 characters",
  [
    {
      "time": "2021-07-01T20:22:12.000Z",
      "type": 2051,
      "relay-connected": "true",
      "network-connected": "false",
      "power-connected": "true"
    },
    {
      "time": "2021-07-05T11:05:07.000Z",
      "type": 2051,
      "relay-connected": "true",
      "network-connected": "true",
      "power-connected": "true"
    },
    ...
    {
      "time": "2021-07-30T15:13:55.000Z",
      "type": 2051,
      "relay-connected": "true",
      "network-connected": "true",
      "power-connected": "true"
    },
  ]
}
```

</details>
</p>

<p>
<details>
<summary>tipos de única classe:</summary>

```json
{
  "uuid_req": "string up to 64 characters",
  [
    {
      "time": "2021-08-01T20:22:12.000Z",
      "type": 256,
      "relay-connected": "true",
      "network-connected": "true",
      "power-connected": "true"
    },
    {
      "time": "2021-08-02T12:00:00.000Z",
      "type": 257,
      "relay-connected": "true",
      "network-connected": "false",
      "power-connected": "true"
    },
    ...
    {
      "time": "2021-08-02T15:10:44.000Z",
      "type": 256,
      "relay-connected": "true",
      "network-connected": "true",
      "power-connected": "true"
    },
  ]
}
```

</details>
<p>

<p>
<details>
<summary>tipos de múltiplas classes:</summary>

```json
{
  "uuid_req": "string up to 64 characters",
  [
    {
      "time": "2021-08-01T20:00:12.000Z",
      "type": 513,
      "relay-connected": "true",
      "network-connected": "false",
      "power-connected": "true"
    },
    {
      "time": "2021-08-02T20:00:23.000Z",
      "type": 515,
      "relay-connected": "true",
      "network-connected": "true",
      "power-connected": "true"
    },
    {
      "time": "2021-08-03T08:10:44.000Z",
      "type": 2048,
      "relay-connected": "false",
      "network-connected": "true",
      "power-connected": "true"
    },
    {
      "time": "2021-08-03T08:10:44.000Z",
      "type": 1036,
      "relay-connected": "true",
      "network-connected": "true",
      "power-connected": "true"
    }
  ]
}
```

</details>
</p>

<p>
<details>
<summary>mesmo instante:</summary>

```json
{
  "uuid_req": "string up to 64 characters",
  [
    {
      "time": "2021-08-01T20:00:12.000Z",
      "type": 2048,
      "relay-connected": "true",
      "network-connected": "false",
      "power-connected": "true"
    },
    {
      "time": "2021-08-01T20:00:12.000Z",
      "type": 259,
      "relay-connected": "true",
      "network-connected": "true",
      "power-connected": "true"
    }
  ]
}
```

</details>
</p>

### 6.1.1 Busca por tipos

<p>
<details>
<summary></summary>

|  Condição / Fluxo | Descrição / Dado trocado |
|:-:|:-|
| Ator Principal | Coisa e MDC |
| Ator Secundário | Coisa |
| Pré-condição | Coisa conectada a WiFi com acesso a Internet, ou; <br> Coisa conectada a rede LoRaWAN |
| Pós-condição |  |
| Fluxo Principal | 1) MDC envia requisição de recuperação de log de eventos de **tipo específico** (power-loss): <br><br>&emsp; json-req("uuid_req", 1, "type", 256, 256); <br><br> 2) Coisa responde todos os eventos do tipo requisitado **presentes no log de eventos** populado como um `JSON` contendo um array de eventos, como exemplificado [anteriormente](https://gitlab.com/iose-wifi/documentacao-iose/-/blob/master/docs/firmware/11-Eventos_Alarmes_Log.md#61-recupera%C3%A7%C3%A3o-de-log-de-eventos). |
| Fluxo Alternativo 1 | 1) MDC envia requisição de recuperação de log de eventos de **classe específica** (`meter`): <br><br>&emsp; json-req("uuid_req", 1, "type", 1024, 2047); <br><br> 2) Coisa responde todos os eventos do tipo requisitado **presentes no log de eventos** populado como um `JSON` contendo um array de eventos, como exemplificado [anteriormente](https://gitlab.com/iose-wifi/documentacao-iose/-/edit/master/docs/firmware/11-Eventos_Alarmes_Log.md#61-recupera%C3%A7%C3%A3o-de-log-de-eventos). |
| Fluxo Alternativo 2 | 1) MDC envia requisição de recuperação de log de eventos de **range de tipos entre classes** (`power`, `comms`, `meter` e `relay`): <br><br>&emsp; json-req("uuid_req", 1, "type", 0, 4097); <br><br> 2) Coisa responde todos os eventos do tipo requisitado **presentes no log de eventos** populado como um `JSON` contendo um array de eventos, como exemplificado [anteriormente](https://gitlab.com/iose-wifi/documentacao-iose/-/edit/master/docs/firmware/11-Eventos_Alarmes_Log.md#61-recupera%C3%A7%C3%A3o-de-log-de-eventos). |
| Fluxo de Excessão 1 | 1) MDC envia qualquer das requisições dos fluxos anteriores; <br> 2) Coisa responde como um `JSON` falha de request (TBD!). |
| Fluxo de Excessão 2 | 1) MDC envia qualquer das requisições dos fluxos anteriores; <br> 2) Coisa responde como um `JSON` contendo um **array de vazio, caso não haja eventos a serem recuperados para os tipos buscados**. |

</details>
</p>

### 6.1.2 Busca por instantes

<p>
<details>
<summary></summary>

|  Condição / Fluxo | Descrição / Dado trocado |
|:-:|:-|
| Ator Principal | Coisa e MDC |
| Ator Secundário | Coisa |
| Pré-condição | Coisa conectada a WiFi com acesso a Internet, ou; <br> Coisa conectada a rede LoRaWAN |
| Pós-condição |  |
| Fluxo Principal | 1) MDC envia requisição de recuperação de log de eventos de **um instante específico**: <br><br>&emsp; json-req("uuid_req", 1, "time", 1627924966, 1627924966); <br><br> 2) Coisa responde todos os eventos ocorridos **presentes no log de eventos** para aquele instante específico, como exemplificado [anteriormente](https://gitlab.com/iose-wifi/documentacao-iose/-/edit/master/docs/firmware/11-Eventos_Alarmes_Log.md#61-recupera%C3%A7%C3%A3o-de-log-de-eventos). |
| Fluxo Alternativo 1 | 1) MDC envia requisição de recuperação de log de eventos **entre dois instantes específicos** (um dia): <br><br>&emsp; json-req("uuid_req", 1, "time", 1627838566, 1627924966); <br><br> 2) Coisa responde todos os ocorridos **no intervalo de tempo dado** populado como um `JSON` contendo um array de eventos, como exemplificado [anteriormente](https://gitlab.com/iose-wifi/documentacao-iose/-/edit/master/docs/firmware/11-Eventos_Alarmes_Log.md#61-recupera%C3%A7%C3%A3o-de-log-de-eventos). |
| Fluxo Alternativo 2 | 1) MDC envia requisição de recuperação de **todo o log de eventos** : <br><br>&emsp; json-req("uuid_req", 1, "time", 0, 0xFFFFFFFF); <br><br> 2) Coisa responde **todos os eventos presentes no log de eventos** populado como um `JSON` contendo um array de eventos, como exemplificado [anteriormente](https://gitlab.com/iose-wifi/documentacao-iose/-/edit/master/docs/firmware/11-Eventos_Alarmes_Log.md#61-recupera%C3%A7%C3%A3o-de-log-de-eventos). |
| Fluxo de Excessão 1 | 1) MDC envia qualquer das requisições dos fluxos anteriores; <br> 2) Coisa responde como um `JSON` falha de request (TBD!). |
| Fluxo de Excessão 2 | 1) MDC envia qualquer das requisições dos fluxos anteriores; <br> 2) Coisa responde como um `JSON` contendo um **array de vazio, caso não haja eventos a serem recuperados para os tipos buscados**. |

</details>
</p>

## 6.2 *Report* de alarme

Em caso de **alarmes de mais de um evento, cada alarme é reportado individualmente**.

<p>
<details>
<summary></summary>

|  Condição / Fluxo | Descrição / Dado trocado |
|:-:|:-|
| Ator Principal | Coisa |
| Ator Secundário | MDC |
| Pré-condição | Coisa conectada a WiFi com acesso a Internet, ou; <br> Coisa conectada a rede LoRaWAN |
| Pós-condição |  |
| Fluxo Principal | 1) Coisa identifica evento a ser alarmado para o sistema<br>2) Coisa envia frame contendo alarme contendo dados do evento a ser reportado [como especificado em](https://gitlab.com/iose-wifi/iose-fw/-/blob/c29a64d278e25ee8a6fa6149f00c180cb99179a0/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#24-configura%C3%A7%C3%A3o-habilita%C3%A7%C3%A3o-de-report);<br>3) Coisa **recebe** frame de `acknowledge` de recebimento de alarme, [conforme especificado](https://iose-wifi.gitlab.io/documentacao-iose/firmware/11-Eventos_Alarmes_Log.html#_2-1-comportamento), antes de o dispositivo enviar a quantidade limite de *reports* configurada para os alarmes;<br>4) Coisa **registra evento de sucesso de** `acknowledge` **de alarme**. |
| Fluxo de Excessão 1 | 1) Coisa identifica evento a ser alarmado para o sistema<br>2) Coisa envia frame contendo alarme contendo dados do evento a ser reportado [como especificado em](https://gitlab.com/iose-wifi/iose-fw/-/blob/c29a64d278e25ee8a6fa6149f00c180cb99179a0/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#24-configura%C3%A7%C3%A3o-habilita%C3%A7%C3%A3o-de-report);<br>3) Coisa **não recebe** frame de `acknowledge` de recebimento de alarme;<br>4) Coisa **registra evento de falha de** `acknowledge` **de alarme**. |

</details>
</p>

## 6.3 Configuração de *report* de alarme

### 6.3.1 Leitura de habilitação ou configuração de comportamento

A leitura de habilitação de eventos e de configuração de comportamento pode ser feita usando o mesmo `JSON` (uma vez que as configurações de comportamento são retornadas quando é requisitada a leitura de configuração de qualquer classe de eventos), onde:

* para **leitura de habilitação** o **índice** da classe é **diferente de zero**;
* para **leitura de configuração de comportamento** o **índice** é **igual a zero**.

<p>
<details>
<summary></summary>

|  Condição / Fluxo | Descrição / Dado trocado |
|:-:|:-|
| Ator Principal | MDC |
| Ator Secundário | Coisa |
| Pré-condição | Coisa conectada a WiFi com acesso a Internet, ou; <br> Coisa conectada a rede LoRaWAN |
| Pós-condição |  |
| Fluxo Principal | 1) MDC envia comando de leitura configuração da classe de evento (ex. `4105` ou `0`) [conforme especificado](https://gitlab.com/iose-wifi/iose-fw/-/blob/c29a64d278e25ee8a6fa6149f00c180cb99179a0/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#23-decodifica%C3%A7%C3%A3o-dos-eventos): <br><br>&emsp; json-req("uuid_req", 1, 7)<br><br>2) Coisa recebe requisição e **responde** com **configuração da classe requisitada**, assim como **configuração global de comportamento de report de eventos**, [conforme especificado](https://gitlab.com/iose-wifi/documentacao-iose/-/edit/master/docs/firmware/11-Eventos_Alarmes_Log.md#53-respota-de-leitura-ou-escrita):<br><br>&emsp; json-req("uuid_req", [status], x, y, z)<br><br>Onde `status` é um sequencia com `256` entradas de tipo `boolean`; `x`, `y` e `z` são as configurações de comportamento de *report* .|
| Fluxo de Excessão 1 | 1) MDC envia comando de leitura de configuração de evento [diferente do especificado](https://gitlab.com/iose-wifi/iose-fw/-/blob/c29a64d278e25ee8a6fa6149f00c180cb99179a0/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#23-decodifica%C3%A7%C3%A3o-dos-eventos)<br>2) Coisa recebe requisição e **responde indicativo de falha na requisição recebida** (TBD!). |

</details>
</p>

### 6.3.2 Escrita de habilitação ou configuração de comportamento

A escrita de habilitação de eventos e de configuração de comportamento pode ser feita usando o mesmo `JSON` (uma vez que as configurações de comportamento são retornadas quando é requisitada a leitura de configuração de qualquer classe de eventos), onde:

* para efetivamente **configurar o comportamento** de `report` de eventos os **valores devem ser diferentes de zero**;
* para **configurar somente comportamento** dos `reports` **sem precisar configurar eventos**, a **classe** deve ser **nula**;
* para **configurar somente habilitação de *report* eventos**, os **parâmetros de comportamento** devem ser **nulos**.

<p>
<details>
<summary></summary>

|  Condição / Fluxo | Descrição / Dado trocado |
|:-:|:-|
| Ator Principal | MDC |
| Ator Secundário | Coisa |
| Pré-condição | Coisa conectada a WiFi com acesso a Internet, ou; <br> Coisa conectada a rede LoRaWAN |
| Pós-condição |  |
| Fluxo principal | 1) MDC envia comando de escrita de configuração [conforme especificado](https://gitlab.com/iose-wifi/documentacao-iose/-/edit/master/docs/firmware/11-Eventos_Alarmes_Log.md#52-escrita) para **somente promover um evento a alarme**: <br><br>&emsp; json-req("uuid_req", class, 1036, "true", x, y, z)<br><br>2) Coisa recebe requisição, configura evento e responde estado do evento configurado, assim como configuração de comportamento de *report* (global) -- [conforme especificado](https://gitlab.com/iose-wifi/documentacao-iose/-/edit/master/docs/firmware/11-Eventos_Alarmes_Log.md#53-respota-de-leitura-ou-escrita). |
| Fluxo Alternativo 1 | 1) MDC envia comando de escrita de configuração [conforme especificado](https://gitlab.com/iose-wifi/documentacao-iose/-/edit/master/docs/firmware/11-Eventos_Alarmes_Log.md#52-escrita) para **configurar um  alarme e**, também, **o comportamento de *report***: <br><br>&emsp; json-req("uuid_req", 2, 1036, "true", 12, 5, 10000)<br><br>2) Coisa recebe requisição, configura evento e responde estado do evento configurado, assim como configuração de comportamento de *report* (global) -- [conforme especificado](https://gitlab.com/iose-wifi/documentacao-iose/-/edit/master/docs/firmware/11-Eventos_Alarmes_Log.md#53-respota-de-leitura-ou-escrita). |
| Fluxo de Excessão 1 | 1) MDC envia comando de escrita de configuração de evento [diferente do especificado](https://gitlab.com/iose-wifi/iose-fw/-/blob/c29a64d278e25ee8a6fa6149f00c180cb99179a0/fw/src/iose_wifi/src/main/application/storage/app_layer/events_log/event_log.md#23-decodifica%C3%A7%C3%A3o-dos-eventos)<br>2) Coisa recebe requisição e **responde indicativo de falha na requisição recebida** (TBD!). |

</details>
</p>
