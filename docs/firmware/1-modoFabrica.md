﻿# Requisitos do modo Fábrica.
O modo fábrica consiste nas etapas de teste e configurações que serão realizadas no Módulo afim de garantir todas suas funcionalidades e calibrações necessárias para operação. Para isso,os seguintes requisitos foram levantados e analisados,dos quais precisam ser alcançados para o módulo ser considerado testado e calibrado:

## Validação da gravação de Firmware.
Deverá ser realizada a validação da gravação de firmware no módulo. Caso a gravação falhe, o módulo deverá ser encaminhado para o retrabalho.

## Validação dos LEDs.
Os LEDs presentes no módulo deverão ser validados de forma visual por um operador,que irá checar se os LEDs irão acender durante a inicialização do módulo. Caso algum dos LEDs não acenda,o módulo deverá ser encaminhado para o retrabalho.

## Validação dos botões.
Os botões presentes no módulo deverão ser validados de forma manual por um operador,que irá apertar botão por botão,e verificar se os LEDs irão acender ao pressionar cada um. Caso ao pressionar algum botão os LEDs não acendam,o módulo deverá ser encaminhado para o retrabalho. 

## Validação do relé.
Deverá ser realizada a validação do funcionamento do Relé do módulo pela detecção de fase. Caso o teste de validação falhe,o módulo deverá ser encaminhado para o retrabalho. 

## Validação da conexão WiFi
Para validar a conexão WiFi,o módulo irá se conectar á uma rede WiFi disponível,cujo os parâmetros de conexão serão enviados pelo Software de Fábrica. Após o módulo ter conseguido se conectar à uma rede WiFi, uma troca de pacotes será realizada entre o módulo e uma aplicação para verificar a capacidade de comunicação do módulo. Caso a troca de pacotes falhe,o módulo deverá ser encaminhado para o retrabalho.


## Validação da comunicação e calibração do MCP.
O microcontrolador deverá realizar uma troca de mensagens com o módulo MCP para validar a capacidade de comunicação entre os dois,para isso será enviada uma mensagem para o MCP,que será respondida com a versão do próprio MCP. Após isso,algumas calibrações serão feitas no MCP,como calibração sem carga,calibração de fator de potência 0,5 e 1 e reset de energias acumuladas. Caso a troca de mensagens ou as calibrações falhem, o módulo deverá ser encaminhado para o retrabalho.

## Validação da comunicação com o AWS IoT Mqtt.
Deverá ser realizada a validação do cadastro do módulo no sistema da AWS IoT,afim de garantir que os certificados e chaves foram gravados corretamente dentro do módulo. Caso a validação falhe, o módulo deverá ser encaminhado para o retrabalho.

