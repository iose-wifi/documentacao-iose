﻿# Alarmes do Medidor
# Introdução
Este documento especifica a funcionalidade de _Alarmes do Medidor_. Nesta especificação serão definidos:

* tópico de requisição e resposta da captura dos limites dos alarmes;
* tópico de requisição e resposta da alteração dos limites dos alarmes;
* formatação de dados trocados entre software e firmware;
* comportamento esperado do dispositivo para realização da operação;
* como verificar os alarmes ocorridos no log de eventos.

Esta especificação deve, ao seu fim, ser de tal forma que o dispositivo esteja apto a alterar e capturar os limites dos alarmes do medidor e verificar os alarmes ocorridos.

# Limites de Alarmes
Os limites de alarmes definem o ponto em que o medidor irá sinalizar que a tensão, corrente, potência, temperatura estão em níveis críticos.
A alteração ou captura dos limites de alarmes do medidor pode ser feito remotamente via requisição MQTT.

## Comunicação MQTT
Nesta secção são determinados os tópicos de onde são feitos as requisições e para onde serão enviadas as medidas solicitadas e seus respectivos pacotes JSON.

### Requisição
#### Tipo
| Operação | Tipo |
|----------|------|
| Alterar ou capturar limites dos alarmes do medidor | 19 |

#### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |
| Produção | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |

#### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_req | string | Identificador único da requisição (máx. 63 caracteres) |
| type | integer | Indica o tipo de requisição |
| operation | integer | Operação de alteração ou captura |
| volt_lower | integer | Limite inferior de tensão (0.1V) |
| volt_upper | integer | Limite superior de tensão (0.1V) |
| curr_upper | integer | Limite superior de corrente (0.001A) |
| powr_upper | integer | Limite superior de potência (0.01W) |
| temp_upper | integer | Limite superior de temperatura |

#### Exemplo - Captura de limites
```json
{
    "uuid_req": "string up to 64 characters",
    "type": 19,
    "operation": 0
}
```

#### Exemplo - Alteração de limites
```json
{
    "uuid_req": "string up to 64 characters",
    "type": 19,
    "operation": 1,
    "volt_lower": 800,
    "volt_upper": 2400,
    "curr_upper": 200000,
    "powr_upper": 440000,
    "temp_upper": 383
}
```

### Resposta
#### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/alarms |
| Produção | prod/iose/general/alarms |

#### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único da módulo |
| uuid_req | string | Identificador único da requisição recebida (máx. 63 caracteres) |
| success | boolean | Estado de execução |
| limits | array | Lista de valores atuais dos limites |
| volt_lower | integer | Limite inferior de tensão (0.1V) |
| volt_upper | integer | Limite superior de tensão (0.1V) |
| curr_upper | integer | Limite superior de corrente (0.001A) |
| powr_upper | integer | Limite superior de potência (0.01W) |
| temp_upper | integer | Limite superior de temperatura |

#### Exemplo
```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_req": "string up to 64 characters",
    "success": true,
    "limits": [
        {
            "volt_lower": 800,
            "volt_upper": 2400,
            "curr_upper": 200000,
            "powr_upper": 440000,
            "temp_upper": 383
        }
    ]
}
```

### Casos de Uso
|  Condição / Fluxo | Descrição / Dado trocado |
|-|-|
| Ator Principal | Módulo e MQTT (Cloud) |
| Ator Secundário | Rotina de alteração e captura de limites |
| Pré-condição | Usuário necessita alterar ou capturar os limites dos alarmes |
| Pós-condição | Módulo envia os limites atuais e o estado da operação |
| Fluxo Principal | <br>1) Módulo recebe uma requisição para **alterar ou capturar os limites**: <br><br>&emsp;Executa rotina de alteração ou captura de limites; <br><br>2) Módulo envia mensagem de sucesso na operação e os valores atuais de limites:<br><br>&emsp;json-msg(AABBCCDDEEFF, 123456789, true, 800, 2400, 200000, 440000, 383).<br><br>
| Fluxo Alternativo | <br>1) Módulo recebe uma requisição para **alterar ou capturar os limites**: <br><br>&emsp;Falha ao executar rotina de alteração ou captura de limites; <br><br>2) Módulo envia mensagem de falha na operação e os valores atuais de limite:<br><br>&emsp;json-msg(AABBCCDDEEFF, 123456789, false, 800, 2400, 200000, 440000, 383).<br><br> |

# Verificação de Alarmes
É possível verificar os alarmes através do recurso de recuperação de log de eventos, buscando o intervalo de eventos correspondentes as ocorrências de alarmes.

## Eventos dos Alarmes
| Alarme | Evento |
|----------|--------|
| Queda de tensão | 1036 |
| Surto de tensão | 1037 |
| Surto de corrente | 1038 |
| Surto de potência | 1039 |
| Superaquecimento | 1040 |

### Requisição de Recuperação de eventos de alarmes
Para recuperar os eventos de alarmes, é necessário fazer uma requisição para buscar por tipo e em um intervalo entre 1036 e 1040.

#### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |
| Produção | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |

#### Exemplo
```json
{
    "uuid_req": "string up to 64 characters",
    "type": 3,
    "operation": 1,
    "target": "type",
    "start": 1036,
    "end": 1040
}
```

### Resposta de Recuperação de eventos de alarmes
O módulo irá retornar todos os eventos de alarmes registrados, divididos em mensagens contendo o array com os eventos.

#### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/logs |
| Produção | prod/iose/general/logs |

#### Exemplo
```json
{
  "uuid_req": "string up to 64 characters",
  "uuid_circuit": "240AC4159924",
  "events": [
    {
      "time": "2022-04-07T20:06:20.000Z",
      "type": 1036,
      "relay": false,
      "network": true,
      "power": true,
      "repetition": 0
    },
    {
      "time": "2022-04-07T20:06:29.000Z",
      "type": 1037,
      "relay": true,
      "network": false,
      "power": true,
      "repetition": 0
    },
    {
      "time": "2022-04-07T20:08:11.000Z",
      "type": 1038,
      "relay": true,
      "network": true,
      "power": true,
      "repetition": 0
    },
    {
      "time": "2022-04-07T20:08:11.000Z",
      "type": 1039,
      "relay": true,
      "network": true,
      "power": true,
      "repetition": 0
    },
    {
      "time": "2022-04-07T20:08:11.000Z",
      "type": 1040,
      "relay": true,
      "network": true,
      "power": true,
      "repetition": 0
    }
  ]
}
```