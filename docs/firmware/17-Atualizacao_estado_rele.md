﻿# Atualização de Estado do Relé
# Introdução
Este documento especifica a funcionalidade de _Atualização de Estado do Relé_. Nesta especificação serão definidos:

* tópico de envio das informações de estado do relé;
* formatação de dados trocados entre software e firmware: requisição e respostas;
* comportamento esperado do dispositivo para realização da operação.

Esta especificação deve, ao seu fim, ser de tal forma que o dispositivo esteja apto a enviar a cada 30 segundos, ou por requisição, ou ao ocorrer um acionamento, o estado do relé para o tópico específico.

# Comunicação MQTT
Nesta secção são determinados os tópicos de onde são feitos as requisições e para onde serão enviadas as medidas solicitadas e seus respectivos pacotes JSON.

## Requisição
### Tipo
| Operação | Tipo |
|----------|------|
| Verificar estado do relé | 14 |

### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |
| Produção | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_req | string | Identificador único da requisição (máx. 63 caracteres) |
| type | integer | Indica o tipo de requisição |

#### Exemplo
```json
{
    "uuid_req": "123456789",
    "type": 14
}
```

## Resposta
### Origens
| Origem | Valor | Descrição |
|--------|------|-----------|
| Manual (botão) | 0 | Acionamento ao pressionar botão |
| Remoto | 1 | Acionamento remoto |
| Agendamento | 2 | Acionamento agendado |
| Requisição | 3 | Requisição remota |
| Periódico | 4 | A cada 30 segundos |

### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/relay_action_info |
| Produção | prod/iose/general/relay_action_info  |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único do módulo |
| origin | integer | Identificador da origem do acionamento |
| relay_state | boolean | Estado atual do relé |
| timestamp | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo
```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "origin": 0,
    "relay_state": false,
    "timestamp": "2021-11-07T20:11:05.000Z"
}
```

# Comportamento Esperado
## Falha
Abaixo seguem os casos de falha e o comportamento.

| Falha | Comportamento |
|-------|---------------|
| Pacote JSON inválido | Nenhum pacote de estado do relé será enviado |
| Falha no acionamento | Nenhum pacote de estado do relé será enviado |

## Sucesso
Abaixo estão descritos os comportamentos que o módulo poderá fazer pra cada operação.

| Operação | Comportamento |
|-------|---------------|
| Manual | Ao acionar pelo botão o módulo irá enviar o estado do relé com origem 0 |
| Remoto | Ao acionar remotamente o módulo irá enviar o estado do relé com origem 1 |
| Agendamento | Ao acionar por agendamento o módulo irá enviar o estado do relé com origem 2 |
| Período/Requisição | A cada 30 segundos, ou por requisição o módulo irá enviar o estado do relé com origem 3 |