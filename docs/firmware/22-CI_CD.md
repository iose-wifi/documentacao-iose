# Documentação CI/CD FW
## Explicação do CI/CD do Gitlab:
A etapa de Continuous Integration (CI) entra para executar verificações automaticamente. É feito build e teste automático e continuo. Esses testes garantem que as alterações sejam aprovadas em todos os testes, diretrizes e padrões de conformidade de código que você estabeleceu para sua aplicação.

Por fim, após garantir que o código está seguindo os padrões de qualidade, a etapa de Continuous Deployment (CD) cuida da implantação automatizada do código: empacotar o código e colocar em execução em um servidor.

GitLab CI/CD surge como ferramenta integrada ao GitLab onde é possível descrever todos os passos de integração e implantação contínua em um arquivo dentro do repositório. Este arquivo é o .gitlab-ci.yml , esse arquivo segue o formato do YAML que nada mais é do que uma linguagem de marcação. Este arquivo define a ordem em que se dará a execução do pipeline. 

Uma pipeline é um grupo de jobs (tarefas) que são executados em estágios. Os estágios são grupos de tarefas (jobs) e seus comandos são executados sequencialmente e se todos eles tiverem sucesso, a pipeline move para o próximo estágio. Se um dos jobs falharem, o próximo estágio não será executado (geralmente). Você pode acessar a página de pipelines na aba Pipelines do seu projeto.

O GitLab roda nossos pipelines em Runners. Ao executar o pipeline, esses Runners recebem automaticamente o conteúdo do seu repositório e executam tarefas que você designou conforme seu arquivo YML. Por padrão os Runners utilizados são Runners do GitLab

## Explicação do processo de CI/CD no projeto do Iose Firmware:
A primeira coisa a ser feita foi definir os estágios que a pipeline irá conter..
O arquivo `.gitlab-ci.yml` do firmware foi criado com três estágios (build, test, deploy)

```
stages:
    - build
    - test
    - deploy
```    

No estágio de build a aplicação é compilada. No estágio de test o código é testado. No estágio de deploy empacota o código e lança para um servidor.  

### Estágio de Build:

```
build:
  image: sffrotaa/esp32-python:latest
  stage: build
  script:
       - cd fw/src/third_party/esp-idf
       - git submodule update --init --recursive
       - git status
       - export PATH="$IDF_PATH/tools:$PATH"
       - ./install.sh
       - . export.sh
       - cd /builds/iose-wifi/iose-fw/fw/src/iose_wifi/src
       - idf.py build
```       

*Explicando a estrutura de linha por linha:*  

"build:" - Nomeia o job (tarefa).

"image: sffrotaa/esp32-python:latest" - Específica a imagem docker a ser usada para executar o job (tarefa), a imagem foi criada com python, git, esp idf e ninja build.

"stage: build" - Específica o estágio ao qual o job (tarefa) pertence. 

"script:" - Determina o comando ou script que será executado pelo runner.

"- cd fw/src/third_party/esp-idf" - Comando que entra no diretório onde contem a versão utilizada do esp idf.

"- git submodule update --init --recursive" - Comando que atualiza e e inicia os submódulos necessários.

"- git status" - Comando que mostra para onde HEAD está apontando, seja um branch ou um commit (é para onde você fez o check-out).

"- export PATH="$IDF_PATH/tools:$PATH"" - Variável PATH do sistema para incluir o diretório que contém a ferramenta idf.py (parte do ESP-IDF).

"- ./install.sh" - Instalação das ferramentas usadas pelo ESP-IDF, como compilador, depurador, pacotes Python, etc.

"- . export.sh" - Um script que o ESP-IDF fornece para tornar as ferramentas utilizáveis na linha de comando.

"- cd /builds/iose-wifi/iose-fw/fw/src/iose_wifi/src" - Entra no diretório onde está o projeto a ser compilado

"- idf.py build" - Comando que compila o aplicativo e todos os componentes ESP-IDF e, em seguida, gerará o carregador de inicialização, a tabela de partição e os binários do aplicativo.

{Resumo Build: A imagem docker foi criada com o Git, Ninja Build, Esp idf porque tem as ferramentas necessárias para implementar o esp idf na pipeline, depois de instalado já pode ser feito o build no projeto.}    

### Estágio de Teste:

```
test:
  image: gcc:latest
  stage: test
  script: 
       - apt update && apt -y upgrade
       - apt -y install ninja-build
       - apt-get install python-is-python3
       - apt -y install git
       - cd fw/src/third_party/esp-idf
       - git submodule update --init --recursive
       - git status
       - apt install libcjson1 libcjson-dev
       - cd /builds/iose-wifi/iose-fw/tests/eclipse-project/gcc_make
       - chmod -R 777 *
       - mkdir -p Debug/test-generated-files
       - make all    
       - ./test_release.exe
```

*Explicando a estrutura de linha por linha:*   

"test:" - Nomeia o job (tarefa).

"image: gcc:latest" - Específica a imagem docker a ser usada para executar o job (tarefa), a imagem é oficial do gcc para rodar testes unitários em C e makefile.

"stage: test" - Específica o estágio ao qual o job (tarefa) pertence.

"script:" - Determina o comando ou script que será executado pelo runner.

"- apt update && apt -y upgrade" - Comando que atualiza e instala novas versões dos pacotes.

"- apt -y install ninja-build" - Comando que instala o Ninja Build.

"- apt-get install python-is-python3" - Comando que instala o Python.

"- apt -y install git" - Comando que instala o Git.

"- cd fw/src/third_party/esp-idf" - Comando que entra no diretório onde contem a versão utilizada do esp idf.

"- git submodule update --init --recursive" - Comando que atualiza e e inicia os submódulos necessários.

"- git status" - Comando que mostra para onde HEAD está apontando, seja um branch ou um commit (é para onde você fez o check-out).

"- apt install libcjson1 libcjson-dev" - Comando que instala pacotes Cjson necessários para compilação dos testes unitários.

"- cd /builds/iose-wifi/iose-fw/tests/eclipse-project/gcc_make" - Comando que entra no diretório que contem os arquivos relacionados aos testes unitários. 

"- chmod -R 777 *" - Comando que garante permissão aos arquivos.

"- mkdir -p Debug/test-generated-files" - Comando que cria as pastas "Debug" e "test-generated-files" no diretório que contem os testes unitários.

"- make all" - Comando que realiza a execução do makefile e seus comados que estão redigidos no arquivo. 

"- ./test_release.exe" - Comando que realiza a execução do programa de testes unitários executável compilado pelo makefile.

{Resumo Teste: A imagem usada é a oficial do gcc para lidar com os testes unitários em C e makefile, além disso os testes unitários necessitam de arquivos do esp idf e Cjson, por isso é instalado por comando o python, ninja build e git para fazer a instalação do esp idf e disponibilizar os arquivos necessários,  também é instalado por comando pacotes do Cjson, com tudo já instalado pode acessar e executar o makefile que gera o programa executável de testes e por fim utilizar o comando para executar o programa de testes.}

### Estágio de Deploy:

```
deploy:
  image: sffrotaa/esp32-python:latest
  stage: deploy
  script:
       - cd fw/src/third_party/esp-idf
       - git submodule update --init --recursive
       - git status
       - ./install.sh
       - . export.sh
       - cd /builds/iose-wifi/iose-fw/fw/src/iose_wifi/src
       - idf.py build
       - pip install awscli
       - aws configure set region us-west-2
       - aws s3 cp iose_wifi.bin s3://$S3_BUCKET/iose_wifi.bin
```

*Explicando a estrutura de linha por linha:*

"deploy:" - Nomeia o job (tarefa).

"image: sffrotaa/esp32-python:latest" - Específica a imagem docker a ser usada para executar o job (tarefa), a imagem foi criada com python, git, esp idf e ninja build.

"stage: deploy" - Específica o estágio ao qual o job (tarefa) pertence.

"script:" - Determina o comando ou script que será executado pelo runner.

"- cd fw/src/third_party/esp-idf" - Comando que entra no diretório onde contem a versão utilizada do esp idf.

"- git submodule update --init --recursive" - Comando que atualiza e e inicia os submódulos necessários.

"- git status" - Comando que mostra para onde HEAD está apontando, seja um branch ou um commit (é para onde você fez o check-out).

"- ./install.sh" - Instalação das ferramentas usadas pelo ESP-IDF, como compilador, depurador, pacotes Python, etc.

"- . export.sh" - Um script que o ESP-IDF fornece para tornar as ferramentas utilizáveis na linha de comando.

"- cd /builds/iose-wifi/iose-fw/fw/src/iose_wifi/src" - Entra no diretório onde está o projeto a ser compilado

"- idf.py build" - Comando que compila o aplicativo e todos os componentes ESP-IDF e, em seguida, gerará o carregador de inicialização, a tabela de partição e os binários do aplicativo.

"- pip install awscli" - Comando que instala o aws cli que permite gerenciar e automatizar serviços AWS usando comandos.

"- aws configure set region us-west-2" Comando que pega as variaveis configuradas no repositorio do gitlab (S3_BUCKET; AWS_ACCESS_KEY_ID; AWS_SECRET_ACCESS_KEY) e configura a região na qual o bucket referenciado foi criado.

"- aws s3 cp iose_wifi.bin s3://$S3_BUCKET/iose_wifi.bin" - Comando que copia o arquivo binário que foi compilado (iose_wifi.bin) e faz o upload do mesmo no bucket da amzon que foi configurado no gitlab.

{Resumo Deploy: Foi preciso criar um bucket na AWS, depois gerar chaves de acesso na AWS para usuário, depois de ter feito esses passos é preciso entrar na configuração do repositorio do gitlab na secção de CI/CD e acessar o campo de variaveis, então adicionar as seguintes informações(S3_BUCKET-nome do bucket criado; AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY-chaves de acesso de usuário geradas). A imagem sffrotaa/esp32-python:latest foi usada porque para fazer o deploy é necessário compilar o projeto para gerar o arquivo binário que sera feito o deploy, então depois de todos os comandos para compilar o projeto, é instalado o AWS CLI que permite gerenciar a AWS através de comandos, depois é o comando de configuração que referencia a pipeline do Gitlab às informações da AWS, e por fim é feito o deploy.}

## Execução e verificação da pipeline:
Para uma pipeline ser executada o arquivo .gitlab-ci.yml tem que estar na raiz do projeto e ter pelo menos um estágio configurado (EX: build), então digamos que foi adicionado .gitlab-ci.yml na branch master assim que fizer uma alteração no projeto e subir o commit a pipeline vai ser executada automaticamente.

Para verificar o resultado da pipeline é preciso ir na seção CI/CD do gitlab e lá irá aparecer a lista de pipelines, pode ser feito a verificação do job enquanto está em execução, ao lado do codigo do commit estará um ícone em azul, clicando nele irá aparecer o nome do job em execução, ao clicar no nome do job será acessado o log da pipeline, assim como quando a pipeline terminar o ícone azul fica vermelho se ocorrer falha, ou verde se for sucesso, podendo acessar o log da mesma forma.