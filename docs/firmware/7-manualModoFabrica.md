﻿# Manual do Modo Fábrica
Esse documento apresenta o manual do Software de Fábrica assim como operá-lo.
[Manual do Software de Fábrica](./imagens/factory_mode.pdf)
### Processo de gravação de firmware release

Este tópico orienta como performar a gravação do firmware de release no módulo IOSE

#### Comandos necessários para **PRIMEIRA** gravação do firmware release no módulo etravamento de gravação e debug via UART/JTAG (**SEGURO)**:
   1. Geração de binário assinado da Aplicação:
      > espsecure.py generate\_signing\_key main/helpers/files/keys/secure\_boot\_signing\_key.pem
   2. Geração da chave de encriptação da flash:
      > espsecure.py generate\_flash\_encryption\_key main/helpers/files/keys/flash\_encrypt\_key.bin
   3. Compilação do firmware release:
      > idf.py build
   4. Geração de imagem encriptada da partição &quot;nvs&quot; (contém os certificados para a aplicação) e sua respectiva chave:
      > python /$IDF\_PATH/components/nvs\_flash/nvs\_partition\_generator/nvs\_partition\_gen.py encrypt main/helpers/files/csv/nvs.csv bin/nvs\_encr.bin 0x14000 --keygen --keyfile nvs\_keys.bin --outdir main/helpers/files/
   5. Gravação(queima) das chaves de encriptação e boot seguro:
      1. Queimar chave de encriptação:
         1. > espefuse.py --port PORT burn\_key flash\_encryption main/helpers/files/keys/flash\_encrypt\_key.bin
         2. Digite a palavra &quot;BURN&quot; para finalizar o processo.
      2. Queimar chave de boot seguro:
         1. > espefuse.py --port PORT burn\_key secure\_boot\_v1 build/bootloader/secure-bootloader-key-256.bin
         2. Digite a palavra &quot;BURN&quot; para finalizar o processo.
   6. Gravação dos binários:
      1. Apague todo o conteúdo da memória flash:
            > idf.py erase\_flash
      2. Gravar partition-table, otadata e iose\_wifi:
            > esptool.py -p (PORT) -b 460800 --before default\_reset --after hard\_reset --chip esp32 write\_flash --flash\_mode dio --flash\_size detect --flash\_freq 40m 0x10000 build/partition\_table/partition-table.bin 0x25000 build/ota\_data\_initial.bin 0xb0000 build/iose\_wifi.bin
      3. Gravar o bootloader:
            > esptool.py -p (PORT) -b 460800 --before default\_reset --after hard\_reset --chip esp32 write\_flash --flash\_mode dio --flash\_size detect --flash\_freq 40m 0x1000 build/bootloader/bootloader.bin
      4. Gravar imagem da &quot;nvs&quot;:
         1. Comando para gravar imagem nvs-key:
            > esptool.py -p (PORT) -b 460800 --before default\_reset --after hard\_reset --chip esp32 write\_flash --flash\_mode dio --flash\_size detect --flash\_freq 40m --encrypt 0x3b0000 main/helpers/files/keys/nvs\_keys.bin
         2. Comando para gravar imagem &quot;nvs&quot;:
            > esptool.py -p (PORT) -b 460800 --before default\_reset --after hard\_reset --chip esp32 write\_flash --flash\_mode dio --flash\_size detect --flash\_freq 40m 0x11000 main/helpers/files/bin/nvs\_encr.bin
   7.  Teste de firmware
       1. Enviar &quot;600\n&quot; para o módulo via UART:
          1. Sem resposta – Falha na gravação do firmware;
          2. Resposta &quot;602\n&quot; - Falta da imagem &quot;nvs&quot;;
          3.  Resposta &quot;601\n&quot; - Firmware OK.
   8.  Procedimento para bloquear a gravação e debug via UART/JTAG **(APENAS SE A GRAVAÇÃO DOS BINÁRIOS OBTIVER ÊXITO)**:
       1. Para setar os efuses use este comando:
          1. > espefuse.py --port (PORT) burn\_efuse DISABLE\_DL\_ENCRYPT 1 DISABLE\_DL\_DECRYPT 1 DISABLE\_DL\_CACHE 1 JTAG\_DISABLE 1 CONSOLE\_DEBUG\_DISABLE 1 
          2.  Digite a palavra &quot;BURN&quot; para finalizar o processo.
       2.  Para remover a função escrita dos efuses use este comando:
           1. > espefuse.py --port (PORT) write\_protect\_efuse DISABLE\_DL\_ENCRYPT DISABLE\_DL\_DECRYPT DISABLE\_DL\_CACHE JTAG\_DISABLE CONSOLE\_DEBUG\_DISABLE 
           2.  Digite a palavra &quot;BURN&quot; para finalizar o processo;
#### Comandos necessários para **PRIMEIRA** gravação do firmware release no módulo (**NÃO SEGURO)**:
   1. Compilação do firmware release:
      > idf.py build
   2. Geração de imagem da partição &quot;nvs&quot; (contém os certificados para a aplicação) :
      > python /$IDF\_PATH/components/nvs\_flash/nvs\_partition\_generator/nvs\_partition\_gen.py generate main/helpers/files/csv/nvs.csv main/helpers/files/bin/nvs.bin 0x14000
   3. Gravação dos binários:
      1. Apague todo o conteúdo da memória flash:
         > idf.py erase\_flash
      2. Gravar bootloader, partition-table, otadata e iose\_wifi:
         > idf.py flash
      3. Gravar imagem da &quot;nvs&quot;:
         1. Comando para gravar imagem &quot;nvs&quot;:
            > esptool.py -p (PORT) -b 460800 --before default\_reset --after hard\_reset --chip esp32 write\_flash --flash\_mode dio --flash\_size detect --flash\_freq 40m 0x11000 main/helpers/files/bin/nvs.bin
   4. Teste de firmware:
      1. Enviar &quot;600\n&quot; para o módulo via UART:
         1. Sem resposta – Falha na gravação do firmware;
         2. Resposta &quot;602\n&quot; - Falta da imagem &quot;nvs&quot;;
         3.  Resposta &quot;601\n&quot; - Firmware OK.
#### Comandos **AUXILIARES** para gravação do fw release no módulo:
   1. Comando para ler MAC e fazer HARD RESET (**USAR CASO MÓDULO ATINJA A QUANTIDADE MAX DE MSGS VIA UART)**:
      > esptool.py -p (PORT) --after hard\_reset read\_mac
   2. Geração de binário assinado da Aplicação:
      1. Comandos para validar a imagem assinada
         1. Gere uma chave pública com o comando: 
            > espsecure.py extract\_public\_key -k main/helpers/files/keys/secure\_boot\_signing\_key.pem main/helpers/files/keys/secure\_boot\_signing\_key\_pub.pem
         2. Valide o binário com o comando: 
            > espsecure.py verify\_signature -v 1 -k main/helpers/files/keys/secure\_boot\_signing\_key\_pub.pem build/{INPUT\_DATAFILE}
      2. Comando para assinar a imagem de forma avulsa
         > espsecure.py sign\_data -v 1 -k {INPUT\_PRIVATE\_KEY} -o build/{INPUT\_DATAFILE} build/{OUTPUT\_DATAFILE}
   3. Regravação dos binários:
      1. Para regravar partition-table, otadata e iose\_wifi:
            > esptool.py -p (PORT) -b 460800 --before default\_reset --after no\_reset --chip esp32 write\_flash --flash\_mode dio --flash\_size detect --flash\_freq 40m --encrypt 0x10000 build/partition\_table/partition-table.bin 0x25000 build/ota\_data\_initial.bin 0xb0000 build/iose\_wifi.bin
      2. Para regravar o bootloader:
            > esptool.py -p (PORT) -b 460800 --before default\_reset --after no\_reset --chip esp32 write\_flash --flash\_mode dio --flash\_size detect --flash\_freq 40m --encrypt 0x0 build/bootloader/bootloader-reflash-digest.bin
#### Fluxo da etapa de gravação do firmware release:
1. Seguro:
   ***** ![](./imagens/Fluxogama_Etapa_gravacao_fw_release_seguro.png)
2. Não seguro:
   * ![](./imagens/Fluxograma_Etapa_gravacao_fw_release_nao_seguro.png)