﻿# Agendamento
# Introdução

Este documento especifica a funcionalidade de _Agendamento_. Nesta especificação são definidos:

* formatação de dados trocados entre software e firmware: requests de inserção, modificação, remoção e checagem de cadastros;
* fluxo de atividade do firmware para realização de Agendamento de ações no dispositivo;
* comportamento esperado do dispositivo para realização das ações nele agendadas.

Esta especificação deve, ao seu fim, ser de tal forma que o dispositivo esteja apto a realizar tarefas de acordo com os agendamentos cadastrados e na periodicidade dada em cada cadastro.

As possibilidades de agendamento e periodicidade esperadas para esta funcionalidade são:

1) possibilitar pelo menos 50 cadastros de ações;
2) periodicidade: semanal (repetitivas ou não);
3) excessões a cadastros existentes.

# Dados para operações sobre cadastros de ações

Nesta secção são determinados os dados trocados entre software e firmware a fim de:

* cadastrar um novo agendamento;
* editar um agendamento existente;
* remover um agendamento existente.
* checar se requisição de agendamento foi atendida.

São determinados dados para requisição tanto como resposta do dispositivo para os casos de sucesso e falha da ação.

## Informações para requisições de operação sobre cadastros

Para que operações sobre cadastros de ação agendadas no dispositivo ocorra, deve-se disponibilizar as seguintes informações:

### Agendamento normal

| Inserção | Edição | Remoção| Checagem |
|:-:|:-:|:-:|:-:|
| Operação: **0**  |  Operação: **1** | Operação: **2** | Operação: **3** |
| - | Índice | Índice | - |
| Timestamp de início |  Timestamp de início | Tipo de agendamento: **1** | Tipo de agendamento: **1** |
| - | - | - | Id da requisição |
| Ação sobre Relé | Ação sobre Relé  | - | - |
| Repetitividade | Repetitividade| - | - |
| Comportamento do agendamento para cada dia da semana| Comportamento do agendamento para cada dia da semana | - | - |

 O timestamp enviado serve tanto para determinar a data de início do agendamento requisitado como para determinar a hora e o minuto que a ação agendada deve ocorrer nos dias da semana em que o agendamento esteja habilitado a ocorrer.
 
### Agendamento de exceção

| Inserção | Edição | Remoção| Checagem |
|:-:|:-:|:-:|:-:|
| Operação: **0**  |  Operação: **1** | Operação: **2** | Operação: **3**
| - | Índice | Índice | - |
| Timestamp de início |  Timestamp de início | Tipo de agendamento: **0** | Tipo de agendamento: **0** |
| - | - | - | Id da requisição |
| Timestamp de fim |  Timestamp de fim | - |

### Formatação dos dado pela camada de comunicação

Os dados recebidos pelo dispositivo devem ser formatados adequadamente de acordo com o protocolo de comunicação o qual o dispositivo se encontra em operação. Caso haja algum erro de formatação **(tipo de dado inválido ou incompatibilidade com o padrão JSON)** do pacote enviado ao dispositivo, será enviado um pacote ao servidor no tópico **"dev/iose/general/return"** com os seguintes campos:

#### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único da módulo |
| uuid_req | string | Identificador único da requisição recebida (máx. 63 caracteres) |
| req_type | integer | Tipo de requisição recebida |
| success | boolean | Estado de execução |
| relay_state | boolean | Estado atual do relé |
| timestamp | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo
```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_req": "123456789",
    "req_type": 13,
    "success": false,
    "relay_state": true,
    "timestamp": "2020-09-23T18:54:12.000Z"
}
```

#### Protocolo de comunicação MQTT: agendamento normal

1) Inserção:

```json
{
  "uuid_req": "up-to-36-characters",
  "type": 13,
  "operation": 0,
  "time": <timestamp-unix>,
  "connect_relay": true,
  "repeat": true,
  "week": {
    "sun": true,
    "mon": true,
    "tue": false,
    "wed": false,
    "thu": false,
    "fri": false,
    "sat": true
  }
}
```

2) Edição:

```json
{
  "uuid_req": "up-to-36-characters",
  "type": 13,
  "operation": 1,
  "index": 12,
  "time": <timestamp-unix>,
  "connect_relay": true,
  "repeat": false,
  "week": {
    "sun": true,
    "mon": true,
    "tue": false,
    "wed": false,
    "thu": false,
    "fri": false,
    "sat": true
  }
}
```

3) Remoção:

```json
{
  "uuid_req": "up-to-36-characters",
  "type": 13,
  "operation": 2,
  "schedule-type": 1,
  "index":  12
}
```

4) Checagem:

```json
{
  "uuid_req": "up-to-36-characters",
  "type": 13,
  "operation": 3,
  "schedule-type": 1,
  "schedule-req_id": "up-to-36-characters"
}
```

#### Protocolo de comunicação MQTT: agendamento de exceção

1) Inserção:

```json
{
  "uuid_req": "up-to-36-characters",
  "type": 13,
  "operation": 0,
  "time-start": <timestamp-unix>,
  "time-end": <timstamp-unix>
}
```

2) Edição:

```json
{
  "uuid_req": "up-to-36-characters",
  "type": 13,
  "operation": 1,
  "index": 2,
  "time-start": <timestamp-unix>,
  "time-end": <timstamp-unix>
}
```

3) Remoção:

```json
{
  "uuid_req": "up-to-36-characters",
  "type": 13,
  "operation": 2,
  "schedule-type": 0,
  "index":  2
}
```
4) Checagem:

```json
{
  "uuid_req": "up-to-36-characters",
  "type": 13,
  "operation": 3,
  "schedule-type": 0,
  "schedule-req_id": "up-to-36-characters"
}
```

## Pacote para respostas de operação sobre cadastros

Esta secção define as possíveis respostas  para os tipos de requisições recebidas pelo dispositivo. 

Para que operações sobre um cadastramento de ação agendada no dispositivo a resposta do dispositivo contém as seguintes informações:

 1) identificação da requisição responsável pela operação no cadastro;
 2) índice do cadastro sobre o qual a operação foi realizada;
 3) status da operação.

As respostas para protocolo MQTT serão entregues no tópico "**dev/iose/general/action_schedule**".

A informação dada acima deve ser enviada pelo dispositivo. Em caso de protocolo de comunicação **MQTT** essa informação pode ser encodada em **JSON** como dado abaixo.

```json
{
  "uuid_req": "up-to-36-characters",
  "index": 12,
  "status": 0
}
```

Na definição acima as informações são:

| Campo | Descrição|
|:-:|-|
| uuid_req  | Identificação da requisição que realizou a operação no cadastro |
| index  | Índice do cadastro sobre o qual foi operado |
| status |  Resultado da operação requisitada, conforme abaixo: <br>&emsp; **0**: Sucesso <br>&emsp; **1**: Slots não disponíveis <br>&emsp; **2**: Slot inválido; <br>&emsp; **3 - 15**: Erro no armazenamento (file system) | |

# Casos de Uso

Nesta secção são definidos os casos de uso para o firmware. São definidos fluxos de funcionamento normal para sucesso e falhas.

Ocorrências fora destes casos devem ser reportados à equipe de desenvolvimento e serem analisados para possível adição aos casos aqui definidos.

## Inserção de cadastro

|  Condição / Fluxo | Descrição / Dado trocado |
|-|-|
| Ator Principal | Coisa e MDC |
| Ator Secundário | Coisa |
| Pré-condição | Coisa conectada a WiFi com acesso a Internet, ou; <br> Coisa conectada a rede LoRaWAN |
| Pós-condição | Dispositivo com agendamento salvo e íntegro |
| Fluxo Principal | 1) MDC envia requisição de **cadastramento**: <br><br>&emsp; json-req(0, 1613670517, "true", week); <br><br> 2) Coisa cadastra agendamento e responde com **sucesso**: <br><br>&emsp; json-res(3, 0).
| Fluxo Alternativo 1 | 1) MDC envia requisição de **cadastramento**: <br><br>&emsp;  json-req(0, 1613670517, "false", week); <br><br> 2) Coisa rejeita cadastramento e responde com **falha tipo 1** (slots indisponíveis):  <br><br>&emsp;  json-res(128, 1). |

No caso de slots não disponíveis, o dispositivo deve enviar a quantidade total de slots suportados.

## Edição de cadastro

|  Condição / Fluxo | Descrição / Dado trocado |
|-|-|
| Ator Principal | Coisa e MDC |
| Ator Secundário | Coisa |
| Pré-condição | Coisa conectada a WiFi com acesso a Internet, ou; <br> Coisa conectada a rede LoRaWAN |
| Pós-condição | Dispositivo com agendamento editado e íntegro |
| Fluxo Principal | 1) MDC envia requisição de **edição de cadastro já feito**: <br><br>&emsp; json-req(1, 0, schedule-info); <br><br> 2) Coisa edita cadastro e responde com **sucesso**: <br><br>&emsp; json-res(0, 0).

## Remoção de cadastro

|  Condição / Fluxo | Descrição / Dado trocado |
|-|-|
| Ator Principal | Coisa e MDC |
| Ator Secundário | Coisa |
| Pré-condição | Coisa conectada a WiFi com acesso a Internet, ou; <br> Coisa conectada a rede LoRaWAN |
| Pós-condição | Dispositivo com agendamento removido |
| Fluxo Principal | 1) MDC envia requisição de **remoção de cadastro populado**: <br><br>&emsp; json-req(2, 1); <br><br> 2) Coisa remove cadastro de agendamento e responde com **sucesso**: <br><br>&emsp; json-res(2, 0). |
| Fluxo Alternativo 1 | 1) MDC envia requisição de **remoção de cadastro de índice inválido**; <br><br>&emsp; json-req(2, 10289); <br><br> 2) Coisa rejeita operação e responde com ** falha tipo 2** (slot inválido): <br><br>&emsp; json-res(10289, 2). |

## Checagem de requisição executada

|  Condição / Fluxo | Descrição / Dado trocado |
|-|-|
| Ator Principal | Coisa e MDC |
| Ator Secundário | Coisa |
| Pré-condição | Coisa conectada a WiFi com acesso a Internet, ou; <br> Coisa conectada a rede LoRaWAN |
| Pós-condição | Dispositivo executou ou não a requisição |
| Fluxo Principal | 1) MDC envia requisição de **checagem de requisição executada**: <br><br>&emsp; json-req(3, 1, schedule-req_id); <br><br> 2) Coisa verifica se o schedule-req_id está armazenado e responde com **uuid_req = schedule-req_id** e **sucesso**: <br><br>&emsp; json-res(2, 0). |
| Fluxo Alternativo 1 | 1) MDC envia requisição de **checagem de requisição executada com id de requisição inválida ou não executada**: <br><br>&emsp; json-req(3, 1, schedule-req_id); <br><br> 2) Coisa rejeita operação e responde com **uuid_req = schedule-req_id**, **índice máximo** e **falha tipo 2** (Slot inválido): <br><br>&emsp; json-res(50, 2). |

# Fluxo do firmware para operações sobre cadastros

Abaixo é dado o Diagrama de Sequência do firmware para os comportamentos até aqui descritos.

![Resposta de agendamento de ações](./imagens/action-schedule-fw-seq-diag-1-v1r1.png)

# Execução de agendamentos cadastrados

Para execução de agendamentos cadastrados no dispositivo, este pode **executar uma checagem de todos os cadastros a cada 1 minuto**, considerando que a granularidade dos agendamentos seja de 1 minuto, para verificar a necessidade de executar algum dos eventos cadastrados.

**Notar que esta funcionalidade necessita diretamente que o dispositivo tenha data atualizada, caso contrário qualquer dos agendamentos não pode ser realizado com confiabilidade**.

Esta funcionalidade só pode ser habilitada uma vez que o dispositivo estiver com sua hora sincronizada com a rede a qual este se encontra.

## Habilitar execução de agendamento
