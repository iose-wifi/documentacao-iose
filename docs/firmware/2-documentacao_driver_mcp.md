﻿# Documentação Driver MCP
Nesse documento serão descritas as principais funcionalidades do driver feito para o sensor MCP39F511A.

## Principais funções
Aqui serão listadas e descritas as principais funções do driver.

### mcp_init(void)
Essa função inicializa as configurações iniciais do módulo MCP,assim como configura a porta serial do microcontrolador para se comunicar adequadamente com o MCP. <br>
**Observação:** O módulo MCP pode vir configurado de fábrica com o baud rate da serial em 9600bps,caso isso aconteça,a função **mcp_init()** irá automaticamente configurar para o baud rate correto de 115200bps. 
**Retorno:** Essa função tem como retorno um **enum** dizendo que a operação de inicialização foi completa com êxito.

### mcp_set_design_registers(void)
Essa função realiza todas as configurações de registradores do módulo MCP,para se adequar à aplicação em que ele está sendo utilizado.<br>
**Retorno:** Essa função tem como retorno um **enum** dizendo que a operação de inicialização foi completa com êxito. 

### mcp_get_system_status(void)
Essa função informa diversos parâmetros sobre as leituras e configurações do módulo MCP:
- **DCMode:** Informa se a tensão lida é DC ou AC.
- **Sign_DCCurrent:** Informa se a corrente RMS lida é positiva ou negativa.
- **Sign_DCVoltage:** Informa se a tensão RMS lida é positiva ou negativa.
- **Event2:** Informa se o Evento 2 ocorreu ou não.
- **Event1:** Informa se o Evento 1 ocorreu ou não.
- **OVERTEMP:** Informa se a temperatura limite foi ultrapassada ou não.
- **Sign_PR:** Informa se a potência reativa está positiva ou negativa.
- **Sign_PA:** Informa se a potência ativa está positiva ou negativa.
- **OVERPOW:** Informa se potência limite foi ultrapassada ou não.
- **OVERCUR:** Informa se corrente limite foi ultrapassada ou não.
- **VSURGE:** Informa se ocorreu ou não um surto de tensão além do limite.
- **VSAG:** Informa se ocorreu ou não uma queda de energia além do limite.

**Retorno:** Essa função tem como retorno uma **struct** contendo todas os parâmetros citados acima.

### mcp_get_system_version(void)
Essa função informa a versão de sistema do módulo MCP. <br>
**Retorno:** Essa função tem como retorno a data da versão no formato YYWW. 

### mcp_get_vrms(void)
Essa função informa a tensão RMS lida em Volts.<br>
**Retorno:** Essa função tem como retorno o valor da tensão RMS.

### mcp_get_line_frequency(void)
Essa função informa a frequência da rede de energia em Hertz.<br>
**Retorno:** Essa função tem como retorno o valor da frequência.

### mcp_get_thermistor_voltage(void)
Essa função informa a temperatura lida pelo sensor de temperatura interno do módulo MCP em Celsius.<br>
**Retorno:** Essa função tem com retorno a temperatura interna do módulo.

###	mcp_get_Power_factor(void)
Essa função informa o fator de potência lido pelo módulo MCP.<br>
**Retorno:** Essa função tem com retorno o fator de potência.

### mcp_get_Current_RMS(void)
Essa função informa o valor da corrente RMS lida pelo módulo MCP em Ampere.<br>
**Retorno:** Essa função tem com retorno a corrente RMS.

### mcp_get_active_power(void)
Essa função informa o valor da potência ativa lida pelo módulo MCP em Watt.<br>
**Retorno:** Essa função tem com retorno a potência ativa.

### mcp_get_reactive_power(void)
Essa função informa o valor da potência reativa lida pelo módulo MCP em kVAr.<br>
**Retorno:** Essa função tem com retorno a potência reativa.

### mcp_get_apparent_power(void)
Essa função informa o valor da potência aparente lida pelo módulo MCP em kVA.<br>
**Retorno:** Essa função tem com retorno a potência aparente.

### mcp_get_minimum_record_1(void)
Essa função informa o valor mínimo lido do limiar 1.<br>
**Retorno:** Essa função tem com retorno o valor mínimo lido do limiar 1.

### mcp_get_minimum_record_2(void)
Essa função informa o valor mínimo lido do limiar 2.<br>
**Retorno:** Essa função tem com retorno o valor mínimo lido do limiar 2.

### mcp_get_maximum_record_1(void)
Essa função informa o valor máximo lido do limiar 1.<br>
**Retorno:** Essa função tem com retorno o valor máximo lido do limiar 1.

### mcp_get_maximum_record_2(void)
Essa função informa o valor máximo lido do limiar 2.<br>
**Retorno:** Essa função tem com retorno o valor máximo lido do limiar 2.

### mcp_get_import_active_energy_counter(void)
Essa função informa o valor de energia ativa consumida em kWh.<br>
**Retorno:** Essa função tem com retorno o valor da energia ativa consumida.

### mcp_get_import_reactive_energy_counter(void)
Essa função informa o valor de energia reativa consumida em kWh.<br>
**Retorno:** Essa função tem com retorno o valor da energia reativa consumida.

### mcp_get_export_active_energy_counter(void)
Essa função informa o valor de energia ativa exportada em kWh.<br>
**Retorno:** Essa função tem com retorno o valor da energia ativa exportada.

### mcp_get_export_reactive_energy_counter(void)
Essa função informa o valor de energia reativa exportada em kWh.<br>
**Retorno:** Essa função tem com retorno o valor da energia reativa exportada.

## Inicialização e configurações iniciais.
Para começar a utilizar esse driver para o módulo MCP basta inicilizá-lo com a função **mcp_init()** . Após a operação tiver êxito,o próximo passo é configurar os registradores para calibrar o sensor de forma adequada,
para isso utilize a função **mcp_set_design_registers()** . Após a inicialização e configuração do módulo , as demais funções não precisam de configurações extras,estando todas prontas para uso.

