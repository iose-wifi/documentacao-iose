# Testes realizados durante o desenvolvimento
Nesse documento será definido e descrito os testes realizados em APIs ou recursos de hardware, para consulta futura.
## Leitura de bytes em FAT filesystem na ESP32
### Condições
* Tamanho do setor: 512 bytes;
* Wear leveling habilitado e em modo "safety".
### Tempo de leitura - 50 entradas de 4 bytes
| Amostra | Tempo (ms) |
|--------|--------|
| 1 | 5629 |
| 2 | 5561 |
| 3 | 5640 |
| 4 | 5599 |
| 5 | 5661 |
| **Média** | **5618** |

### Tempo de leitura - 50 entradas de 36 bytes
| Amostra | Tempo (ms) |
|--------|--------|
| 1 | 6020 |
| 2 | 5768 |
| 3 | 5562 |
| 4 | 5495 |
| 5 | 5743 |
| **Média** | **5717,6** |

### Conclusão
Com uma diferença de 99,6 ms é possível concluir que o aumento no numero da entrada não impacta significativamente o tempo de processo. Provavelmente o tempo elevado nas operações de escrita e leitura seja causado por operações de segurança no modo wear leveling.<br>