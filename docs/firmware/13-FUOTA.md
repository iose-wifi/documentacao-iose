﻿# Atualização Remota de Firmware (FUOTA)
# Introdução
Este documento especifica a funcionalidade de _Atualização Remota de Firmware_. Nesta especificação serão definidos:

* tópicos de requisição e resposta para a atualização remota de firmware;
* formatação de dados trocados entre software e firmware: requisição e resposta;
* comportamento esperado do dispositivo para realização da operação.

Esta especificação deve, ao seu fim, ser de tal forma que o dispositivo esteja apto a receber a requisição, baixar o firmware, enviar a mensagem de retorno para o tópico específico e executar a atualiazação.

# Comunicação MQTT
Nesta secção são determinados os tópicos de onde são feitos as requisições e para onde serão enviadas as medidas solicitadas e seus respectivos pacotes JSON.

## Requisição
### Tipo
| Operação | Tipo |
|----------|------|
| FUOTA | 9 |

### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |
| Produção | {ENDEREÇO MAC SEM PONTUAÇÃO E ESPAÇOS} |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_req | string | Identificador único da requisição (máx. 63 caracteres) |
| type | integer | Indica o tipo de requisição |
| payload | struct | Contém a URL para download |
| url | string | URL para download do firmware |

#### Exemplo
```json
{
	"uuid_req": "123456789",
	"type": 9, 
	"payload":
	{
		"url": "https://ota-iose-east1.s3.amazonaws.com/iose_wifi.bin"
	}
}
```
## Resposta - Retorno de execução da requisição
### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/return |
| Produção | prod/iose/general/return  |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único da módulo |
| uuid_req | string | Identificador único da requisição recebida (máx. 63 caracteres) |
| req_type | integer | Tipo de requisição recebida |
| success | boolean | Estado de execução |
| timestamp | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo
```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_req": "123456789",
    "req_type": 2,
    "success": true,
    "timestamp": "2020-09-23T18:54:12.000Z"
}
```

# Comportamento Esperado
## Falha
Abaixo segue os casos de falha e o comportamento.
| Falha | Comportamento |
|-------|---------------|
| Pacote JSON inválido | Enviará pacote de retorno com req_type = 0 e success = false |
| Falha no download do firmware | Enviará pacote de retorno com req_type = 9 e success = false |

## Sucesso
O módulo irá baixar a imagem do firmware, verificar sua versão e integridade, enviar a mensagem de retorno para o seu tópico e executar a atualização, reinicializando o módulo no final do processo.