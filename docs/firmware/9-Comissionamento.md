﻿# Comissionamento de Circuito
# Introdução

Este documento especifica a funcionalidade de _Comissionamento de Circuito_. Nesta especificação são definidos:

* captura das informação entre dispositivo configurador e o módulo IOSE;
* formatação de dados trocados entre software e firmware: mensagem com informações do circuito;
* fluxo de atividade do firmware para realização do comissionamento no dispositivo;
* comportamento esperado do dispositivo para realização do comissionamento.

Esta especificação deve, ao seu fim, ser de tal forma que o dispositivo esteja apto a enviar as informações de nome e descrição do circuito, ao qual o módulo foi comissionado, para o software.

As possibilidades de comissionamento esperadas para esta funcionalidade são:

1) capturar as informações do circuito na interface de configuração de Wi-Fi;
2) enviar mensagem contendo as informações para o software via MQTT.

# Dados para realizar o comissionamento

Nesta secção são determinados os dados trocados entre o dispositivo configurador (computador/mobile), módulo (firmware) e software a fim de:

* capturar as informação inseridas na interface de configuração de Wi-Wi;
* enviar informações do circuito onde o módulo está instalado.

São determinados dados apenas para envio, não há resposta do dispositivo para os casos de sucesso e falha da ação.

## Informações do circuito

Para que o comissionamento do circuito ao dispositivo ocorra, deve-se disponibilizar as seguintes informações por meio da interface de configuração de rede (disponível ao pressionar o botão "function" por 5 segundos, maiores detalhes no tópico 7.1 do [guia de usuário do módulo](https://gitlab.com/iose-wifi/documentacao-iose/-/raw/master/docs/firmware/anexos/Guia_do_usu%C3%A1rio_IoSE_Monof%C3%A1sico_V1R1_ed1-4_revisado.pdf?inline=true)):

| Informações |
|:-:|
| Nome do circuito |
| Descrição |

Essas informações devem ser recebidas pelo software a fim de identificar o módulo com seu circuito correspondente na interface.

# Comunicação MQTT
Nesta secção são determinados os tópicos de onde são feitos as requisições e para onde serão enviadas as medidas solicitadas e seus respectivos pacotes JSON.

### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/general/module_info |
| Produção | prod/iose/general/module_info |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único do módulo |
| circuit_name | string | Nome do circuito (Máx: 50 char)|
| description | string | Descrição do circuito (Máx: 250 char) |
| timestamp | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo

```json
{
    "uuid_circuit": "dev-mac-address",
    "circuit_name": "up-to-50-characters",
    "description": "up-to-250-characters",
    "timestamp": "2021-01-21T23:01:22.000Z"
}
```

# Casos de Uso

Nesta secção são definidos os casos de uso para o firmware. São definidos fluxos de funcionamento normal para sucesso e falhas.

Ocorrências fora destes casos devem ser reportados à equipe de desenvolvimento e serem analisados para possível adição aos casos aqui definidos.

## Captura e envio de informações de comissionamento

|  Condição / Fluxo | Descrição / Dado trocado |
|-|-|
| Ator Principal | Módulo e Usuário |
| Ator Secundário | Módulo |
| Pré-condição | Módulo em modo de configuração de rede |
| Pós-condição | Módulo com as informações capturadas em buffers |
| Fluxo Principal | 1) Usuário insere **informações do módulo** pela interface de configuração: <br><br>&emsp; um formulario POST é populado com as informações inseridas; <br><br> 2) Usuário insere ou escolhe uma **rede sem fio** e conecta: <br><br>&emsp; as infomações de rede são armazenadas no mesmo formulario e enviado para o módulo. <br><br> 3) Módulo recebe o formulario com **todas as informações**: <br><br>&emsp; todas as informações são guardadas dinamicamente no primeiro momento, a autenticação de rede é armazenada na flash. <br><br> 4) Módulo sincroniza **data e hora**, conecta no **broker MQTT** e envia as informações de comissionamneto: <br><br>&emsp; json-msg(240AC41598F0, Laboratório, NEPEN P&D, 2021-01-21T23:01:22.000Z).
| Fluxo Alternativo 1 | 1) Usuário não insere **informações do módulo** pela interface de configuração: <br><br>&emsp; é exibido uma mensagem de erro na tela do dispositivo configurador.
| Fluxo Alternativo 2 | 1) Usuário insere nome do circuito acima de **50 caracteres**, ou insere uma descrição acima de **250 caracteres** pela interface de configuração: <br><br>&emsp; o usuário é impedido de inserir mais caracteres ao atingir o limite 


# Fluxo de captura de informações de comissionamento

Para a captura das informações inseridas pelo usuário é feita seguindo o seguinto fluxo:
![Captura de informações do módulo](./imagens/module-info-capture-seq-v0r0.png)

# Fluxo do firmware para o envio das informações de comissionamento

Abaixo é dado o Diagrama de Sequência do firmware para os comportamentos até aqui descritos.

![Resposta de agendamento de ações](./imagens/module-info-fw-seq-diag-1-v0r0.png)
