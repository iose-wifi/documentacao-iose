﻿# Cadastramento do Módulo
# Introdução
Este documento especifica a funcionalidade de _Cadastramento do Módulo_. Nesta especificação serão definidos:

* tópico de envio do cadastramento do módulo;
* formatação de dados trocados entre o backend e o software de fábrica;
* comportamento esperado do backend para realização da operação.

Esta especificação deve, ao seu fim, ser de tal forma que o dispositivo seja cadastrado na interface com o usuário.

# Comunicação MQTT
Nesta secção são determinados os tópicos de onde são feitos as requisições e para onde serão enviadas as medidas solicitadas e seus respectivos pacotes JSON.

## Cadastramento
### Tópico
| Ambiente | Tópico |
|----------|--------|
| Desenvolvimento | dev/iose/register_module |
| Produção | prod/iose/register_module |

### Pacote JSON
| Campo | Tipo | Descrição |
|-------|------|-----------|
| uuid_circuit | string | Identificador único do módulo |
| uuid_client | string | Identificador único do cliente |
| uuid_unity | string | Identificador único da unidade |
| code_module | string | QR code do módulo |
| has_relay | boolean | Indica se o módulo cadastrado possui relé |
| timestamp | string | Timestamp (YYYY-MM-DDThh:mm:ss.msZ) |

#### Exemplo
```json
{
    "uuid_circuit": "AABBCCDDEEFF",
    "uuid_client": "123456789",
    "uuid_unity": "0000",
    "code_module": "ABCDEFGHIJK",
    "has_relay": true,
    "timestamp": "2020-09-23T18:54:12.000Z"
}
```

# Comportamento Esperado
O software de fábrica irá enviar o pacote de cadastramento na etapa final do processo de fábrica, o módulo cadastrado deve aparecer na lista de não associados na interface do cliente.