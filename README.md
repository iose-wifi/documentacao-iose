# Documentação Geral IOSE

A documentação pode ser acessada através do link : [https://iose-wifi.gitlab.io/documentacao-iose ](https://iose-wifi.gitlab.io/documentacao-iose )

# Utilização
Clone este repositório e dentro da pasta do projeto utilize o comando:
```bash
npm install
```
isso irá instalar todas as dependências necessárias. Após isso, para escrever a documentação é necessário entrar no modo desenvolvimento, para isto basta usar o comando a seguir.

```bash
npm run dev
```
Com isto feito já é possível criar arquivos markdown e editar os existente, estes ficam localizados dentro da pasta docs e são divididos em 4 pastas:
- __introducao__ - Contém os arquivos com fluxos e explicações básicas necessários para todas as partes do projeto.
- __hardware__ - Contém os arquivos para a documentação do hardware.
- __firmware__ - Contém os arquivos para a documentação do firmware.
- __frontend__ - contém arquivos para o frontend da aplicação.
- __backend__ - Contém os arquivos para a backend da aplicação.

Toda e qualquer alteração feita no repositório master estará automaticamente disponível mo site da documentação, por isso recomenda-se que as modificações sejam feitas em _branchs_ diferentes e após aprovação destas que seja transferida para o master.

